sap.ui.define([
	'jquery.sap.global',
	"sap/ui/core/mvc/Controller", 
	"sap/ui/core/routing/History",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/Filter",
	"sap/ui/core/format/DateFormat",
	"./Formatter"
], function(jQuery, Controller, History, FilterOperator, Filter, DateFormat, Formatter){
	"use strict";
	
	var ServiceCardController = Controller.extend("SeidorB1UI5.controller.ServiceCard", {
	
		formatter: SeidorB1UI5.controller.Formatter, busy: new sap.m.BusyDialog(),
	
		onInit: function() {
			if(sessionStorage.length == 0){
				this.handleLogout();
			}else{
				this.limpiaCampos();
			
				if(!oServiceJSONData || oServiceJSONData === undefined){
					sap.ui.core.BusyIndicator.show(0);
					oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
					getService();
					oServiceJSONData = JSON.parse(sessionStorage.getItem("services"));
				}	
				
				var oModel = null, view = this.getView();
				this._nuevo = true;
				oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
				//oDatosUsuario.code_col = "CFR0001";
				
				var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
				this.byId("avatar").setInitials(iAvatar);
				this.byId("avatar").setTooltip("Usuario: " + avatar);
				
				oDimenJSONData = JSON.parse(sessionStorage.getItem("location"));
				oEspecialidad = JSON.parse(sessionStorage.getItem("serviceEspecialidades"));
				
				oServiceCallsJSONData = JSON.parse(sessionStorage.getItem("services"));
	//			oServiceCallsJSONData.departamentos = JSON.parse(sessionStorage.getItem('departamentos')).value;
	//			oServiceCallsJSONData.tipoFinanciamientos = JSON.parse(sessionStorage.getItem('tipoFinanciamientos')).value;
				
				oModel = new sap.ui.model.json.JSONModel();
				oModel.setData(oServiceCallsJSONData);
				
				if(oDatosUsuario.code_rol === "2"){
					this.getView().byId("lblDescarga").setVisible(false);
					this.getView().byId("btnDescarga").setVisible(false);
				}
				
				//RUT PROVEEDORES
				var oBusinessPartner = null;
				getAsync("sociosTarjeta");
				oBusinessPartner = JSON.parse(sessionStorage.getItem("sociosTarjeta"));
				
				var oData = oModel.getData();
				//oData.BusinessPartners = oBusinessPartner.value;
//				this.getView().byId("inpCustomer").setValue(oBusinessPartner.CardCode);
				this.getView().byId("inpCustmrName").setValue(oBusinessPartner.CardCode +";"+oBusinessPartner.CardName);
				
				
				//NUMEROS DE SERIE
				var oSerialNumber = null;
				getAsync("numerosDeSerie");
				oSerialNumber = JSON.parse(sessionStorage.getItem("numerosDeSerie"));
				oData.SerialNumbers = oSerialNumber.value;
				
				//ACTIVOS FIJOS
				var oItems = null;
				getAsync("activosFijos");
				oItems = JSON.parse(sessionStorage.getItem("activosFijos"));
				oData.Items = oItems.value;
				
				//PAISES
				var oCountries = null;
				getAsync("paises");
				oCountries = JSON.parse(sessionStorage.getItem("paises"));
				oData.Countries = oCountries.value;
				
				//ESTADOS O REGIONES
				var oStates = null;
				getAsync("regiones");
				oStates = JSON.parse(sessionStorage.getItem("regiones"));
				oData.States = oStates.value;
				
				//DIMESIONES
				var oDimenJSONData = null;
				oDimenJSONData = JSON.parse(sessionStorage.getItem("location"));
				oData.Ubicacion1 = this.loadCombobox(oDimenJSONData);
				
				//ESTRUCTURA FISICA
				var oEstruFisica = null;
				getAsync("estructuraFisica");
				oEstruFisica = JSON.parse(sessionStorage.getItem("estructuraFisica"));
				oData.PhisicalStructure = oEstruFisica.value;
				
				//PRESUPUESTOS
				var oPresupuestos = null;
				getAsync("presupuestos");
				oPresupuestos = JSON.parse(sessionStorage.getItem("presupuestos"));
				oPresupuestos = oPresupuestos.ValidValuesMD;
							
				oData.Presupuestos = oPresupuestos;
				
				//MOTIVOS BAJA
				var oMotivosBaja = null;
				getAsync("motivosBaja");
				oMotivosBaja = JSON.parse(sessionStorage.getItem("motivosBaja"));
				oMotivosBaja = oMotivosBaja.value[0].ValidValuesMD;
							
				oData.MotivosBaja = oMotivosBaja;
				
				view.setModel(oModel);
		    	
		    	view.byId("btnAceptar").setVisible(true);
				view.byId("btnCancelar").setVisible(true);
				view.byId("cbxUbicacion").setSelectedKey(oDatosUsuario.cole_cod);
		    			
		    	sap.ui.core.UIComponent.getRouterFor(this).attachRouteMatched(function(oEvent) {
					if (oEvent.getParameter("name") === "ServiceDetail") {
						var oListItem = ordersTable;			    
						var context = new sap.ui.model.Context(view.getModel(), oListItem.getBindingContext().getPath());
						view.setBindingContext(context);
						
						var found = oListItem.getBindingContext().getPath().indexOf("ServiceCalls/");
						this.path = oListItem.getBindingContext().getPath().substring(found);
						var doc   = oModel.oData.ServiceCalls[this.path.replace('ServiceCalls/','')];
						
						this._callID = (doc.callID) ? parseInt(doc.callID) : -2;//ESTADO UDO CONSUMO
	
						this.busy.open();
	//					limit = "";filtro = "&filter=ServiceCalls.callID eq " + doc.callID;
	//					select = "&select=callID,DocNum,Customer,custmrName,subject,createDate,status,U_U_SEI_FECHO,U_U_SEI_HORAO,U_U_SEI_CODCIR,U_U_SEI_VEND,U_U_SEI_INST,U_U_SEI_TIPS,U_U_SEI_HORACV,U_U_SEI_LAD,U_U_SEI_TIPOS,U_U_SEI_CODCIR";
	//					getAsync('ServiceCalls', 'Line');
						getServicesCalls(doc.callID)
						if(sessionStorage.getItem("ServiceCallsLine"))
							oServiceCallsJSONData.DI = JSON.parse(sessionStorage.getItem("ServiceCallsLine"));
						this.busy.close();
						
						this.cargaAsisQui(oServiceCallsJSONData.DI);
						this.cargaInciden(oServiceCallsJSONData.DI);
						
						var Activities = {Activities:[]};
						var newActivities = {Activities:[]};
						
						Activities = this.cargaActividades(oServiceCallsJSONData.DI);
						var oData = view.getModel().getData();
						oData.Activities = Activities.Activities;
						
						oModel.setData(oServiceCallsJSONData);
						view.getModel().setData(oData);//ASINGNA ACTIVIDADES AL MODELO
						view.setModel(oModel);
				    }
				}, this);
			}
		},
		
		onNavButton: function(oEvent) {
			var oView = this, oHistory, sPreviousHash, oRouter, oTarget, asistQui, inciTipo;
			oHistory = sap.ui.core.routing.History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();
			oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			
			new sap.m.MessageBox.show("Esta de Seguro de Volver. Si lo hace, se perdera todos los cambios.", {
				icon: sap.m.MessageBox.Icon.INFORMATION,
				title: "Creacion de Tarjeta de Equipo",
				actions: [sap.m.MessageBox.Action.OK, sap.m.MessageBox.Action.CANCEL],
				emphasizedAction: sap.m.MessageBox.Action.OK,
				onClose: function (oAction) { 
					if(oAction === "OK"){
						oView.limpiaCampos();
						if (sPreviousHash !== undefined) {
							window.history.go(-1);
						} else {
							oRouter.navTo("Page", true);
						}
					}
				}
			});
		},
		
		handleLogout : function (evt) {
			sessionStorage.clear();
			delCookies();
			window.location.replace("");
		},
		
		validaCambioUbicacion: function(oEvent){
			var oModel = this.getView().getModel();
			var oData = oModel.getData();
			var oValidatedComboBox = oEvent.getSource(),
		    sSelectedKey = oValidatedComboBox.getSelectedKey(),
			sValue = oValidatedComboBox.getValue(), oBusinessPartner;
			
			
			oDatosUsuario.socio_cod = sSelectedKey;
			oDatosUsuario.cole_cod = sSelectedKey;
			getAsync("sociosNegocio");
			oBusinessPartner = JSON.parse(sessionStorage.getItem("sociosNegocio"));
			
			var oEstruFisica = null;
			getAsync("estructuraFisica");
			oEstruFisica = JSON.parse(sessionStorage.getItem("estructuraFisica"));
			oServiceCallsJSONData.PhisicalStructure = oEstruFisica.value;
			
			this.getView().byId("inpCustmrName").setValue(oBusinessPartner.CardCode +";"+oBusinessPartner.CardName);
			
			//NUMEROS DE SERIE
			var oSerialNumber = null;
			getAsync("numerosDeSerie");
			oSerialNumber = JSON.parse(sessionStorage.getItem("numerosDeSerie"));
			oServiceCallsJSONData.SerialNumbers = oSerialNumber.value;
			
			//ACTIVOS FIJOS
			var oItems = null;
			getAsync("activosFijos");
			oItems = JSON.parse(sessionStorage.getItem("activosFijos"));
			oServiceCallsJSONData.Items = oItems.value;
			
		},
		
		cargaAsisQui: function(oServiceCalls) {
			var oServiceCall = oServiceCalls.ServiceCalls, oFormat, sDate, fechaO;
			
			oFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({ style: "medium" });
			sDate = (oServiceCall.U_U_SEI_FECHO) ? oServiceCall.U_U_SEI_FECHO: "00000000";
			sDate = sDate.substr(0,4) + "-" + sDate.substr(4,2) + "-" + sDate.substr(6,2)
			
			fechaO = (oServiceCall.U_U_SEI_FECHO) ? new Date(sDate).toISOString().slice(0,10) : sDate;
			
			this.getView().byId("txtNDoc").setText(oServiceCall.DocNum);
//			this.getView().byId("txtEstDoc").setText(oServiceCall.status);
			this.getView().byId("txtCodCir").setText(oServiceCall.U_U_SEI_CODCIR);
			this.getView().byId("txtHorCir").setText(this.formatter.formatHour(oServiceCall.U_U_SEI_HORAO));
			this.getView().byId("txtFechCir").setText(fechaO);
			this.getView().byId("txtLado").setText(oServiceCall.U_U_SEI_LAD);
			this.getView().byId("txtInstitucion").setText(oServiceCall.U_U_SEI_INST);
			
			this.busy.open();
			limit = "";filtro = "";
			select = "&select=Code,Name,U_SEI_ZON";
			getAsync('AsistenteQuirurgico');
			if(sessionStorage.getItem("AsistenteQuirurgico"))
				oServiceCallsJSONData.Asistente = JSON.parse(sessionStorage.getItem("AsistenteQuirurgico")).UserObjects;
			this.busy.close();
			
			this.getView().byId("cbxAsiQui").removeAllItems();
			for(var i = 0; i < oServiceCallsJSONData.Asistente.length; i++){
				var asistente = oServiceCallsJSONData.Asistente[i];
				
				var newItem = null;
				newItem = new sap.ui.core.Item({key: asistente.Code, text: asistente.Name});
				this.getView().byId("cbxAsiQui").addItem(newItem);
			}
			
//			this.byId().setText(oServiceCall.U_U_SEI_VEND);
//			this.byId().setText(oServiceCall.U_U_SEI_TIPS);
//			this.byId().setText(oServiceCall.U_U_SEI_HORACV);
//			this.byId().setText(oServiceCall.U_U_SEI_TIPOS);
		},
		
		cargaInciden: function(oServiceCalls){
			var oServiceCall = oServiceCalls.ServiceCalls;
			this.getView().byId("cbxSocNeg").setValue(oServiceCall.CustomerName).setSelectedKey(oServiceCall.CustomerCode);
			
			this.busy.open();
			limit = "";filtro = "";select = "";
			getAsync('ActivityTypes');
			if(sessionStorage.getItem("ActivityTypes"))
				oServiceCallsJSONData.ActivityTypes = JSON.parse(sessionStorage.getItem("ActivityTypes")).ActivityTypes;
			this.busy.close();
			
			this.getView().byId("cbxTipo").removeAllItems();
			for(var i = 0; i < oServiceCallsJSONData.ActivityTypes.length; i++){
				var activityType = oServiceCallsJSONData.ActivityTypes[i];
				
				var newItem = null;
				newItem = new sap.ui.core.Item({key: activityType.Code, text: activityType.Name});
				this.getView().byId("cbxTipo").addItem(newItem);
			}
			
		},

		cargaTarjeta: function(oServiceCalls){
			var oServiceCall = oServiceCalls.ServiceCalls;
			var SN = this.byId("inpCustmrName").getValue();
			
			this.busy.open();
			filtro = "ManufacturerSerialNum eq '" + SN + "'";
			getAsync('equipoTarjeta');
			if(sessionStorage.getItem("equipoTarjeta"))
				oServiceCallsJSONData.ServiceCard = JSON.parse(sessionStorage.getItem("equipoTarjeta")).ActivityTypes;
			this.busy.close();
			
			for(var i = 0; i < oServiceCallsJSONData.ServiceCard.length; i++){
				var activityType = oServiceCallsJSONData.ActivityTypes[i];
				
				var newItem = null;
				newItem = new sap.ui.core.Item({key: activityType.Code, text: activityType.Name});
				this.getView().byId("cbxTipo").addItem(newItem);
			}
		},
		
		cargaActividades: function(oServiceCalls){
			var oServiceCall = oServiceCalls.ServiceCalls;
			var Activities = {Activities:[]};
			var newActivities = {Activities:[]};
			
			sessionStorage.removeItem("activities");
			
			select ="&select=parentId,ClgCode,CntctType,CardCode,DocEntry,DocType,status,Details,CntctDate,CntctTime,AssignedBy"; limit = "";filtro = "&filter=Activities.parentId eq $to_varchar(" + oServiceCall.ServiceCallID + ")";
			getAsync("activities");
			var DateFormat = sap.ui.core.format.DateFormat;
			
			if(sessionStorage.getItem("activities")){
				if(JSON.parse(sessionStorage.getItem("activities")).Status.DIresult === "success"){//VALIDA RESULTADO DE LLAMADA
					var incidencias = JSON.parse(sessionStorage.getItem("activities")).Activities;//ASIGNA OBJETO ACTIVIDAD A VARIABLE
			
					for(var i = 0; i < incidencias.length; i++){//RECORRE ELEMENTOS DEL OBJETO
						var incidencia = incidencias[i];//ASIGNA ELEMENTO SEGUN CONTADOR
						
						 var hora = this.formatter.formatHour(incidencia.CntctTime);
//						var hora  = incidencia.CntctTime;
//						hora = (hora.length === 3) ? "0" + hora : hora;
//						hora = hora.substr(0,2) + ':' + hora.substr(2,2);
						
						var fecha = new Date(incidencia.CntctDate).toISOString().slice(0,10);
						
						var activities = {};
						var oFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({ style: "medium" });
						var sDate = fecha + " " + hora;
						var oDate = oFormat.format(new Date(sDate));
						
						activities.Author 	= incidencia.AssignedByName;
						activities.Date 	= oDate;
						activities.Text 	= incidencia.Details;
						activities.ActivityCode = incidencia.ClgCode;
						activities.ActivityType = incidencia.CntctType;
						
						Activities.Activities.push(activities);
					}
				}
			} else {
				new sap.m.MessageBox.show("Hubo un problema en la carga de Incidencias", {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: "Actualizacion Llamada de Servicios",
					onClose: function(oAction) {
						
					}
				});
			}
			
//			return Activities.Activities;
			return Activities;
		},
		
		onPost: function(oEvent) {
			var DateFormat, oFormat, oFecha, oHora, oDate, sDate, sFech, sHora, sValue, inciTipo;
			inciTipo = this.getView().byId("cbxTipo");
			
			if(inciTipo.getValue() && inciTipo.getValueState() === "None"){
				DateFormat = sap.ui.core.format.DateFormat;
				oFormat = DateFormat.getDateTimeInstance({ style: "medium" });
				oFecha = DateFormat.getDateTimeInstance({ pattern: "yyyy-MM-dd" });
				oHora = DateFormat.getDateTimeInstance({ pattern: "HH:mm:ss" });
				
				oDate = new Date();
				sDate = oFormat.format(oDate);
				sFech = oFecha.format(oDate);
				sHora = oHora.format(oDate);
				//creacion de mensaje
				sValue = oEvent.getParameter("value");
				var  oEntry = {
						"ActivityDate": sFech,
						"ActivityTime": sHora,			
						"Author": oDatosUsuario.user_code,
						"Subject": oDatosUsuario.user_id,
						"Date": "" + sDate,
						"Text": inciTipo.getValue() + ": " + sValue
				};
				
				var oModel = this.getView().getModel();
				var aEntries = oModel.getData().Activities;
				aEntries.unshift(oEntry);
				oModel.refresh();
				this.setActivity(oEntry);
				//this.getView().byId("Feed").setEnabled(false);
			} else {
				new sap.m.MessageBox.show('Se debe seleccionar un valor valido en campo "Tipo", para crear la Incidencia.', {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: "Actualizacion Llamada de Servicios",
					onClose: function(oAction) {
						
					}
				});
				
			}
		},
		
		setActivity: function(oEntry){
			var oActivities = {}, oDetailActivity = {}, res = 0, inciTipo;
			inciTipo = this.getView().byId("cbxTipo");
				
			oDetailActivity.CardCode	 		= this.getView().byId("cbxSocNeg").getSelectedKey();
			oDetailActivity.ActivityDate 		= oEntry.ActivityDate;
			oDetailActivity.ActivityTime		= oEntry.ActivityTime;
			oDetailActivity.Details 			= oEntry.Text;
			oDetailActivity.ActivityType		= parseInt(inciTipo.getSelectedKey());
			oDetailActivity.Duration 			= 900;
			oDetailActivity.DurationType 		= "du_Minuts";
			oDetailActivity.Reminder 			= "tYES";
			oDetailActivity.ReminderPeriod 		= 15;
			oDetailActivity.ReminderType 		= "du_Minuts";
			oDetailActivity.ParentObjectId		= this._callID;
			oDetailActivity.ParentObjectType	= "191";
			
			oActivities.Activity = oDetailActivity
			
			this.busy.open();
			res = setActivities(oActivities);			
			if(res > 0){ this.updateServices(this._callID, res); }			
			this.busy.close();
			
			return res;
		},
		
		updateServices: function(_serviceCallId, activityCode) {
			var serviceCall = {};
			serviceCall.ServiceCallActivities = [{"ActivityCode": activityCode}];
			
			if(oServiceCallsJSONData.DI.ServiceCallActivities){
				var servicesCallActivities = oServiceCallsJSONData.DI.ServiceCallActivities;
				
				if(servicesCallActivities.ActivityCode){
					serviceCall.ServiceCallActivities.push({ "LineNum": servicesCallActivities.LineNum, "ActivityCode": servicesCallActivities.ActivityCode})
				} else if(servicesCallActivities.length > 0){
					for(var i = 0; i < servicesCallActivities.length; i++){
						var servicesActivities = servicesCallActivities[i];
						
						serviceCall.ServiceCallActivities.push({ "LineNum": servicesActivities.LineNum, "ActivityCode": servicesActivities.ActivityCode})
					}
				}
			}
			addActivitiesService(_serviceCallId, serviceCall);	
		},
		
		loadCombobox: function(oDimenJSONData){			
			var aTemp = [];
			var oUbicacion1 = [];		
			for(var i = 0; i < oDimenJSONData.value.length; i++){
				var oUbicaciones = oDimenJSONData.value[i];				
				if(oUbicaciones.U_SEI_NIVEL1 && jQuery.inArray(oUbicaciones.U_SEI_NIVEL1, aTemp) < 0){
					aTemp.push(oUbicaciones.U_SEI_NIVEL1);
					oUbicacion1.push({"Value": oUbicaciones.U_SEI_COLEGIO,"Name": oUbicaciones.U_SEI_NIVEL1});
				}
			}
			//provisorio
			oServiceJSONData.Ubicacion1 = oUbicacion1;
			return oUbicacion1;
		},
		
		handleBuscarBtn: function(){
			this._nuevo = false;
			//TODO Limpia los formularios
			this.limpiaCampos();
			//TODO Muestra campos de busqueda
			this.byId("lblInternalSNSearch").setVisible(true); 
			this.byId("inpInternalSNSearch").setVisible(true);
			//TODO Oculta campos de busqueda
			this.byId("lblInternalSNSet").setVisible(false); 
			this.byId("inpInternalSNSet").setVisible(false);
			
			//TODO Muestra campos de busqueda
			this.byId("lblItemCodeSearch").setVisible(false); 
			this.byId("inpItemCodeSearch").setVisible(false);
			//TODO Oculta campos de busqueda
			this.byId("lblItemCode").setVisible(true); 
			this.byId("inpItemCode").setVisible(true);
		},
		
		handleNuevoBtn: function(){
			this._nuevo = true;
			//TODO Limpia los formularios
			this.limpiaCampos();
			//TODO Muestra campos de busqueda
			this.byId("lblInternalSNSet").setVisible(true); 
			this.byId("inpInternalSNSet").setVisible(true);
			//TODO Oculta campos de busqueda
			this.byId("lblInternalSNSearch").setVisible(false); 
			this.byId("inpInternalSNSearch").setVisible(false);
			
			//TODO Oculta campos de busqueda
			this.byId("lblItemCode").setVisible(false); 
			this.byId("inpItemCode").setVisible(false);
			//TODO Muestra campos de busqueda
			this.byId("lblItemCodeSearch").setVisible(true); 
			this.byId("inpItemCodeSearch").setVisible(true);
			
		},
		
		handleAceptBtn: function(oEvent) {
			var msg = "";
			
			//CAMPOS TARJETA
			var tjSN, tjItemCode, tjItemName, tjStatus, tjMotivoBaja, tjCustomer, tjCustomerName, tjInstallLocation = this;
			
			tjSN = (this._nuevo == true) ? this.getView().byId("inpInternalSNSet").getValue() : this.getView().byId("inpInternalSNSearch").getValue() ;
			tjItemCode = (this._nuevo == true) ? this.getView().byId("inpItemCodeSearch").getValue() : this.getView().byId("inpInternalSNSearch").getValue() ;
			tjItemName = this.getView().byId("txtItemName").getValue();
			tjStatus = this.getView().byId("cbxStatus").getSelectedKey();
			tjCustomer = this.getView().byId("inpCustmrName").getValue().split(';')[0];
			tjCustomerName = this.getView().byId("inpCustmrName").getValue().split(';')[1];
			tjInstallLocation = this.getView().byId("inpInstallLocation").getValue();
			
			if(tjItemCode == "" || tjItemName == "" ||tjStatus == "" || tjCustomer == "" || tjCustomerName == ""){
				msg += "Debe llenar los campos de cabezera en Detalle de Equipo.\n";
			}
			
			if(this.getView().byId("cbxU_SEI_MOTIVO_BAJA").getEnabled() == true)
				if(this.getView().byId("cbxU_SEI_MOTIVO_BAJA").getValue() == "")
					msg += "Debe seleccionar un valor para el motivo de baja.\n";
				else
					tjMotivoBaja = this.getView().byId("cbxU_SEI_MOTIVO_BAJA").getSelectedKey(); 
			
			//CAMPOS DIRECCION
			var dirStreet, dirCity, dirCounty, dirCountry, dirState = this;
			dirStreet = this.getView().byId("inpStreet").getValue();
			dirCountry = this.getView().byId("cbxCountry").getSelectedKey();
			dirState = this.getView().byId("cbxState").getSelectedKey();
			dirCity = this.getView().byId("cbxCity").getValue();
			dirCounty = this.getView().byId("cbxCounty").getValue();
			
//			if(dirStreet == "" || dirCity == "" || dirCounty == "" || dirCountry == "" || dirState == ""){
//				msg += "No se encuentran completos los campos de direccion.\n";
//			}
			
			//CAMPOS EQUIPO
			var eqEquipmentCardNum, eqU_SEI_Path_number, eqU_SEI_Modelo, eqU_SEI_Marca, eqU_SEI_Cod_Barra_Interno, eqU_SEI_Ram, eqU_SEI_CapHHD, eqU_SEI_Procesador, eqU_SEI_Version_SO, eqU_SEI_IDProducto_SO, eqU_SEI_key_SO, eqU_SEI_Version_Office, eqU_SEI_ID_Office, eqU_SEI_Nro_Licencia_Office, eqU_SEI_T_EQUIPO, eqU_SEI_IP, eqU_SEI_ANTIVIRUS, eqU_SEI_MOTIVO_BAJA = this;

			
			eqU_SEI_Path_number = this.getView().byId("inpU_SEI_Path_number").getValue();
			eqU_SEI_Modelo = this.getView().byId("inpU_SEI_Modelo").getValue();
			eqU_SEI_Marca = this.getView().byId("inpU_SEI_Marca").getValue();
			//eqU_SEI_Cod_Barra_Interno = this.getView().byId("inpU_SEI_Cod_Barra_Interno").getValue();
			eqU_SEI_Ram = this.getView().byId("inpU_SEI_Ram").getValue();
			eqU_SEI_CapHHD = this.getView().byId("inpU_SEI_CapHHD").getValue();
			eqU_SEI_Procesador = this.getView().byId("inpU_SEI_Procesador").getValue();
			eqU_SEI_Version_SO = this.getView().byId("inpU_SEI_Version_SO").getValue();
			eqU_SEI_IDProducto_SO = this.getView().byId("inpU_SEI_IDProducto_SO").getValue();
			eqU_SEI_key_SO = this.getView().byId("inpU_SEI_key_SO").getValue();
			eqU_SEI_Version_Office = this.getView().byId("inpU_SEI_Version_Office").getValue();
			eqU_SEI_ID_Office = this.getView().byId("inpU_SEI_ID_Office").getValue();
			eqU_SEI_Nro_Licencia_Office = this.getView().byId("inpU_SEI_Nro_Licencia_Office").getValue();
			eqEquipmentCardNum = this.getView().byId("inpEquipmentCardNum").getValue();
			eqU_SEI_IP = this.getView().byId("inpU_SEI_IP").getValue();
			eqU_SEI_ANTIVIRUS = this.getView().byId("cbxU_SEI_ANTIVIRUS").getValue();
			eqU_SEI_MOTIVO_BAJA = this.getView().byId("cbxU_SEI_MOTIVO_BAJA").getSelectedKey();
			
//			if(eqU_SEI_Cod_Barra_Interno == "") { //if(eqU_SEI_Path_number == "" || eqU_SEI_Modelo == "" || eqU_SEI_Marca == "" || eqU_SEI_Cod_Barra_Interno == "" ||  eqU_SEI_Ram == "" ||  eqU_SEI_CapHHD == "" ||  eqU_SEI_Procesador == "" ||  eqU_SEI_Version_SO == "" ||  eqU_SEI_IDProducto_SO == "" ||  eqU_SEI_key_SO == "" || eqU_SEI_T_EQUIPO == "" || eqU_SEI_IP == "" || eqU_SEI_ANTIVIRUS == ""){
//				msg += "Se debe ingresar un Codigo de barra Interno.\n";
//			}
			
			//CAMPOS FACTURACION
			var facU_SEI_FAC, facU_SEI_Factura, facU_SEI_Garantia, facU_SEI_RUTPROV, facU_SEI_Presupuesto, facU_SEI_Fecha_Compra = this;
			
			facU_SEI_FAC = this.getView().byId("inpU_SEI_FAC").getValue();
			facU_SEI_Factura = this.getView().byId("inpU_SEI_Factura").getValue();
			facU_SEI_Garantia = this.getView().byId("inpU_SEI_Garantia").getValue();
			facU_SEI_RUTPROV = this.getView().byId("inpU_SEI_RUTPROV").getValue();
			facU_SEI_Presupuesto = this.getView().byId("cbxU_SEI_Presupuesto").getSelectedKey();
			facU_SEI_Fecha_Compra = this.getView().byId("inpU_SEI_Fecha_Compra").getValue();
			
//			if(facU_SEI_FAC == "" || facU_SEI_Factura == "" || facU_SEI_Garantia == "" || facU_SEI_RUTPROV == "" || facU_SEI_Presupuesto == "" || facU_SEI_Fecha_Compra == ""){
//				msg += "No se encuentran completos los campos de Facturacion.\n";
//			}
			
			//CAMPOS UBICACION
			var dirU_SEI_UBICACION_PRINCIPAL = this;
		
			dirU_SEI_UBICACION_PRINCIPAL = this.getView().byId("inpUbicacionSearch");
						
			if(dirU_SEI_UBICACION_PRINCIPAL.getValue() == "" ){
				msg += "No se encuentran seleccionados una Ubicacion para la tarjeta de equipo. ";
			}
			
			if(msg !== ""){
				new sap.m.MessageBox.show(msg, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Creacion de Tarjeta de Equipo",
				});
				return;
			}
			
			var oEquipmentCard = {
				InternalSerialNum: tjSN,
				CustomerCode : tjCustomer,
				CustomerName: tjCustomerName,
				ItemDescription : tjItemName,
				StatusOfSerialNumber : tjStatus,
				InstallLocation: tjInstallLocation,
				//DIRECCION
				Street: dirStreet == "" ? undefined : dirStreet,
				CountryCode : "CL",
				StateCode : dirState == "" ? undefined : dirState,
				City: dirCity == "" ? undefined : dirCity,
				County: dirCounty  == "" ? undefined : dirCounty,
				//EQUIPO
				U_SEI_Path_number : eqU_SEI_Path_number == "" ? undefined : eqU_SEI_Path_number,
				U_SEI_Modelo : eqU_SEI_Modelo == "" ? undefined : eqU_SEI_Modelo,
				U_SEI_Marca : eqU_SEI_Marca == "" ? undefined : eqU_SEI_Marca, 
//				U_SEI_Cod_Barra_Interno : eqU_SEI_Cod_Barra_Interno, 
				U_SEI_Ram : eqU_SEI_Ram == "" ? undefined : eqU_SEI_Ram, 
				U_SEI_CapHHD : eqU_SEI_CapHHD == "" ? undefined : eqU_SEI_CapHHD, 
				U_SEI_Procesador: eqU_SEI_Procesador == "" ? undefined : eqU_SEI_Procesador, 
				U_SEI_Version_SO : eqU_SEI_Version_SO == "" ? undefined : eqU_SEI_Version_SO, 
				U_SEI_IDProducto_SO : eqU_SEI_IDProducto_SO == "" ? undefined : eqU_SEI_IDProducto_SO, 
				U_SEI_key_SO : eqU_SEI_key_SO == "" ? undefined : eqU_SEI_key_SO, 
				U_SEI_Version_Office : eqU_SEI_Version_Office == "" ? undefined : eqU_SEI_Version_Office, 
				U_SEI_ID_Office : eqU_SEI_ID_Office == "" ? undefined : eqU_SEI_ID_Office, 
				U_SEI_Nro_Licencia_Office : eqU_SEI_Nro_Licencia_Office == "" ? undefined : eqU_SEI_Nro_Licencia_Office, 
				//U_SEI_T_EQUIPO : eqU_SEI_T_EQUIPO == "" ? undefined : eqU_SEI_T_EQUIPO, 
				U_SEI_IP : eqU_SEI_IP == "" ? undefined : eqU_SEI_IP, 
				U_SEI_ANTIVIRUS : eqU_SEI_ANTIVIRUS == "" ? undefined : eqU_SEI_ANTIVIRUS,
				U_SEI_MOTIVO_BAJA : eqU_SEI_MOTIVO_BAJA == "" ? undefined : eqU_SEI_MOTIVO_BAJA,
				//FACTURACION
				U_SEI_FAC : facU_SEI_FAC == "" ? undefined : facU_SEI_FAC, 
				U_SEI_Factura : facU_SEI_Factura == "" ? undefined : facU_SEI_Factura, 
				U_SEI_Garantia : facU_SEI_Garantia == "" ? undefined : facU_SEI_Garantia, 
				U_SEI_RUTPROV : facU_SEI_RUTPROV == "" ? undefined : facU_SEI_RUTPROV, 
				U_SEI_Presupuesto : facU_SEI_Presupuesto == "" ? undefined : facU_SEI_Presupuesto, 
				U_SEI_Fecha_Compra : facU_SEI_Fecha_Compra == "" ? undefined : facU_SEI_Fecha_Compra,
				//UBICACION
				U_SEI_UBICACION_PRINCIPAL : dirU_SEI_UBICACION_PRINCIPAL.getValue(),
            };
			
			if(this._nuevo == true)
				oEquipmentCard.ItemCode= tjItemCode;
			
			if(setEquipmentCard(oEquipmentCard,this._nuevo) == 1){
//				TODO Limpia los formularios
				this.limpiaCampos();
				
				//NUMEROS DE SERIE
				var oSerialNumber = null;
				getAsync("numerosDeSerie");
				oSerialNumber = JSON.parse(sessionStorage.getItem("numerosDeSerie"));
				this.getView().getModel().oData.SerialNumbers = oSerialNumber.value;
			};
			
		},

		handleCancelBtn: function(oEvent) {
			var oView = this;
			
			new sap.m.MessageBox.show("Si Cancelas se perderan los cambios realizados y campos seleccionados", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Creacion de Tarjeta de Equipo",
				actions: [sap.m.MessageBox.Action.OK, sap.m.MessageBox.Action.CANCEL],
				emphasizedAction: sap.m.MessageBox.Action.OK,
				onClose: function(oAction) {
					if(oAction === "OK"){
						//TODO Limpia los formularios
						oView.limpiaCampos();
						oView.handleNuevoBtn();
					}
				}
			});
		},	
		
		validaComboBox: function(oEvent){
			var oValidatedComboBox = oEvent.getSource(),
		    sSelectedKey = oValidatedComboBox.getSelectedKey(),
		    sValue = oValidatedComboBox.getValue();
		
			if(!sSelectedKey && sValue) {
				oValidatedComboBox.setValueState("Error");
				oValidatedComboBox.setValueStateText("Porfavor Ingresa un valor Valido!");
			} else {
				oValidatedComboBox.setValueState("None");
			}
			
			if(oValidatedComboBox.sId.lastIndexOf("cbxStatus") > 0){
				if(sValue == "Cancelado")
					this.byId("cbxU_SEI_MOTIVO_BAJA").setEnabled(true);
				else
					this.byId("cbxU_SEI_MOTIVO_BAJA").setEnabled(false);
			}
			
			return oValidatedComboBox;
		},
		
		limpiaCampos: function(){
			//DETALLES DE EQUIPO
			this.byId("inpInternalSNSearch").setValue();
			this.byId("inpInternalSNSet").setValue();
			this.byId("inpItemCodeSearch").setValue();
			this.byId("inpItemCode").setValue();
			this.byId("txtItemName").setValue();
			this.byId("inpItemCode").setValue();
			this.byId("cbxStatus").setValue().setSelectedKey("A");
			this.byId("inpInstallLocation").setValue();
			this.byId("cbxU_SEI_MOTIVO_BAJA").setSelectedKey("");
			this.byId("cbxU_SEI_MOTIVO_BAJA").setValue().setEnabled(false);
			

			//DETALLES DEL SOCIO NEGOCIO
			//this.byId("inpInternalSNSet").setValue();
			//this.byId("inpCustmrName").setValue();
			
			//DIRECCION
			this.byId("inpStreet").setValue();
			this.byId("cbxCountry").setSelectedKey("");
			this.byId("cbxState").setValue().setSelectedKey("");
			this.byId("cbxCity").setValue().setSelectedKey("");
			this.byId("cbxCounty").setValue().setSelectedKey("");
			
			//EQUIPO
			this.byId("inpU_SEI_Path_number").setValue();
			this.byId("inpU_SEI_Modelo").setValue();
			this.byId("inpU_SEI_Marca").setValue();
//			this.byId("inpU_SEI_Cod_Barra_Interno").setValue();
			this.byId("inpU_SEI_Ram").setValue();
			this.byId("inpU_SEI_CapHHD").setValue();
			this.byId("inpU_SEI_Procesador").setValue();
			this.byId("inpU_SEI_Version_SO").setValue();
			this.byId("inpU_SEI_IDProducto_SO").setValue();
			this.byId("inpU_SEI_key_SO").setValue();
			this.byId("inpU_SEI_Version_Office").setValue();
			this.byId("inpU_SEI_ID_Office").setValue();
			this.byId("inpU_SEI_Nro_Licencia_Office").setValue();
			this.byId("inpEquipmentCardNum").setValue();
			this.byId("inpU_SEI_IP").setValue();
			this.byId("cbxU_SEI_ANTIVIRUS").setValue().setSelectedKey("SI");
			this.byId("cbxU_SEI_MOTIVO_BAJA").setValue().setSelectedKey("");
			
			//FACTURACION
			this.byId("inpU_SEI_FAC").setValue();
			this.byId("inpU_SEI_Factura").setValue();
			this.byId("inpU_SEI_Garantia").setValue();
			this.byId("inpU_SEI_RUTPROV").setValue();
			this.byId("cbxU_SEI_Presupuesto").setValue().setSelectedKey("");
			this.byId("inpU_SEI_Fecha_Compra").setValue();
			
			//UBICACION
			this.byId("inpUbicacionSearch").setValue();
			this.byId("inpU_SEI_UBICACION1").setValue().setSelectedKey("");
			this.byId("inpU_SEI_UBICACION2").setValue().setSelectedKey("");
			this.byId("inpU_SEI_UBICACION3").setValue().setSelectedKey("");
			this.byId("inpU_SEI_UBICACION4").setValue().setSelectedKey("");
		},
		
		handleLoadItems: function(oControlEvent, idCbx) {
			var selText = oControlEvent.getParameter("selectedItem").getText();
			this.llenaUbicacion(selText, idCbx);
		},
		
		handleRegion : function(oEvent){
			var selReg = oEvent.getParameter("selectedItem").getKey();
			this.llenaCiudad(selReg);
		},
		
		handleCiudad : function(oEvent){
			var selCiu = oEvent.getParameter("selectedItem").getKey();
			this.llenaComuna(selCiu);
		},
		
		llenaUbicacion : function(selText, idCbx){
			var view = this.getView()
			var oDimenJSONData = null;
			oDimenJSONData = JSON.parse(sessionStorage.getItem("location"));
			
			var oModel = new sap.ui.model.json.JSONModel();
			var aTemp = [];
			var aDimension2 = [];
			var aDimension3 = [];
			var aDimension4 = [];			
			
			if(idCbx === "dimension1") {
				this._dimension1 = selText;
				for(var i = 0; i < oDimenJSONData.value.length; i++){
					var oDimensiones = oDimenJSONData.value[i];				
					if(oDimensiones.U_SEI_NIVEL1 === this._dimension1 && 
					   oDimensiones.U_SEI_NIVEL2 && 
					   jQuery.inArray(oDimensiones.U_SEI_NIVEL2, aTemp) < 0){						
						aTemp.push(oDimensiones.U_SEI_NIVEL2);
						aDimension2.push({"Name": oDimensiones.U_SEI_NIVEL2});						
					} else if(oDimensiones.U_SEI_NIVEL1 === this._dimension1 && 
							   !oDimensiones.U_SEI_NIVEL2 && 
							   jQuery.inArray(oDimensiones.U_SEI_NIVEL3, aTemp) < 0){
						aTemp.push(oDimensiones.U_SEI_NIVEL3);
						aDimension3.push({"Name": oDimensiones.U_SEI_NIVEL3});
					}
				}				
				oServiceCallsJSONData.Dimension2 = aDimension2;
				
				if(aDimension3)
					//provisorio
					oServiceCallsJSONData.Dimension3 = aDimension3;
				
				if(aDimension4)
					//provisorio
					oServiceCallsJSONData.Dimension4 = aDimension4;
				
			}else if(idCbx === "dimension2") {
				this._dimension2 = selText;
				for(var i = 0; i < oDimenJSONData.value.length; i++){
					var oDimensiones = oDimenJSONData.value[i];				
					if(oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
					   oDimensiones.U_SEI_NIVEL3 && 
					   jQuery.inArray(oDimensiones.U_SEI_NIVEL3, aTemp) < 0){						
						aTemp.push(oDimensiones.U_SEI_NIVEL3);
						aDimension3.push({"Name": oDimensiones.U_SEI_NIVEL3});						
					} else if(oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
							   !oDimensiones.U_SEI_NIVEL3 && 
							   jQuery.inArray(oDimensiones.U_SEI_NIVEL4, aTemp) < 0){
						aTemp.push(oDimensiones.U_SEI_NIVEL4);
						aDimension4.push({"Name": oDimensiones.U_SEI_NIVEL4});
					}
				}				
				oServiceCallsJSONData.Dimension3 = aDimension3;
				
				if(aDimension4)
					//provisorio
					oServiceCallsJSONData.Dimension4 = aDimension4
				
			} else if(idCbx === "dimension3") {
				this._dimension3 = selText;
				for(var i = 0; i < oDimenJSONData.value.length; i++){
					var oDimensiones = oDimenJSONData.value[i];				
					if(oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
					   oDimensiones.U_SEI_NIVEL3 === this._dimension3 && 
					   oDimensiones.U_SEI_NIVEL4 && 
					   jQuery.inArray(oDimensiones.U_SEI_NIVEL4, aTemp) < 0){
						
						aTemp.push(oDimensiones.U_SEI_NIVEL4);
						aDimension4.push({"Name": oDimensiones.U_SEI_NIVEL4});
					}
				}
				//provisorio
				oServiceCallsJSONData.Dimension4 = aDimension4;				
			}
			oModel.setData(oServiceCallsJSONData);
			view.setModel(oModel);
		},
		
		llenaCiudad : function(selReg){
			var view = this.getView();
			var oModel = new sap.ui.model.json.JSONModel();
			var oCiudades = null;
			
			filtro = "&filter=U_codreg eq '" + selReg +"'";
			select = "select=U_codigo,U_nombre,U_codreg";
			limit = "";
			getAsync('ciudades');
			
			oCiudades = JSON.parse(sessionStorage.getItem("ciudades"));
			oServiceCallsJSONData.Ciudades = oCiudades.value;
			oServiceCallsJSONData.Comunas = [];
			oModel.setData(oServiceCallsJSONData);
			view.setModel(oModel);
			this.getView().byId("cbxCity").setValue().setSelectedKey("");
			this.getView().byId("cbxCounty").setValue().setSelectedKey("");
		},
		
		llenaComuna : function(selCiu){
			var view = this.getView();
			var oModel = new sap.ui.model.json.JSONModel();
			var oComunas = null;
			
			filtro = "&filter=U_codpro eq '" + selCiu +"'";
			select = "select=U_codigo,U_nombre,U_codpro";
			limit = "";
			getAsync('comunas');
			
			oComunas = JSON.parse(sessionStorage.getItem("comunas"));
			oServiceCallsJSONData.Comunas = oComunas.value;
			oModel.setData(oServiceCallsJSONData);
			view.setModel(oModel);
			this.getView().byId("cbxCounty").setValue().setSelectedKey("");
		},
		
		handleTraeEqC : function(){
            getAsync("tarjetasEquipo");
            var oTarjetas = JSON.parse(sessionStorage.getItem("tarjetasEquipo"));
            
            
            if(oTarjetas.value.length > 0){
            	const items = oTarjetas.value;
                const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
                const header = Object.keys(items[0]);
                const csv = [
                  header.join(';'), // header row first
                  ...items.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(';'))
                ].join('\r\n');
                
                
                var bytes = new TextEncoder().encode(csv);
                var blob = new Blob([bytes], {type: 'text/csv;charset=utf-8'});
                if(window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveBlob(blob, filename);
                }
                else{
                    var elem = window.document.createElement('a');
                    elem.href = window.URL.createObjectURL(blob);
                    elem.download = 'TarjetasEquipo.csv';        
                    document.body.appendChild(elem);
                    elem.click();        
                    document.body.removeChild(elem);
                }
            }else{
            	new sap.m.MessageBox.show("No existen datos para descargar", {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Descarga Tarjetas de Equipo",
					onClose: function(oAction) {
						
					}
				});
            }
        },
		
		/**
		 * Funcion de Evento del campo Cliente para mostrar Socios de negocio. el que levanta un popup con una tabla, 
		 * en los que aparecen los rut y nombres de los socios de negocios para su posterior seleccion y carga
		 * en los campos Cliente y Nombre. 
		 */
		onValueHelpRequested: function() {
			this._oInput = this.getView().byId("inpCustomer");
			this.oColModel = new sap.ui.model.json.JSONModel();
			this.oColModel.setData(
					{
						"cols": [
							{ 
								"label": "Codigo", 
								"template": "CardCode", 
								"width": "10rem" 
							}, 
							{ 
								"label": "Nombre", 
								"template": "CardName" 
							}
						]
					}
			);
			
			var aCols = this.oColModel.getData().cols;
			this._oBasicSearchField = new sap.m.SearchField({showSearchButton: false});
			
			this._oValueHelpDialog = sap.ui.xmlfragment("SeidorB1UI5.view.fragment.SocioNegocio", this);
			this.getView().addDependent(this._oValueHelpDialog);

			this._oValueHelpDialog.getFilterBar().setBasicSearch(this._oBasicSearchField);
			
			this._oValueHelpDialog.getTableAsync().then(function (oTable) {
				oTable.setModel(this.oJSONModel);
				oTable.setModel(this.oColModel, "columns");

				if (oTable.bindRows) {
					oTable.bindAggregation("rows", "/BusinessPartners");
				}

				if (oTable.bindItems) {
					oTable.bindAggregation("items", "/BusinessPartners", function () {
						return new sap.m.ColumnListItem({
							cells: aCols.map(function (column) {
								return new sap.m.Label({ text: "{" + column.template + "}" });
							})
						});
					});
				}

				this._oValueHelpDialog.update();
			}.bind(this));

			var oToken = new sap.m.Token();
			oToken.setKey(this._oInput.getSelectedKey());
			oToken.setText(this._oInput.getValue());
			this._oValueHelpDialog.setTokens([oToken]);
			this._oValueHelpDialog.open();
		},

		onValueHelpRequestedNS: function() {
			this._oInput = this.getView().byId("inpInternalSNSearch");
			this.oColModel = new sap.ui.model.json.JSONModel();
			this.oColModel.setData(
					{
						"cols": [
							{ 
								"label": "Numero de Serie Interno", 
								"template": "InternalSerialNum", 
								"width": "10rem" 
							}, 
							{ 
								"label": "Codigo Producto", 
								"template": "ItemCode" 
							},
							{ 
								"label": "Descripcion Producto", 
								"template": "ItemDescription" 
							}
						]
					}
			);
			
			var aCols = this.oColModel.getData().cols;
			this._oBasicSearchField = new sap.m.SearchField({showSearchButton: false});
			
			this._oValueHelpDialog = sap.ui.xmlfragment("SeidorB1UI5.view.fragment.NumerosDeSerie", this);
			this.getView().addDependent(this._oValueHelpDialog);

			this._oValueHelpDialog.getFilterBar().setBasicSearch(this._oBasicSearchField);
			
			this._oValueHelpDialog.getTableAsync().then(function (oTable) {
				oTable.setModel(this.getView().getModel());
				oTable.setModel(this.oColModel, "columns");

				if (oTable.bindRows) {
					oTable.bindAggregation("rows", "/SerialNumbers");
				}

				if (oTable.bindItems) {
					oTable.bindAggregation("items", "/SerialNumbers", function () {
						return new sap.m.ColumnListItem({
							cells: aCols.map(function (column) {
								return new sap.m.Label({ text: "{" + column.template + "}" });
							})
						});
					});
				}

				this._oValueHelpDialog.update();
			}.bind(this));

			var oToken = new sap.m.Token();
			oToken.setKey(this._oInput.getSelectedKey());
			oToken.setText(this._oInput.getValue());
			this._oValueHelpDialog.setTokens([oToken]);
			this._oValueHelpDialog.open();
		},
		
		onValueHelpRequestedAF: function() {
			this._oInput = this.getView().byId("inpItemCodeSearch");
			this.oColModel = new sap.ui.model.json.JSONModel();
			this.oColModel.setData(
					{
						"cols": [
							{ 
								"label": "Codigo Producto", 
								"template": "ItemCode" 
							},
							{ 
								"label": "Descripcion Producto", 
								"template": "ItemName" 
							}
						]
					}
			);
			
			var aCols = this.oColModel.getData().cols;
			this._oBasicSearchField = new sap.m.SearchField({showSearchButton: false});
			
			this._oValueHelpDialog = sap.ui.xmlfragment("SeidorB1UI5.view.fragment.ActivosFijos", this);
			this.getView().addDependent(this._oValueHelpDialog);

			this._oValueHelpDialog.getFilterBar().setBasicSearch(this._oBasicSearchField);
			
			this._oValueHelpDialog.getTableAsync().then(function (oTable) {
				oTable.setModel(this.getView().getModel());
				oTable.setModel(this.oColModel, "columns");

				if (oTable.bindRows) {
					oTable.bindAggregation("rows", "/Items");
				}

				if (oTable.bindItems) {
					oTable.bindAggregation("items", "/Items", function () {
						return new sap.m.ColumnListItem({
							cells: aCols.map(function (column) {
								return new sap.m.Label({ text: "{" + column.template + "}" });
							})
						});
					});
				}

				this._oValueHelpDialog.update();
			}.bind(this));

			var oToken = new sap.m.Token();
			oToken.setKey(this._oInput.getSelectedKey());
			oToken.setText(this._oInput.getValue());
			this._oValueHelpDialog.setTokens([oToken]);
			this._oValueHelpDialog.open();
		},
		
		onValueHelpRequestedEF: function() {
			this._oInput = this.getView().byId("inpUbicacionSearch");
			this.oColModel = new sap.ui.model.json.JSONModel();
			this.oColModel.setData(
					{
						"cols": [
							{ 
								"label": "Codigo Ubicacion", 
								"template": "Code" 
							},
							{ 
								"label": "Nivel 1", 
								"template": "U_SEI_NIVEL1" 
							},
							{ 
								"label": "Nivel 2", 
								"template": "U_SEI_NIVEL2" 
							},
							{ 
								"label": "Nivel 3", 
								"template": "U_SEI_NIVEL3" 
							},
							{ 
								"label": "Nivel 4", 
								"template": "U_SEI_NIVEL4" 
							}
						]
					}
			);
			
			var aCols = this.oColModel.getData().cols;
			this._oBasicSearchField = new sap.m.SearchField({showSearchButton: false});
			
			this._oValueHelpDialog = sap.ui.xmlfragment("SeidorB1UI5.view.fragment.EstructuraFisica", this);
			this.getView().addDependent(this._oValueHelpDialog);

			//this._oValueHelpDialog.getFilterBar().setBasicSearch(this._oBasicSearchField);
			
			this._oValueHelpDialog.getTableAsync().then(function (oTable) {
				oTable.setModel(this.getView().getModel());
				oTable.setModel(this.oColModel, "columns");

				if (oTable.bindRows) {
					oTable.bindAggregation("rows", "/PhisicalStructure");
				}

				if (oTable.bindItems) {
					oTable.bindAggregation("items", "/PhisicapStructure", function () {
						return new sap.m.ColumnListItem({
							cells: aCols.map(function (column) {
								return new sap.m.Label({ text: "{" + column.template + "}" });
							})
						});
					});
				}

				this._oValueHelpDialog.update();
			}.bind(this));

			var oToken = new sap.m.Token();
			oToken.setKey(this._oInput.getSelectedKey());
			oToken.setText(this._oInput.getValue());
			this._oValueHelpDialog.setTokens([oToken]);
			this._oValueHelpDialog.open();
		},
		
		onFilterBarSearch: function (oEvent) {
			var sSearchQuery = this._oBasicSearchField.getValue(),
				aSelectionSet = oEvent.getParameter("selectionSet");
			var aFilters = aSelectionSet.reduce(function (aResult, oControl) {
				if (oControl.getValue()) {
					aResult.push(new sap.ui.model.Filter({
						path: oControl.getName(),
						operator: sap.ui.model.FilterOperator.Contains,
						value1: oControl.getValue()
					}));
				}

				return aResult;
			}, []);

			aFilters.push(new sap.ui.model.Filter({
				filters: [
					new sap.ui.model.Filter({ path: "CardCode", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery }),
					new sap.ui.model.Filter({ path: "CardName", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery })
				],
				and: false
			}));

			this._filterTable(new sap.ui.model.Filter({
				filters: aFilters,
				and: true
			}));
		},
		
		onFilterBarSearchNS: function (oEvent) {
			var sSearchQuery = this._oBasicSearchField.getValue(),
				aSelectionSet = oEvent.getParameter("selectionSet");
			var aFilters = aSelectionSet.reduce(function (aResult, oControl) {
				if (oControl.getValue()) {
					aResult.push(new sap.ui.model.Filter({
						path: oControl.getName(),
						operator: sap.ui.model.FilterOperator.Contains,
						value1: oControl.getValue()
					}));
				}

				return aResult;
			}, []);

			aFilters.push(new sap.ui.model.Filter({
				filters: [
					new sap.ui.model.Filter({ path: "InternalSerialNum", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery }),
					new sap.ui.model.Filter({ path: "U_SEI_Cod_Barra_Interno", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery }),
					new sap.ui.model.Filter({ path: "ItemDescription", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery })
				],
				and: false
			}));

			this._filterTable(new sap.ui.model.Filter({
				filters: aFilters,
				and: true
			}));
		},
		
		onFilterBarSearchAF: function (oEvent) {
			var sSearchQuery = this._oBasicSearchField.getValue(),
				aSelectionSet = oEvent.getParameter("selectionSet");
			var aFilters = aSelectionSet.reduce(function (aResult, oControl) {
				if (oControl.getValue()) {
					aResult.push(new sap.ui.model.Filter({
						path: oControl.getName(),
						operator: sap.ui.model.FilterOperator.Contains,
						value1: oControl.getValue()
					}));
				}

				return aResult;
			}, []);

			aFilters.push(new sap.ui.model.Filter({
				filters: [
					new sap.ui.model.Filter({ path: "ItemCode", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery }),
					new sap.ui.model.Filter({ path: "ItemName", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery })
				],
				and: false
			}));

			this._filterTable(new sap.ui.model.Filter({
				filters: aFilters,
				and: true
			}));
		},
  
		onFilterBarSearchEF: function (oEvent) {
			var sSearchQuery = this._oBasicSearchField.getValue(),
				aSelectionSet = oEvent.getParameter("selectionSet");
			var aFilters = aSelectionSet.reduce(function (aResult, oControl) {
				if (oControl.getValue()) {
					aResult.push(new sap.ui.model.Filter({
						path: oControl.getName(),
						operator: sap.ui.model.FilterOperator.Contains,
						value1: oControl.getValue()
					}));
				}

				return aResult;
			}, []);

			aFilters.push(new sap.ui.model.Filter({
				filters: [
					new sap.ui.model.Filter({ path: "ItemCode", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery }),
					new sap.ui.model.Filter({ path: "ItemName", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery })
				],
				and: false
			}));

			this._filterTable(new sap.ui.model.Filter({
				filters: aFilters,
				and: true
			}));
		},
		
		_filterTable: function (oFilter) {
			var oValueHelpDialog = this._oValueHelpDialog;

			oValueHelpDialog.getTableAsync().then(function (oTable) {
				if (oTable.bindRows) {
					oTable.getBinding("rows").filter(oFilter);
				}

				if (oTable.bindItems) {
					oTable.getBinding("items").filter(oFilter);
				}

				oValueHelpDialog.update();
			});
		},
		
		/**
		 * Funcion de Evento Seleccion de Linea de la tabla. que carga los valores en los campos
		 * Cliente - Nombre y llama a los acuerdos globales asociados al socio de negocio seleccionado
		 * para llenar los datos en combobox Acuedo globales.
		 */
		onValueHelpOkPress: function (oEvent) {
			sap.ui.core.BusyIndicator.show(0);
			
			var aTokens = oEvent.getParameter("tokens");
//			this._oInput.setValue(aTokens[0].getKey());
			this._oInput.setValue(aTokens[0].getKey());
			var nom = aTokens[0].getText();
			nom = nom.replace(" ("+ aTokens[0].getKey() +")", "");
			this.byId("inpCustmrName").setValue(nom);
			
			sap.ui.core.BusyIndicator.hide();
			
			this._oValueHelpDialog.close();
		},
		
		/**
		 * Funcion de Evento Seleccion de Linea de la tabla. que carga los valores en los campos
		 * Cliente - Nombre y llama a los acuerdos globales asociados al socio de negocio seleccionado
		 * para llenar los datos en combobox Acuedo globales.
		 */
		onValueHelpOkPressNS: function (oEvent) {
			sap.ui.core.BusyIndicator.show(0);
			
			var aTokens = oEvent.getParameter("tokens");
			this._oInput.setValue(aTokens[0].getKey());
			var nom = aTokens[0].getText();
			nom = nom.replace(" ("+ aTokens[0].getKey() +")", "");
			this.byId("inpItemCode").setValue(nom);
			
			//LLENAR DATOS DE TARJETA
			//inpInternalSNSet
			this._oCard = this.getView().getModel().getData().SerialNumbers;
			
			for(var i = 0; i< this._oCard.length; i++){
				var card = this._oCard[i];
				
				if(card.InternalSerialNum === this._oInput.getValue()){
					//this.byId("").setValue(card.); //para input y combobox
					//this.byId("").setText(card.); // para textarea
					equipmentCardNum = card.EquipmentCardNum;
					
					this.byId("txtItemName").setValue(card.ItemDescription);

					this.byId("inpItemCode").setValue(card.ItemCode);
//					this.byId("inpCustomer").setValue(card.CustomerCode);
					this.byId("inpCustmrName").setValue(card.CustomerCode +';'+ card.CustomerName);
					//this.byId("inpCustomer").setValue(card.CustomerCode);
					if(card.StatusOfSerialNumber == "sns_Active"){
						this.byId("cbxStatus").setValue().setSelectedKey("A");
					}else if(card.StatusOfSerialNumber == "sns_Returned"){
						this.byId("cbxStatus").setValue().setSelectedKey("R");
					}else if(card.StatusOfSerialNumber == "sns_Terminated"){
						this.byId("cbxStatus").setValue().setSelectedKey("T");
						this.byId("cbxU_SEI_MOTIVO_BAJA").setEnabled(true);
						this.byId("cbxU_SEI_MOTIVO_BAJA").setSelectedKey(card.U_SEI_MOTIVO_BAJA);
					}else if(card.StatusOfSerialNumber == "sns_Loaned"){
						this.byId("cbxStatus").setValue().setSelectedKey("L");
					}else{
						this.byId("cbxStatus").setValue().setSelectedKey("I");
					}
					
					this.byId("inpInstallLocation").setValue(card.InstallLocation);
					
					this.byId("cbxCountry").setSelectedKey(card.CountryCode);
					this.byId("inpStreet").setValue(card.Street);
					this.byId("cbxState").setValue().setSelectedKey(card.StateCode);
					//LLENAR CMB DE CIUDADES
					this.llenaCiudad(card.StateCode);
					for (var i = 0; i < oServiceCallsJSONData.Ciudades.length; i++){
						if(oServiceCallsJSONData.Ciudades[i].U_nombre == card.City){
							this.byId("cbxCity").setValue().setSelectedKey(oServiceCallsJSONData.Ciudades[i].U_codigo);
							break;
						}
					}
					//LENAR CMB DE COMUNAS
					this.llenaComuna(this.byId("cbxCity").getSelectedKey());
					for (var i = 0; i < oServiceCallsJSONData.Comunas.length; i++){
						if(oServiceCallsJSONData.Comunas[i].U_nombre == card.County){
							this.byId("cbxCounty").setValue().setSelectedKey(oServiceCallsJSONData.Comunas[i].U_codigo);
							break;
						}
					}
					
					
					this.byId("inpU_SEI_Path_number").setValue(card.U_SEI_Path_number);
					this.byId("inpU_SEI_Modelo").setValue(card.U_SEI_Modelo);
					this.byId("inpU_SEI_Marca").setValue(card.U_SEI_Marca);
					//this.byId("inpU_SEI_Cod_Barra_Interno").setValue(card.U_SEI_Cod_Barra_Interno);
					this.byId("inpU_SEI_Ram").setValue(card.U_SEI_Ram);
					this.byId("inpU_SEI_CapHHD").setValue(card.U_SEI_CapHHD);
					this.byId("inpU_SEI_Procesador").setValue(card.U_SEI_Procesador);
					this.byId("inpU_SEI_Version_SO").setValue(card.U_SEI_Version_SO);
					this.byId("inpU_SEI_IDProducto_SO").setValue(card.U_SEI_IDProducto_SO);
					this.byId("inpU_SEI_key_SO").setValue(card.U_SEI_key_SO);
					this.byId("inpU_SEI_Version_Office").setValue(card.U_SEI_Version_Office);
					this.byId("inpU_SEI_ID_Office").setValue(card.U_SEI_ID_Office);
					this.byId("inpU_SEI_Nro_Licencia_Office").setValue(card.U_SEI_Nro_Licencia_Office);
					this.byId("inpEquipmentCardNum").setValue(card.EquipmentCardNum);
					this.byId("inpU_SEI_IP").setValue(card.U_SEI_IP);
					this.byId("cbxU_SEI_ANTIVIRUS").setValue().setSelectedKey(card.U_SEI_ANTIVIRUS);
					this.byId("cbxU_SEI_MOTIVO_BAJA").setValue().setSelectedKey(card.U_SEI_MOTIVO_BAJA);
					
					this.byId("inpU_SEI_FAC").setValue(card.U_SEI_FAC);
					this.byId("inpU_SEI_Factura").setValue(card.U_SEI_Factura);
					this.byId("inpU_SEI_Garantia").setValue(card.U_SEI_Garantia);
					this.byId("inpU_SEI_RUTPROV").setValue(card.U_SEI_RUTPROV);
					this.byId("cbxU_SEI_Presupuesto").setValue().setSelectedKey(card.U_SEI_Presupuesto);
					this.byId("inpU_SEI_Fecha_Compra").setValue(card.U_SEI_Fecha_Compra);			
					
					for (var i = 0; i < oServiceCallsJSONData.PhisicalStructure.length; i++){
						if(oServiceCallsJSONData.PhisicalStructure[i].Code == parseInt(card.U_SEI_UBICACION_PRINCIPAL)){
							this.byId("inpU_SEI_UBICACION1").setValue(oServiceCallsJSONData.PhisicalStructure[i].U_SEI_NIVEL1);
							this.byId("inpU_SEI_UBICACION2").setValue(oServiceCallsJSONData.PhisicalStructure[i].U_SEI_NIVEL2);
							this.byId("inpU_SEI_UBICACION3").setValue(oServiceCallsJSONData.PhisicalStructure[i].U_SEI_NIVEL3);
							this.byId("inpU_SEI_UBICACION4").setValue(oServiceCallsJSONData.PhisicalStructure[i].U_SEI_NIVEL4);
							this.byId("inpUbicacionSearch").setValue(oServiceCallsJSONData.PhisicalStructure[i].Code);
							
							//this.byId("cbxCounty").setValue().setSelectedKey(oServiceCallsJSONData.Comunas[i].U_codigo);
							break;
						}
					}
					
					
					break;
				}
			}
			
			sap.ui.core.BusyIndicator.hide();
			
			this._oValueHelpDialog.close();
			
		},
		
		onValueHelpOkPressAF: function (oEvent) {
			sap.ui.core.BusyIndicator.show(0);
			
			var aTokens = oEvent.getParameter("tokens");
//			this._oInput.setValue(aTokens[0].getKey());
			this._oInput.setValue(aTokens[0].getKey());
			var nom = aTokens[0].getText();
			nom = nom.replace(" ("+ aTokens[0].getKey() +")", "");
			this.byId("inpItemCodeSearch").setValue(aTokens[0].getKey());
			this.byId("txtItemName").setValue(nom);
			
			sap.ui.core.BusyIndicator.hide();
			
			this._oValueHelpDialog.close();
		},
		
		onValueHelpOkPressEF: function (oEvent) {
			sap.ui.core.BusyIndicator.show(0);
			
			var aTokens = oEvent.getParameter("tokens");
			this._oInput.setValue(aTokens[0].getKey());
			this.byId("inpUbicacionSearch").setValue(aTokens[0].getKey());
			this._oCard = this.getView().getModel().getData().PhisicalStructure;
			
			for(var i = 0; i< this._oCard.length; i++){
				var card = this._oCard[i];
				
				if(card.Code === parseInt(this._oInput.getValue())){
					this.byId("inpU_SEI_UBICACION1").setValue(card.U_SEI_NIVEL1);
					this.byId("inpU_SEI_UBICACION2").setValue(card.U_SEI_NIVEL2);
					this.byId("inpU_SEI_UBICACION3").setValue(card.U_SEI_NIVEL3);
					this.byId("inpU_SEI_UBICACION4").setValue(card.U_SEI_NIVEL4);
				}
			}
			
			sap.ui.core.BusyIndicator.hide();
			
			this._oValueHelpDialog.close();
		},

		/**
		 * Funcion de Evento Boton Cancelar. el que llama a la funcion para el cierre 
		 * de la ventana oValueHelpDialog.
		 */
		onValueHelpCancelPress: function () {
			this._oValueHelpDialog.close();
		},

		onValueHelpAfterClose: function () {
			this._oValueHelpDialog.destroy();
		},
		
		onExit: function() {
			if (this._oActionSheet) {
				this._oActionSheet.destroy();
				this._oActionSheet = null;
			}
			//this.oProductsModel.destroy();
		}
	});
	return;
});