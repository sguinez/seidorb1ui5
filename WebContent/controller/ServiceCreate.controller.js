sap.ui.define([
	"jquery.sap.global",
	"sap/m/MessageToast",
	"./Formatter",
	"sap/ui/core/Fragment",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/Filter",	
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox"
	
], function(jQuery, MessageToast, Fragment, Formatter, Controller, FilterOperator, Filter, JSONModel, MessageBox) {
	"use strict";

	var ServiceCreateController = Controller.extend("SeidorB1UI5.controller.ServiceCreate", {
		
		mensaje: null,
		oTable: null,
		oItems:null,
		oJSONModel: null,
		codigo: null,
		descripcion: null,
		fecha1: null,
		fecha2: null,
		unidad: null,
		cardCode: null,
		
		oModelSel: null,
		oSelectD2: null,
		oSelectD3: null,
		
		onInit: function() {
			
			if((oDatosUsuario === undefined || !oDatosUsuario) && sessionStorage.getItem("tiles")){
				oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
			} else if(!sessionStorage.getItem("tiles")){
				this.handleLogout();
			}
			
			var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
			this.byId("avatar").setInitials(iAvatar);
			this.byId("avatar").setTooltip("Usuario: " + avatar);
			
			oDimenJSONData = JSON.parse(sessionStorage.getItem("location"));
			oEspecialidad = JSON.parse(sessionStorage.getItem("serviceEspecialidades"));
			oServiceJSONData = JSON.parse(sessionStorage.getItem("services"));
			oServiceJSONData.departamentos = JSON.parse(sessionStorage.getItem('departamentos')).value;
			oServiceJSONData.tipoFinanciamientos = JSON.parse(sessionStorage.getItem('tipoFinanciamientos')).value;
			getAsync("numerosDeSerie");
			oServiceJSONData.SerialNumbers =  JSON.parse(sessionStorage.getItem("numerosDeSerie")).value;
			
			this.oJSONModel = this.loadCombobox();			
			this.byId("DP1").setDateValue(new Date());
			this.fecha1 = this.byId("DP1").getDateValue();
			this.byId("txtSolicitante").setText(JSON.parse(sessionStorage.getItem("tiles")).user_name);
			this.byId("txtUnidad").setText(JSON.parse(sessionStorage.getItem("tiles")).des_suc);
			this.byId("cbxUbicacion").setValue("Selecciona Ubicacion");
			
			this.getView().setModel(this.oJSONModel);
			var oView = this.getView();
			var oData = oView.getModel().getData();				
			var Activities = {Activities:[]};
			oData.Activities = Activities.Activities;
			oView.getModel().setData(oData);
			this.byId("cbxTipoFinanciamiento").setValue(JSON.parse(sessionStorage.getItem("tiles")).des_fin).setSelectedKey(JSON.parse(sessionStorage.getItem("tiles")).cod_fin);
			//this.getView().byId("cbxEspecialidad").getSelectedItem().setText("Selecciona Especialidad")
//			this.byId("cbxEspecialidad").setValue("Selecciona Especialidad");
			
		},
		
		handleLogout: function (evt) {
			sessionStorage.clear();
			delCookies();
			window.location.replace("");
		},
		
		handleEspecialidad: function(oEvent){
			var selEsp = oEvent.getParameter("selectedItem").getKey();
			
			this.byId("inpInternalSNSearch").setEnabled(selEsp.indexOf("SINFO") >= 0 ? true : false);
		},
		
		/*
		 * Carga combobox de la ubicacion nivel 1
		 */
		loadCombobox: function(){
			var oModel = new JSONModel();
			var aTemp = [];
			var oUbicacion1 = [];		
			for(var i = 0; i < oDimenJSONData.value.length; i++){
				var oUbicaciones = oDimenJSONData.value[i];				
				if(oUbicaciones.U_SEI_NIVEL1 && jQuery.inArray(oUbicaciones.U_SEI_NIVEL1, aTemp) < 0){
					aTemp.push(oUbicaciones.U_SEI_NIVEL1);
					oUbicacion1.push({"Name": oUbicaciones.U_SEI_NIVEL1});
				}
			}
			//provisorio
			oServiceJSONData.Ubicacion1 = oUbicacion1;
			oServiceJSONData.Especialidad = oEspecialidad.value;
			oModel.setData(oServiceJSONData);			
			return oModel;
		},
		
		onExit: function() {
			if (this._oDialog) {
//				this._oDialog.destroy();
			}
		},
		
		handleNavButton: function(oEvent) {
			var sPreviousHash = sap.ui.core.routing.History.getInstance().getPreviousHash();
			
			var items = this.byId("idItemsTable").getItems();
			this.onDelete(items);			
			this.limpiarCampos();
			this.getOwnerComponent().getRouter().navTo("ServiceMaster", oPurchaseJSONData, true);
			//The history contains a previous entry
//			if (sPreviousHash !== undefined) {
//				window.history.go(-1);
//			} else {
//				// There is no history!
//				// replace the current hash with page 1 (will not add an history entry)
//				this.getOwnerComponent().getRouter().navTo("ServiceMaster", oPurchaseJSONData, true);
//			}
		},

		/*
		 * Segun eleccion del combobox Unidad Nivel 1 cambia el cardname y contenido de combobox nivel 2
		 */		
		validaCambioUbicacion: function(){
			// Si hay datos en la tabla pregunta si quiere cambiar de colegio
			// si elige si se borraran los datos de la tabla creados
			var cbxUni = this.getView().byId("cbxUbicacion");
			var oTable = this.getView().byId("idItemsTable");
			var cantLineas = oTable.getItems().length;
		    var data = oTable.getModel();
		    if(cantLineas > 0){
				new MessageBox.warning("Si cambias la Ubicacion Principal, Se Perderan las Ubicaciones Creadas", {
					icon: MessageBox.Icon.WARNING,
					title: "Informacion Crear Llamada se Servicio",
					actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
			    		if(oAction === "OK"){
			    			for (var i = 0; i <= cantLineas; i++) {
								var oData = data.getData();
								oData.newItems.splice(0, 1);
								data.setData(oData);
								data.refresh();
						    }
			    		} else {
			    			cbxUni.setValue(JSON.parse(sessionStorage.getItem("tiles")).des_suc);
			    		}
					}
				});
			} else {
				this.handleUnidad();
			}
		},
		
		/*
		 * carga de combobox unidad 2 segun la unidad seleccionada en cabecera
		 */
		handleUnidad: function(oEvent) {
			this.unidad = this.byId("cbxUbicacion").getValue();
			
			var aTemp = [];
			var aDimension2 = [];		
			for(var i = 0; i < oDimenJSONData.value.length; i++){
				var oDimensiones = oDimenJSONData.value[i];				
				if(oDimensiones.U_SEI_NIVEL1 === this.unidad && 
				   oDimensiones.U_SEI_NIVEL2 && jQuery.inArray(oDimensiones.U_SEI_NIVEL2, aTemp) < 0){
					aTemp.push(oDimensiones.U_SEI_NIVEL2);
					aDimension2.push({"Name": oDimensiones.U_SEI_NIVEL2});
					this.cardCode = oDimensiones.U_SEI_COLEGIO;
				}
			}
			
			oDatosUsuario.socio_cod = this.cardCode;
			oDatosUsuario.cole_cod = this.cardCode;
			getAsync("sociosNegocio");
			var oBusinessPartner = JSON.parse(sessionStorage.getItem("sociosNegocio"));
			this.getView().byId("txtUnidad").setText(oBusinessPartner.CardName);
			
			oServiceJSONData.Dimension2 = aDimension2;
			this.getView().getModel().getData().Dimension2 = aDimension2;
		},
		
		/*
		 * Asigna Nueva fecha cuando se seleccione en el datepicker
		 */
		handleChange: function(oEvent) {
			this.fecha1 = oEvent.getParameter("value");
		},
		
		/*
		 * Boton Aceptar para crear Llamada de Servicio
		 */
		handleAceptBtn: function(oEvent) {
			sap.ui.core.BusyIndicator.show(0);			
			this.validacionEnvio();
			
			if(this.mensaje === null) {
				var serviceCall = {};
				var items = this.getView().byId("idItemsTable").getItems();
				
				
				if(this.getView().getModel().getData().Activities.length){
					var activityCode = this.setActivity();
					serviceCall.ServiceCallActivities = [{"ActivityCode": activityCode}]
				}
				
				serviceCall.Status						= 1;
				serviceCall.CustomerCode				= this.cardCode;
				serviceCall.AssigneeCode 				= user_id;
				serviceCall.Priority 					= "M";
				serviceCall.CreationDate 				= this.fecha1;
				serviceCall.U_SEI_ESPECIALIDADES 		= this.byId("cbxEspecialidad").getSelectedKey();
				serviceCall.Subject 					= this.byId("txtAsunto").getValue();
				serviceCall.Description 				= this.byId("txtComentarios").getValue();
				serviceCall.U_SEI_UBICACION1 			= this.unidad;//unidad
				serviceCall.InternalSerialNum 			= this.byId("inpInternalSNSearch").getEnabled() == false ? undefined : this.byId("inpInternalSNSearch").getValue();
				//serviceCall.ManufacturerSerialNum		= this.byId("inpInternalSNSearch").getEnabled() == false ? undefined : this.byId("inpInternalSNSearch").getValue();
				serviceCall.ItemCode					= this._nom;
				
				serviceCall.U_SEI_COD_SUC				= oDatosUsuario.cod_suc;
				serviceCall.U_SEI_TIPO_FINANCIAMIENTO	= this.byId("cbxTipoFinanciamiento").getSelectedKey();
				serviceCall.U_SEI_DEPARTAMENTO			= this.byId("cbxDepartamento").getSelectedKey();
				
				for(var i = 0; i < items.length; i++){
					//1 2 - 10 4
					switch(i){
						case 0:
							serviceCall.U_SEI_UBIC12 = items[i].getCells()[0].mProperties.value;
							serviceCall.U_SEI_UBIC13 = items[i].getCells()[1].mProperties.value;
							serviceCall.U_SEI_UBIC14 = items[i].getCells()[2].mProperties.value;
							break;
						case 1:
							serviceCall.U_SEI_UBIC22 = items[i].getCells()[0].mProperties.value;
							serviceCall.U_SEI_UBIC23 = items[i].getCells()[1].mProperties.value;
							serviceCall.U_SEI_UBIC24 = items[i].getCells()[2].mProperties.value;
							break;
						case 2:
							serviceCall.U_SEI_UBIC32 = items[i].getCells()[0].mProperties.value;
							serviceCall.U_SEI_UBIC33 = items[i].getCells()[1].mProperties.value;
							serviceCall.U_SEI_UBIC34 = items[i].getCells()[2].mProperties.value;
							break;
						case 3:
							serviceCall.U_SEI_UBIC42 = items[i].getCells()[0].mProperties.value;
							serviceCall.U_SEI_UBIC43 = items[i].getCells()[1].mProperties.value;
							serviceCall.U_SEI_UBIC44 = items[i].getCells()[2].mProperties.value;
							break;
						case 4:
							serviceCall.U_SEI_UBIC52 = items[i].getCells()[0].mProperties.value;
							serviceCall.U_SEI_UBIC53 = items[i].getCells()[1].mProperties.value;
							serviceCall.U_SEI_UBIC54 = items[i].getCells()[2].mProperties.value;
							break;
						case 5:
							serviceCall.U_SEI_UBIC62 = items[i].getCells()[0].mProperties.value;
							serviceCall.U_SEI_UBIC63 = items[i].getCells()[1].mProperties.value;
							serviceCall.U_SEI_UBIC64 = items[i].getCells()[2].mProperties.value;
							break;
						case 6:
							serviceCall.U_SEI_UBIC72 = items[i].getCells()[0].mProperties.value;
							serviceCall.U_SEI_UBIC73 = items[i].getCells()[1].mProperties.value;
							serviceCall.U_SEI_UBIC74 = items[i].getCells()[2].mProperties.value;
							break;
						case 7:
							serviceCall.U_SEI_UBIC82 = items[i].getCells()[0].mProperties.value;
							serviceCall.U_SEI_UBIC83 = items[i].getCells()[1].mProperties.value;
							serviceCall.U_SEI_UBIC84 = items[i].getCells()[2].mProperties.value;
							break;
						case 8:
							serviceCall.U_SEI_UBIC92 = items[i].getCells()[0].mProperties.value;
							serviceCall.U_SEI_UBIC93 = items[i].getCells()[1].mProperties.value;
							serviceCall.U_SEI_UBIC94 = items[i].getCells()[2].mProperties.value;
							break;
						case 9:
							serviceCall.U_SEI_UBIC102 = items[i].getCells()[0].mProperties.value;
							serviceCall.U_SEI_UBIC103 = items[i].getCells()[1].mProperties.value;
							serviceCall.U_SEI_UBIC104 = items[i].getCells()[2].mProperties.value;
							break;
					}
				}			
				var serviceCallID = setService(serviceCall);
				if(serviceCallID){
					if(this.getView().getModel().getData().Activities.length){
						this.updateActivity(activityCode, serviceCallID);
					}
					this.onDelete(items);
					this.handleDelete();
					this.limpiarCampos();
					
//					var loRouter = sap.ui.core.UIComponent.getRouterFor(this);
//					loRouter.navTo("ServiceMaster", { }, onInit);
								    	
					sap.ui.core.BusyIndicator.hide();
				}
			} else {
				sap.m.MessageBox.show(this.mensaje, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Informacion",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		},
		
		/*
		 * al presional el boton cancelar se eliminan todas las lineas creadas y se
		 * reestablecen los campos de cabecera como inicialmente se encontraban
		 */
		handleCancelBtn: function(oEvent) {
			var oTable = this.getView().byId("idItemsTable");
		    //var selRowCount = oTable.getModel().getData().newItems.length;
		    if(oTable.getModel().getData().newItems !== undefined){
				var items = oTable.getItems();
				this.onDelete(items);			
				this.limpiarCampos();
			} else {
				new MessageBox.show("No hay lineas creadas a eliminar", {
					icon: MessageBox.Icon.WARNING,
					title: "Informacion Crear Llamada se Servicio",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		},
		
		/*
		 * Reestablece los campos a de la cabecera a como estaban inicialmente la pagina
		 */
		limpiarCampos: function(){			
			//this.byId("cbxUBicacion").setValue(JSON.parse(sessionStorage.getItem("tiles")).des_suc);
			this.byId("DP1").setDateValue(new Date());
			this.byId("cbxEspecialidad").setValue("Selecciona Especialidad");
			this.byId("txtAsunto").setValue("");
			this.byId("txtComentarios").setValue("");
			this.byId("inpInternalSNSearch").setValue("");
			this.byId("cbxUbicacion").setSelectedKey("").setValue("Selecciona Ubicacion");
			this.byId("inpInternalSNSearch").setEnabled(false);
		},
		
		/*
		 * Elimina celdas seleccionadas de la tabla
		 */
		handleEliminBtn: function(oEvent) {
			var oTable = this.getView().byId("idItemsTable");
		    var data = oTable.getModel();
		    var selRowCount = oTable.getSelectedItems().length;
		    
		    if(selRowCount > 0){
			    for (var i = 0; i < selRowCount; i++) {
			        var rowNum = oTable.getSelectedItems()[i].getBindingContext().getPath().replace("/newItems/", "");
					var oData = data.getData();
					oData.newItems.splice(rowNum, 1);
					data.setData(oData);
					data.refresh();
			    }
//			    var rows = data.getData().newItems.length;
//				this.getView().byId("idItemsTable").setVisibleRowCount(rows);
			} else {
				new MessageBox.show("No se ha seleccionado lineas a eliminar", {
					icon: MessageBox.Icon.WARNING,
					title: "Informacion Crear Llamada se Servicio",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		},
		/*
		 * Borra todas las filas de la tabla
		 */
		onDelete: function(items){
			for(var i = 0; i < items.length; i++){			
				var  oModel = this.getView().byId("idItemsTable").getModel();
				var oData = oModel.getData();
				var removed = oData.newItems.splice(0, 1);
				oModel.setData(oData);
				oModel.refresh();
			}
		},
		
		validacionEnvio: function() {
			this.mensaje = null;
			//Obtengo los Items de la Tabla
			var items = this.getView().byId("idItemsTable").getItems();
			//Valida Datos de la cabecera
			if(this.getView().byId("cbxUbicacion").getValue() === "Selecciona Ubicacion")
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -Debes Seleccionar una Ubicacion." : "-Debes Seleccionar una Ubicacion");
			
			if(this.fecha1 === null)
				this.mensaje = "- Se debe seleccionar fecha de Solicitud.";
			
			if(this.getView().byId("cbxEspecialidad").getSelectedItem().getText() === "Selecciona Especialidad")
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -Debes Seleccionar una Especialidad." : "-Debes Seleccionar una Especialidad");
				
			
			if(!this.getView().byId("txtAsunto").getValue() || !this.getView().byId("txtComentarios").getValue())
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -No hay Asunto y/o Comentarios para crear una Llamada." : "-No hay Asunto y/o Comentarios para crear una Llamada.");
				
			//Valida datos de la Tabla
			if(items.length === 0)
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -No hay Ubicaciones Seleccionadas para crear una Llamada." : "-No hay Ubicaciones Seleccionadas para crear una Llamada.");
			
			this.validaUbicaciones("envio");
			
//			for(var i = 0; i < items.length; i++){
//				if(!items[items.length-1].getCells()[0].getValue() || !items[items.length-1].getCells()[1].getValue()){
//				    this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -Debe seleccionar Ubicaciones." : "- Debe seleccionar Ubicaciones.");
//				} else if(oServiceJSONData.Dimension4.length > 0 && items[items.length-1].getCells()[2].getValue() !== ""){
//				            this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -Debe seleccionar Ubicaciones." : "- Debe seleccionar Ubicaciones.");                    
//				}
//			}
		},
		
		validaUbicaciones: function(tipo){
			if(tipo === "agrega")
				this.mensaje = null;
			
			var items = this.getView().byId("idItemsTable").getItems();
			for(var i = 0; i < items.length; i++){
				if(!items[items.length-1].getCells()[0].getValue() || !items[items.length-1].getCells()[1].getValue()){
					this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -Debe seleccionar Ubicaciones." : "- Debe seleccionar Ubicaciones.");
				} else if(oServiceJSONData.Dimension4.length > 0  && !items[items.length-1].getCells()[2].getValue()){
						this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -Debe seleccionar Ubicaciones." : "- Debe seleccionar Ubicaciones.");
				}
			}
		},
		
		/*
		 * Carga los combobox segun la eleccion del combobox nivel 1
		 */
		handleLoadItems: function(oControlEvent, idCbx) {
			//var selKey = oControlEvent.getParameter("selectedItem").getKey();
			var selText = oControlEvent.getParameter("selectedItem").getText();	
			var nivel1 = this.getView().byId("cbxUbicacion").getSelectedItem().getText();
			
			var aTemp = [];
			var aDimension3 = [];
			var aDimension4 = [];			
			
			if(idCbx === "dimension2") {
				this.getView().getModel().getData().Dimension3 = [];
				this.getView().getModel().getData().Dimension4 = [];
				this._dimension2 = selText;
				for(var i = 0; i < oDimenJSONData.value.length; i++){
					var oDimensiones = oDimenJSONData.value[i];				
					if(oDimensiones.U_SEI_NIVEL1 == nivel1 && 
					   oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
					   oDimensiones.U_SEI_NIVEL3 && 
					   jQuery.inArray(oDimensiones.U_SEI_NIVEL3, aTemp) < 0){						
						aTemp.push(oDimensiones.U_SEI_NIVEL3);
						aDimension3.push({"Name": oDimensiones.U_SEI_NIVEL3});						
					} else if(oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
							   !oDimensiones.U_SEI_NIVEL3 && 
							   jQuery.inArray(oDimensiones.U_SEI_NIVEL4, aTemp) < 0){
						aTemp.push(oDimensiones.U_SEI_NIVEL4);
						aDimension4.push({"Name": oDimensiones.U_SEI_NIVEL4});
					}
				}				
				this.getView().getModel().getData().Dimension3 = aDimension3;
				
				if(aDimension4.length > 0)
					this.getView().getModel().getData().Dimension4 = aDimension4;
				
			} else if(idCbx === "dimension3") {
				this._dimension3 = selText;
				for(var i = 0; i < oDimenJSONData.value.length; i++){
					var oDimensiones = oDimenJSONData.value[i];				
					if(oDimensiones.U_SEI_NIVEL1 == nivel1 && 
					   oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
					   oDimensiones.U_SEI_NIVEL3 === this._dimension3 && 
					   oDimensiones.U_SEI_NIVEL4 && 
					   jQuery.inArray(oDimensiones.U_SEI_NIVEL4, aTemp) < 0){
						
						aTemp.push(oDimensiones.U_SEI_NIVEL4);
						aDimension4.push({"Name": oDimensiones.U_SEI_NIVEL4});
					}
				}
				//provisorio
				this.getView().getModel().getData().Dimension4 = aDimension4;				
			}
			
//			var oModel = this.getView().getModel();
//			oModel.setData(oServiceJSONData);
//			oModel.refresh();
			
//			this.oSelectD3.setModel(oModel);
//			this.getView.getModel().setData(oModel);
//			this.getView.getModel().refresh();
		},
		
		/*
		 * 
		 * COMPORTAMIENTO DEL POPUP CON ITEMS
		 * 
		 */
		
		/*
		 * Evento del Boton "Nueva Ubicacion" el que agrega nuannueva linea 
		 * para agregar nuevas ubicaciones
		 */
		handleAgregarService: function(oEvent) {
			//Tomo los item de la tabla si es que lo tiene
			var items = this.getView().byId("idItemsTable").getItems();	
			if(this.byId("cbxUbicacion").getValue() !== "Selecciona Ubicacion"){
				//si los items son menores a 10 crea una nueva linea
				if(items.length < 10){
					//this.validaUbicaciones("agrega");
					this.handleUnidad();
					var date = new Date();
					var fechaActual = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();			
					this.fecha2 = (this.fecha1 !== null ? this.fecha1 : fechaActual);
					
					//Actualiza la cantidad en el jsondata
					for(var i = 0; i < items.length; i++){				
						for(var x = 0; x < this.oItems.newItem.length; x++ ){
							if(items[i].getCells()[0].getValue() === this.oItems.newItem[x].Dimension2 && 
									items[i].getCells()[1].getValue() === this.oItems.newItem[x].Dimension3 &&
									items[i].getCells()[2].getValue() === this.oItems.newItem[x].Dimension4){
								this.oItems.newItem[x].Dimension2 = items[i].getCells()[0].getValue();
								this.oItems.newItem[x].Dimension3 = items[i].getCells()[1].getValue();
								this.oItems.newItem[x].Dimension4 = items[i].getCells()[2].getValue();
							}
						}
					}			
					
					if(this.oItems === null){
						this.oItems = {newItem: [{ "Dimension2": "", "Dimension3": "", "Dimension4": "" }]};
					} else {
						if(items.length > 0){
							if(items[items.length-1].getCells()[0].getValue() &&
							   items[items.length-1].getCells()[1].getValue() &&
							   (oServiceJSONData.Dimension4.length > 0 && items[items.length-1].getCells()[2].getValue() )){
								this.oItems.newItem.push({ "Dimension2": "","Dimension3": "","Dimension4": "" });
							} else if(items[items.length-1].getCells()[0].getValue() &&
									   items[items.length-1].getCells()[1].getValue() &&
									   oServiceJSONData.Dimension4.length === 0) {
								this.oItems.newItem.push({ "Dimension2": "","Dimension3": "","Dimension4": "" });
							} else {
								var  oModel = this.getView().byId("idItemsTable").getModel();
								var oData = oModel.getData();
								new MessageBox.show("Debes Llenar la linea anterior antes de agregar una nueva ubicacion", {
									icon: MessageBox.Icon.WARNING,
									title: "Informacion Llamada se Servicio",
									onClose: function(oAction) {					
							    		sap.ui.core.BusyIndicator.hide();
							    		oModel.setData(oData);
										oModel.refresh();
									}
								});
							}
						} else {
							this.oItems.newItem.push({ "Dimension2": "","Dimension3": "","Dimension4": "" });
						}
					}
					//Actualiza el oModel para posterior carga
					var oView = oEvent.getSource();
					var oData = oView.getModel().getData();
					oData.newItems = this.oItems.newItem;
					oView.getModel().setData(oData);
					
					//Recarga la lista con nuevos productos
					this.oTable = this.getView().byId("idItemsTable");
					this.oColumn = this.getView().byId("idColList");
					this.oTable.setModel(this.oJSONModel);
					this.oTable.bindItems("/newItems", this.oColumn);
					
					var _dimension = this.byId("cbxUbicacion").getValue();
					this.byId("cbxUbicacion").setValue(_dimension);
					sap.ui.getCore().setModel(new sap.ui.model.json.JSONModel(this.oTable));
					
				} else {
					new sap.m.MessageBox.show("Solo se Pueden Agregar Hasta 10 Ubicaciones por Llamadas", {
						icon: sap.m.MessageBox.Icon.ERROR,
						title: "Informacion Llamada se Servicio",
						onClose: function(oAction) {					
				    		sap.ui.core.BusyIndicator.hide();
						}
					});
				}
			} else {
				new sap.m.MessageBox.show("Debes seleccionar la Ubicacion antes de agregar una nueva linea ", {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Informacion Llamada se Servicio",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
				
		},
		
		handleSearch: function(oEvent) {
			//Se obtiene el texto agregado para filtrar
			var sValue = oEvent.getParameter("value");
			var sQuery = oEvent.getParameter("query");
			//Si contiene datos o el largo es mayor a 0 entra en validacion
			if(sQuery && sQuery.length > 0)
				//se crea un filtro con 3 componentes "nombreCamppo", tipoFiltro y cadenaBuscar"
				var oFilter = new Filter("ItemName", FilterOperator.Contains, sQuery);

			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([oFilter]);
		},

		handleClose: function(oEvent) {
			oEvent.getSource().close();
			//oEvent.getSource().destroy();
		},
		
		handleCrearObj: function() {
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([]);

			var aContexts = oEvent.getParameter("selectedContexts");
			if (aContexts && aContexts.length) {
				aContexts.map(function(oContext) { 
					var codigo = oContext.getObject().ItemCode;
					var nombre = oContext.getObject().ItemName;
				});
			}
		},
		
		handleActiviBtn: function(oEvent) {
			var oView  = this.getView();
			var oModel = oView.getModel();
			var oData  = oModel.getData();
			
			if(oData.Activities.length === 0){
				oView.byId("Feed").setEnabled(true);
				var Activities = {Activities:[]};
				oData.Activities = Activities.Activities;
			} else {
				new sap.m.MessageBox.show("Ya Se ha Creado la Actividad Para este Servicio ", {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Informacion Llamada se Servicio",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		},
		
		onValueHelpRequestedNS: function() {
			this._oInput = this.getView().byId("inpInternalSNSearch");
			this.oColModel = new sap.ui.model.json.JSONModel();
			this.oColModel.setData(
					{
						"cols": [
							{ 
								"label": "Numero de Serie Interno", 
								"template": "InternalSerialNum", 
								"width": "10rem" 
							}, 
							{ 
								"label": "Codigo Producto", 
								"template": "ItemCode" 
							},
							{ 
								"label": "Codigo de Barras", 
								"template": "U_SEI_Cod_Barra_Interno" 
							},
							{ 
								"label": "Descripcion Producto", 
								"template": "ItemDescription" 
							}
						]
					}
			);
			
			var aCols = this.oColModel.getData().cols;
			this._oBasicSearchField = new sap.m.SearchField({showSearchButton: false});
			
			this._oValueHelpDialog = sap.ui.xmlfragment("SeidorB1UI5.view.fragment.NumerosDeSerie", this);
			this.getView().addDependent(this._oValueHelpDialog);

			this._oValueHelpDialog.getFilterBar().setBasicSearch(this._oBasicSearchField);
			
			this._oValueHelpDialog.getTableAsync().then(function (oTable) {
				oTable.setModel(this.getView().getModel());
				oTable.setModel(this.oColModel, "columns");

				if (oTable.bindRows) {
					oTable.bindAggregation("rows", "/SerialNumbers");
				}

				if (oTable.bindItems) {
					oTable.bindAggregation("items", "/SerialNumbers", function () {
						return new sap.m.ColumnListItem({
							cells: aCols.map(function (column) {
								return new sap.m.Label({ text: "{" + column.template + "}" });
							})
						});
					});
				}

				this._oValueHelpDialog.update();
			}.bind(this));

			var oToken = new sap.m.Token();
			oToken.setKey(this._oInput.getSelectedKey());
			oToken.setText(this._oInput.getValue());
			this._oValueHelpDialog.setTokens([oToken]);
			this._oValueHelpDialog.open();
		},
		
		onFilterBarSearchNS: function (oEvent) {
			var sSearchQuery = this._oBasicSearchField.getValue(),
				aSelectionSet = oEvent.getParameter("selectionSet");
			var aFilters = aSelectionSet.reduce(function (aResult, oControl) {
				if (oControl.getValue()) {
					aResult.push(new sap.ui.model.Filter({
						path: oControl.getName(),
						operator: sap.ui.model.FilterOperator.Contains,
						value1: oControl.getValue()
					}));
				}

				return aResult;
			}, []);

			aFilters.push(new sap.ui.model.Filter({
				filters: [
					new sap.ui.model.Filter({ path: "InternalSerialNum", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery }),
					new sap.ui.model.Filter({ path: "U_SEI_Cod_Barra_Interno", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery }),
					new sap.ui.model.Filter({ path: "ItemDescription", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery })
				],
				and: false
			}));

			this._filterTable(new sap.ui.model.Filter({
				filters: aFilters,
				and: true
			}));
		},
		
		_filterTable: function (oFilter) {
			var oValueHelpDialog = this._oValueHelpDialog;

			oValueHelpDialog.getTableAsync().then(function (oTable) {
				if (oTable.bindRows) {
					oTable.getBinding("rows").filter(oFilter);
				}

				if (oTable.bindItems) {
					oTable.getBinding("items").filter(oFilter);
				}

				oValueHelpDialog.update();
			});
		},
		
		onValueHelpOkPressNS: function (oEvent) {
			sap.ui.core.BusyIndicator.show(0);
			
			var aTokens = oEvent.getParameter("tokens");
			//this._oInput.setValue(aTokens[0].getKey());
			var nom = aTokens[0].getText();
			nom = nom.replace(" ("+ aTokens[0].getKey() +")", "");
			this.byId("inpInternalSNSearch").setValue(aTokens[0].getKey());
			this.byId("inpSNDescripcion").setValue(nom);
			this._nom = nom;
			
			sap.ui.core.BusyIndicator.hide();
			
			this._oValueHelpDialog.close();
			
		},
		
		onValueHelpCancelPress: function () {
			this._oValueHelpDialog.close();
		},

		onValueHelpAfterClose: function () {
			this._oValueHelpDialog.destroy();
		},
		
		
		onPost: function(oEvent) {
			var oView  = this.getView();
			oView.byId("Feed").setEnabled(false);
			var DateFormat = sap.ui.core.format.DateFormat;
			var oFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({ style: "medium" });
			var oFecha = sap.ui.core.format.DateFormat.getDateTimeInstance({ pattern: "yyyy-MM-dd" });
			var oHora = sap.ui.core.format.DateFormat.getDateTimeInstance({ pattern: "HH:mm:ss" });//			
			
			var oDate = new Date();
			var sDate = oFormat.format(oDate);
			var sFech = oFecha.format(oDate);
			var sHora = oHora.format(oDate);
			//creacion de mensaje
			var sValue = oEvent.getParameter("value");
			var  oEntry = {
				    "ActivityDate": sFech,
				    "ActivityTime": sHora,
				    "Text":sValue,
					Author: oDatosUsuario.user_name,
					Date: "" + sDate,
					Text:sValue
			};
			
			var oModel = this.getView().getModel();
			this.aEntries = oModel.getData().Activities;
			this.aEntries.unshift(oEntry);
			oModel.refresh();
			//this.setActivity();
		},
		
		handleDelete: function(oEvent) {
			var oView  = this.getView();
			var oModel = oView.getModel();
//			var oData  = oModel.getData().Activities;
			
			var oData  = this.getView().getModel().getData().Activities;
			oData.splice(0,1);//remueve primer elemento
			oModel.refresh();
		},
		
		setActivity: function(){
			var oActivities = {};
			var activities = this.getView().getModel().getData().Activities[0];
			
			oActivities.U_SEI_AUTHOR 		= activities.Author;
			//oActivities.CardCode			= "CFR0001";//this.cardCode Pendiente Obtencion via Logueo
			oActivities.ActivityDate 		= activities.ActivityDate;				
			oActivities.ActivityTime 		= activities.ActivityTime;
			oActivities.Details 			= activities.Text;
			oActivities.Duration 			= 900;
			oActivities.DurationType 		= "du_Minuts";
			oActivities.Reminder 			= "tYES";
			oActivities.ReminderPeriod 		= 15;
			oActivities.ReminderType 		= "du_Minuts";
			oActivities.Notes 				= "";
			
			var res = setActivities(oActivities);
			sap.ui.core.BusyIndicator.hide();
			return res;
		},
		
		updateActivity: function(activityCode, serviceCallID) {
			var oActivities = {};
			
			oActivities.ParentObjectId 		= serviceCallID//N° de llamada de servicio(se actualiza cuando se haya creado la llamada)
			oActivities.ParentObjectType 	= "191"
				
			updateActivities(activityCode, oActivities);	
		}
	});
	return ServiceCreateController;
});