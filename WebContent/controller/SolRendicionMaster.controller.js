sap.ui.define([
	"sap/ui/core/mvc/Controller", 
	"sap/ui/core/routing/History",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/Filter",
	"sap/ui/model/Sorter",
	"./Formatter"
	
], function(Controller, History, FilterOperator, Filter, Sorter, Formatter){
	"use strict";
	var SolRendicionMasterController = Controller.extend("SeidorB1UI5.controller.SolRendicionMaster", {
		
		formatter: Formatter,
		
		onInit: function() {
			//GUARDA ELA FUNCION ACTUALIZAR PARA FORZAR POSTERIOR CARGA DE PANTALLA
			var oPrevious = History.getInstance().getPreviousHash();
			const onAfterShow = () => this.handleActualizar();
			this._afterShowDelegate = { onAfterShow };
			if(oPrevious !== "SolRendicionCreate" || oPrevious !== "Page")
				this.getView().addEventDelegate(this._afterShowDelegate);
			
			//valida que el oRendicionJSONData contenga datos 
			//los valores ajax y crea la variable y carga los datos
			if(!oSolRendicionJSONData || oSolRendicionJSONData === undefined){
				sap.ui.core.BusyIndicator.show(0);
				oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
				getSolRendiciones();
				//carga los valores del sessionStorage en variable
				oSolRendicionJSONData = JSON.parse(sessionStorage.getItem("solrendiciones"));
				
				oSolRendicionJSONData.tipoFinanciamientos 	= JSON.parse(sessionStorage.getItem('tipoFinanciamientos')).value;
				oSolRendicionJSONData.departamentos 		= JSON.parse(sessionStorage.getItem('departamentos')).value;
				oSolRendicionJSONData.tipoDocumentos 		= JSON.parse(sessionStorage.getItem('tipoDocumentos')).value;
				oSolRendicionJSONData.rutFuncionarios 		= JSON.parse(sessionStorage.getItem('rutFuncionarios')).value;
				oSolRendicionJSONData.rutProveedores 		= JSON.parse(sessionStorage.getItem('rutProveedores')).value;
			}
			
			var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
			this.byId("avatar").setInitials(iAvatar);
			this.byId("avatar").setTooltip("Usuario: " + avatar);
			
			var oTable = this.getView().byId("tablaRendicion");
			var oModelTable = new sap.ui.model.json.JSONModel();

			//ordersTable = oTable;
			oModelTable.setData(oSolRendicionJSONData);
			oTable.setModel(oModelTable);
	    	this.getView().setModel(oModelTable);
	    	
	    	//oTable.sort(this.getView().byId("DocDate"), sap.ui.table.SortOrder.Descending, true);
	    	oTable.sort(this.getView().byId("DocEntry"), sap.ui.table.SortOrder.Descending, true);
//	    	var rows = oTable.getRows().length + 1;
//	    	oTable.setVisibleRowCount(rows);
	    	
	    	var fnPress = this.handleActionPress.bind(this);
	    	this.modes=[{
	    		key: "Multi",
				text: "Multiple Actions",
				handler: function(){
					var oTemplate = new sap.ui.table.RowAction({items: [
						new sap.ui.table.RowActionItem({
							type: "Navigation",
							press: fnPress,
							visible: "{Available}"
						})
					]});
					return [2, oTemplate];
				}
	    	}];
	    	this.getView().setModel(new sap.ui.model.json.JSONModel({items: this.modes}), "modes");
			this.switchState("Multi");
		},
		
		handleLogout: function (evt) {
			sessionStorage.clear();
			delCookies();
			window.location.replace("");
		},
	
		switchState : function (sKey) {
			var oTable = this.byId("tablaRendicion");
			var iCount = 0;
			var oTemplate = oTable.getRowActionTemplate();
			if (oTemplate) {
				oTemplate.destroy();
				oTemplate = null;
			}
	
			for (var i = 0; i < this.modes.length; i++) {
				if (sKey == this.modes[i].key) {
					var aRes = this.modes[i].handler();
					iCount = aRes[0];
					oTemplate = aRes[1];
					break;
				}
			}
	
			oTable.setRowActionTemplate(oTemplate);
			oTable.setRowActionCount(iCount);
		},
		
		handleActionPress : function(oEvent) {
			var oRow = oEvent.getParameter("row");
			var oItem = oEvent.getParameter("item");
			ordersTable = oItem;
			selectedOrder = oEvent.getParameter("row").getCells()[0].getText();
			sap.ui.core.UIComponent.getRouterFor(this).navTo("SolRendicionDetail",{from: "solrendicionmaster", contextPath: oItem.getBindingContext().getPath().substr(7)});
		},
	
		handleCrearSoliBtnPress: function() {		
			sap.ui.core.UIComponent.getRouterFor(this).navTo("SolRendicionCreate",{});
		},
		
		handleActualizar: function(){
			oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
			getSolRendiciones();
			oSolRendicionJSONData = JSON.parse(sessionStorage.getItem("solrendiciones"));
			
			var oTable = this.getView().byId("tablaRendicion");
		    var data = oTable.getModel();
			var oData = data.getData();
			
			data.setData(oSolRendicionJSONData);
			data.refresh();
			this.getView().setModel(data);
		},
	
		_filter : function () {
			var oFilter = null;
	
			if (this._oGlobalFilter && this._oPriceFilter) {
				oFilter = new sap.ui.model.Filter([this._oGlobalFilter, this._oPriceFilter], true);
			} else if (this._oGlobalFilter) {
				oFilter = this._oGlobalFilter;
			} else if (this._oPriceFilter) {
				oFilter = this._oPriceFilter;
			}
	
			this.byId("tablaRendicion").getBinding("rows").filter(oFilter, "Application");
		},
	
		filterGlobally : function(oEvent) {
			var sQuery = oEvent.getParameter("query");
			this._oGlobalFilter = null;
	
			if (sQuery) {
				this._oGlobalFilter = new Filter([
					new Filter("U_SEI_NRESP", FilterOperator.Contains, sQuery),
					new Filter("U_SEI_FECHACTI", FilterOperator.Contains, sQuery)
				], false);
			}
	
			this._filter();
		},
	
		onBack : function () {
			var sPreviousHash = History.getInstance().getPreviousHash();
	
			//The history contains a previous entry
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				// There is no history!
				// replace the current hash with page 1 (will not add an history entry)
				this.getOwnerComponent().getRouter().navTo("Page", null, true);
			}
		},
		
		Click_btnImprimir: function(){
			getReportPresupuesto()
		}
	});
	return SolRendicionMasterController;
}, 
/* bExport= */ 
true);