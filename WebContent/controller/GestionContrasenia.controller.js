sap.ui.define([
	"jquery.sap.global",
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"./Formatter"
	
], function(jQuery, Controller, History, JSONModel, MessageToast, MessageBox, Formatter) {
	"use strict";

	var GestionContrasenia = Controller.extend("SeidorB1UI5.controller.GestionContrasenia", {
		/*
		 * Declaracion de variables
		 */
		formatter: SeidorB1UI5.controller.Formatter, busy: new sap.m.BusyDialog(),
		mensaje: null, oTable: null, oItems:null, oJSONModel: null,
		codigo: null, descripcion: null, fecha1: null, fecha2: null,		
		oModelSel: null, oSelectD2: null, oSelectD3: null,
		
		/*
		 * Carga de Elementos de la vista
		 */
		onInit: function() {
			if(!oDatosUsuario)
				oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
			
			var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
			this.byId("avatar").setInitials(iAvatar);
			this.byId("avatar").setTooltip("Usuario: " + avatar);
			
			this._user_id = oDatosUsuario.user_id;
		},
		
		handleLogout: function (evt) {
			sessionStorage.clear();
			sessionStorage.clear();
			delCookies();
			window.location.replace("");
		},

		getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},
		
		handleNav: function(){
			var oHistory, sPreviousHash, oRouter, oTarget;
			
			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();
			oRouter = this.getRouter();
			
			if (sPreviousHash !== undefined) {
				this.limpiarCampos();
				window.history.go(-1);
			} else {
				this.limpiarCampos();
				oRouter.navTo("QuotationsMaster", true);
			}
		},
		
		handleNavButton: function(oEvent) {
			var oView = this, oHistory, sPreviousHash, oRouter, oTarget;
			
			if (this._oDialog)
				this.cierraFragment();
			
			new sap.m.MessageBox.show("Esta de Seguro de Volver. Si lo hace, se perdera todos los cambios.", {
				icon: sap.m.MessageBox.Icon.INFORMATION,
				title: "Gestion de Contraseña",
				actions: [sap.m.MessageBox.Action.OK, sap.m.MessageBox.Action.CANCEL],
				emphasizedAction: sap.m.MessageBox.Action.OK,
				onClose: function (oAction) { 
					if(oAction === "OK")
						oView.handleNav();
				}
			});
			
		},

		Click_btnCambiar: function(oEvent) {
			sap.ui.core.BusyIndicator.show(0);			
			this.validacionEnvio();
			
			if(this.mensaje === null) {
				var oUser = { "UserPassword": this.byId("inpPassNeo").getValue() };
				
				var res = updeteUser(this._user_id, oUser);
				
				this.limpiarCampos();
				
//				{
//				    "UserCode": "u001",
//				    "UserName": "User1",
//				    "UserPassword": "default"
//				}
				
			} else {
				sap.m.MessageBox.show(this.mensaje, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Gestion de Contraseña",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		},
		
		validacionEnvio: function() {
			this.mensaje = null;
			
			if(!this.byId("inpPassNeo").getValue())
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Se debe ingresar la nueva contraseña." : "- Se debe ingresar la nueva contraseña.");
			
			if(!this.byId("inpPassRep").getValue()){
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Se debe repetir la nueva contraseña." : "- Se debe repetir la nueva contraseña.");
			} else if(this.byId("inpPassNeo").getValue() !== this.byId("inpPassRep").getValue()){
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Las contraseñas ingresadas no coinciden." : "- Las contraseñas ingresadas no coinciden.");
			}
			
		},
		
		Click_btnLimpiar: function(oEvent) {
			var oView = this;
			
			new MessageBox.show("Esta seguro de limpiar los campos, los cambios no guardados se perderan", {
				icon: MessageBox.Icon.WARNING,
				title: "Gestion de Contraseña",
				onClose: function(oAction) {
					oView.limpiarCampos();
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		},
		
		limpiarCampos: function(){
			this.byId("inpPassNeo").setValue("");
			this.byId("inpPassRep").setValue("");
		},

		cierraFragment: function(oEvent) {
	        this._oDialog.destroy();  //Second: destoy fragment 
	        this._oDialog=null;  // Third: null name/pointer 
	    },
		
		onExit: function() {
			if (this._oDialog) {
//				this._oDialog.destroy();
			}
		}
	});
	return;
});