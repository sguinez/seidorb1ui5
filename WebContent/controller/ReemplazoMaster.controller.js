sap.ui.define([
	"sap/ui/core/mvc/Controller", 
	"sap/ui/core/routing/History",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/Filter",
	"sap/ui/model/Sorter",
	'sap/ui/core/library',
	"./Formatter"
	
], function(Controller, History, FilterOperator, Filter, Sorter, coreLibrary, Formatter){
	"use strict";
	
	var ValueState = coreLibrary.ValueState;
	
	var ReemplazoMasterController = Controller.extend("SeidorB1UI5.controller.ReemplazoMaster", {
		
	formatter: Formatter, busy: new sap.m.BusyDialog(), 
	
	onInit: function() 	{
		if(document.cookie.length === 0){
			this.handleBtnLogout();
		} else {
			this.busy.open();
			
			oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
			var oPrevious = History.getInstance().getPreviousHash();
			
			this.OcultaInfoRol();
			
			this.busy.close();
			//GUARDA ELA FUNCION ACTUALIZAR PARA FORZAR POSTERIOR CARGA DE PANTALLA
			const onAfterShow = () => this.handleBtnActualizar();
			this._afterShowDelegate = { onAfterShow };
			if(oPrevious !== "ReemplazoCreate" || oPrevious !== "Page")
				this.getView().addEventDelegate(this._afterShowDelegate);
			
			var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
			
			this.byId("avatar").setInitials(iAvatar);
			this.byId("avatar").setTooltip("Usuario: " + avatar);
			
			
			var oTable = this.getView().byId("tablaSolicitud");
			var oModelTable = new sap.ui.model.json.JSONModel();
	
			oModelTable.setData(oReemplazoJSONData);
			oTable.setModel(oModelTable);
	    	this.getView().setModel(oModelTable);
	    	
	    	oTable.sort(this.getView().byId("CreateDate"), sap.ui.table.SortOrder.Descending, true);
	    	oTable.sort(this.getView().byId("Status"), sap.ui.table.SortOrder.Descending, true);
	    	oTable.sort(this.getView().byId("DocNum"), sap.ui.table.SortOrder.Descending, true);
	    	this.getView().setModel(this.oModelList);
	    	
	    	var fnPress = this.handleSelect.bind(this);
	    	this.modes=[{
	    		key: "Multi",
				text: "Multiple Actions",
				handler: function(){
					var oTemplate = new sap.ui.table.RowAction({items: [
						new sap.ui.table.RowActionItem({
							type: "Navigation",
							press: fnPress,
							visible: "{Available}"
						})
					]});
					return [2, oTemplate];
				}
	    	}];
	    	this.getView().setModel(new sap.ui.model.json.JSONModel({items: this.modes}), "modes");
			this.switchState("Multi");
		}
	},
	
	OcultaInfoRol: function(){
		if(oDatosUsuario.code_rol !== "1") {
			//TODO: OCULTA BOTON SI ES NECESARIO SEGUN ID DE COMPONENTE
			var btnApro = view.byId("btnAprobar");
			var btnRech = view.byId("btnRechazar");
			
			btnApro.setVisible(false);
			btnRech.setVisible(false);
        }
	},
	
	handleBtnAvatar: function(){
		
	},
	
	handleBtnLogout: function (evt) {
		sessionStorage.clear();
		delCookies();
		window.location.replace("");
	},
	
	handleCambiaFecha:function(){
		
	},	
	
	handleChangeCb: function(oEvent){
		var oValidatedComboBox = oEvent.getSource(),
		sSelectedKey = oValidatedComboBox.getSelectedKey(),
		sValue = oValidatedComboBox.getValue();

		if (!sSelectedKey && sValue) {
			oValidatedComboBox.setValueState(ValueState.Error);
			oValidatedComboBox.setValueStateText("Por favor ingresa valor valido!");
		} else {
			oValidatedComboBox.setValueState(ValueState.None);
		}
	},
	
	handleBtnActualizar: function(){
		oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
		strAccion = "";
		var fecIni = "", fecFin = "", estado = "", SEI_UNIDAD = "", SEI_ESTADO = "", SEI_BETWEN = "";
		
		fecIni = this.getView().byId("dpFechaDesdeSolicitudes").getValue();
		fecFin = this.getView().byId("dpFechaHastaSolicitudes").getValue();
		estado = this.getView().byId("cbxEstado").getSelectedKey();
		
		SEI_UNIDAD = "U_SEI_UNIDAD eq '" + oDatosUsuario.cod_suc + "'";
		SEI_ESTADO = (estado == "0" || estado == "1" || estado == "2") ? " and U_SEI_ESTADO eq '" + estado + "'" : "";
		SEI_BETWEN = " and CreateDate gt '" + fecIni + "' and CreateDate lt '" + fecFin + "'";
		
		if(fecIni != "" && fecFin != ""){
			if(fecIni <= fecFin){
				select="", filtro="";
				select="", filtro=SEI_UNIDAD + SEI_BETWEN + SEI_ESTADO;
				getAsync("Reemplazo");
			}else{
				new sap.m.MessageBox.show(
						"Las fechas de inicio no puede ser mayor a la fin", { 
	               	icon: sap.m.MessageBox.Icon.ERROR, 
	              	title: "Error en Busqueda de Solicitudes de Compra."
	            });
			}
		}else{
			select="", filtro ="";
			select="", filtro=SEI_UNIDAD + " and CreateDate gt '" + sumarDias(new Date(), -365).toISOString().substr(0,10) + "'" + SEI_ESTADO;
			getAsync("Reemplazo");
		}
		oReemplazoJSONData = JSON.parse(sessionStorage.getItem("Reemplazo"));
		
		var oTable = this.getView().byId("tablaSolicitud");
	    var data = oTable.getModel();
		var oData = data.getData();
		
		data.setData(oReemplazoJSONData);
		data.refresh();
		this.getView().setModel(data);
	},
	
	handleBtnCrear: function() {
		strAccion = "crear";
		sap.ui.core.UIComponent.getRouterFor(this).navTo("ReemplazoCreate",{});
	},
	
	switchState : function (sKey) {
		var oTable = this.byId("tablaSolicitud");
		var iCount = 0;
		var oTemplate = oTable.getRowActionTemplate();
		if (oTemplate) {
			oTemplate.destroy();
			oTemplate = null;
		}

		for (var i = 0; i < this.modes.length; i++) {
			if (sKey == this.modes[i].key) {
				var aRes = this.modes[i].handler();
				iCount = aRes[0];
				oTemplate = aRes[1];
				break;
			}
		}

		oTable.setRowActionTemplate(oTemplate);
		oTable.setRowActionCount(iCount);
	},
	
	handleSelect: function(oEvent) {
		strAccion = "modificar";
		var oRow = oEvent.getParameter("row");
		var oItem = oEvent.getParameter("item");
		ordersTable = oItem;
		selectedOrder = oEvent.getParameter("row").getCells()[0].getText();
		sap.ui.core.UIComponent.getRouterFor(this).navTo("ReemplazoCreate",{from: "ReemplazoMaster", contextPath: oItem.getBindingContext().getPath().substr(7)});
	},
	
	_filter : function () {
		var oFilter = null;

		if (this._oGlobalFilter && this._oPriceFilter) {
			oFilter = new sap.ui.model.Filter([this._oGlobalFilter, this._oPriceFilter], true);
		} else if (this._oGlobalFilter) {
			oFilter = this._oGlobalFilter;
		} else if (this._oPriceFilter) {
			oFilter = this._oPriceFilter;
		}
		this.byId("tablaSolicitud").getBinding("rows").filter(oFilter, "Application");		
	},

	filterGlobally : function(oEvent) {
		var sQuery = oEvent.getParameter("query");
		this._oGlobalFilter = null;

		if (sQuery) {
			this._oGlobalFilter = new Filter([
				new Filter("RequesterName", FilterOperator.Contains, sQuery),
				new Filter("DocDate", FilterOperator.Contains, sQuery),
				new Filter("U_SEI_ESTADO", FilterOperator.Contains, sQuery)
			], false);
		}

		this._filter();
	},
	
	handleBtnObtenerReporte: function(oEvent){
		
	},
	
	handleBtnAprobar: function(oEvent){
		var oTable = this.getView().byId("tablaSolicitud");
	    var data = oTable.getModel();
	    var selRow = oTable.getSelectedIndices();
	    
	    if(selRow.length > 0){
		    for (var i = 0; i < selRow.length; i++) {
		    	var oBody = {};
		    	var rowNum = selRow[i];
		        var sDocEntry = oTable.getRows()[rowNum].getCells()[0].getText();
		        
		        oBody.U_SEI_ESTADO = "0";
		        //TODO: crear llamada para actualizacion de estado de reemplazo a aprobado
		        updateReemplazo(sDocEntry, oBody);
		    }
		    this.byId("tablaSolicitud").clearSelection();
		} else {
			new MessageBox.show("No se ha seleccionado lineas a Aprobar", {
				icon: MessageBox.Icon.WARNING,
				title: "Informacion Actualizacion de Reemplazo",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		}
	},
	
	handleBtnRechazar: function(oEvent){
		var oTable = this.getView().byId("tablaSolicitud");
	    var data = oTable.getModel();
	    var selRow = oTable.getSelectedIndices();
	    
	    if(selRow.length > 0){
		    for (var i = 0; i < selRow.length; i++) {
		    	var oBody = {};
		    	var rowNum = selRow[i];
		        var sDocEntry = oTable.getRows()[rowNum].getCells()[0].getText();
		        
		        oBody.U_SEI_ESTADO = "1";
		        //TODO: crear llamada para actualizacion de estado de reemplazo a rechazado
		        updateReemplazo(sDocEntry, oBody);
		    }
		    this.byId("tablaSolicitud").clearSelection();
		} else {
			new MessageBox.show("No se ha seleccionado lineas a Rechazar", {
				icon: MessageBox.Icon.WARNING,
				title: "Informacion Actualizacion de Reemplazo",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		}
	},
	
	onBack : function () {
		var sPreviousHash = History.getInstance().getPreviousHash();

		//The history contains a previous entry
		if (sPreviousHash !== undefined) {
			window.history.go(-1);
		} else {
			// There is no history!
			// replace the current hash with page 1 (will not add an history entry)
			this.getOwnerComponent().getRouter().navTo("Page", null, true);
		}
	}
	});
	return ReemplazoMasterController;
}, /* bExport= */ true);