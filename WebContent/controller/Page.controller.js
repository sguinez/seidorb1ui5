sap.ui.define([
		'jquery.sap.global',
		'sap/ui/core/mvc/Controller',
		'sap/ui/model/json/JSONModel',
		"sap/ui/core/Fragment"
	], function(jQuery, Controller, JSONModel, Fragment) {
	"use strict";

	var PageController = Controller.extend("SeidorB1UI5.controller.Page", {

		onInit : function (evt) {
			if(document.cookie.length === 0){
				this.handleLogout();
			} else {
				//carga los valores del sessionStorage en variable 
				if((!oTilesJSONData || oTilesJSONData === undefined) && sessionStorage.getItem("tiles")){
					oTilesJSONData = JSON.parse(sessionStorage.getItem("tiles")).value;
					oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
				} else if(!sessionStorage.getItem("tiles")){
					this.handleLogout();
				}
				
				if(oDatosUsuario.user_name)
				{
					var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
				}
				else
				{
					this.handleLogout();
				}
				
				this.byId("avatar").setInitials(iAvatar);
				this.byId("avatar").setTooltip("Usuario: " + avatar);

				var oModelTile = new JSONModel(oTilesJSONData);
				this.getView().setModel(oModelTile);
			}
		},
		
		handleGoApp : function (evt) {			
			var loRouter = sap.ui.core.UIComponent.getRouterFor(this);
			
			switch(evt.oSource.getTitle()){
			case "Solicitud de Orden":
				loRouter.navTo("PurchaseMaster");
				break;
			case"Llamada de Servicio":
				loRouter.navTo("ServiceMaster");
				break;
			case"Tarjetas de Equipo":
				loRouter.navTo("ServiceCard");
				break;
			case"Rendición de Gasto":
				loRouter.navTo("RendicionMaster");
				break;
			case"Solicitud de Fondos":
				loRouter.navTo("SolRendicionMaster");
				break;
			case"Reporte Presupuesto":
				loRouter.navTo("ReportePresupuesto");
				break;
			case"Gestion de Contraseña":
				loRouter.navTo("GestionContrasenia");
				break;
			case"Gestion de Reemplazo":
				loRouter.navTo("ReemplazoMaster");
				break;
			}
		},
		
		handleNavButton: function(oEvent) {
			var sPreviousHash = sap.ui.core.routing.History.getInstance().getPreviousHash();
			this.getView().byId("idItemsTable").setMode("None");
			this.habilitaLineas(false);
			this.habilitaBotones(false);
			
			//The history contains a previous entry
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				// There is no history!. replace the current hash with page 1 (will not add an history entry)
				this.getOwnerComponent().getRouter().navTo("ServiceMaster", oServiceJSONData, true);
			}
		},
		
		handleLogout: function (evt) {
			sessionStorage.clear();
			delCookies();
			window.location.replace("");
		},
		
		CLick_Avatar: function(oEvent){
			new sap.m.MessageToast.show("Se Presiono Avatar!!");
		},
		
		//OCULTAR BOTONES SINO TIENE PERMISO
		hideOrShowForm: function(evt) {
			var inputModel = this.getView().getModel("myModel");
		    inputModel.setProperty("/formVisible", !(inputModel.getProperty("/formVisible")));
		}
	});
	return PageController;
});