jQuery.sap.require("sap.ui.core.format.DateFormat", "SeidorB1UI5.controller.Formatter");

sap.ui.core.mvc.Controller.extend("SeidorB1UI5.controller.SolRendicionDetail", {
	
	formatter: SeidorB1UI5.controller.Formatter,
	
	codSocio: null, montoEntregado: null, saldoPendiente: null,
	nroPago: null, fechaPagoEntregado: null,
	// MODIFICAR //
	// A continuación se muestra el procedimiento de enlace de la vista Detalles para mostrar los datos maestros de elementos en el área del encabezado //
	// MODIFICAR //
	onInit: function() {
		if((oDatosUsuario === undefined || !oDatosUsuario) && sessionStorage.getItem("tiles")){
			oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
		} else if(!sessionStorage.getItem("tiles")){
			this.handleLogout();
		}
		var oModel = null;
		var view = this.getView();
		
		var btnApro = view.byId("btnAprobar");
		var btnRech = view.byId("btnRechazar");
		
		if(oDatosUsuario.code_rol === "2") {
			//OCULTA BOTON SI ES NECESARIO SEGUN ID DE COMPONENTE
			btnApro.setVisible(false);
			btnRech.setVisible(false);
        }
		
		if(!oSolRendicionJSONData || oSolRendicionJSONData === undefined)
			oSolRendicionJSONData = JSON.parse(sessionStorage.getItem("solrendiciones"));
					
    	sap.ui.core.UIComponent.getRouterFor(this).attachRouteMatched(function(oEvent) {
    		oModel = new sap.ui.model.json.JSONModel();
    		oModel.setData(oSolRendicionJSONData);
        	view.setModel(oModel);
        	
			
    		// cuando se produce una navegación detallada, actualice el contexto de enlace
			// MODIFICAR //
			// A continuación se muestra el procedimiento de encuadernación de la vista Tabla de artículos para mostrar las líneas de documento de pedido de ventas en el área Detalles //
			// Es posible que deba cambiar el Identificador de servicio de /DocumentLines para que coincida con el nombre de su colección de Entity Lines
			// MODIFICAR //			
			if (oEvent.getParameter("name") === "SolRendicionDetail") {
				if(ordersTable){
					var oListItem = ordersTable;			    
					var context = new sap.ui.model.Context(view.getModel(), oListItem.getBindingContext().getPath());
					this.path = oListItem.getBindingContext().getPath().substr(7);
					view.setBindingContext(context);
					
					this.nroPago = view.getModel().getData().value[this.path].U_SEI_NROPAGO;
					
					var oData = view.getModel().getData();
					view.getModel().setData(oData);
				} else {
					this.handleNavButton();
				}
			}
		}, this);
	},
	
	handleLogout: function (evt) {
		sessionStorage.clear();
		delCookies();
		window.location.replace("");
	},
	
	/*
	 * Funcion encargada de actualiza los campos de la tabla
	 */
	handleUpdateBtn: function(oEvent){
		if(this.validaEstado()) {
			this.validaCampos();
			if(this.mensaje){
				new sap.m.MessageBox.show(this.mensaje, {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: "Informacion",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			} else if(this.mensaje === null){
				
				var sumaMonto = 0;
				var rendicion = {};
				var docEntry = this.byId("txtCabDocEntry").getText();
				var loRouter = sap.ui.core.UIComponent.getRouterFor(this);
				
				var porcentaje = porcentajeUsado(this.montoEntregado, this.saldoPendiente, sumaMonto);				
				var saldo = this.saldoPendiente - sumaMonto;
				
				if(!updateRendiciones(docNum, rendicion)){
					this.getView().byId("idItemsTable").setMode("None");
					this.desabilitaBotones();
					loRouter.navTo("RendicionMaster");
				}
			}
		}
	},
	
	/*
	 * Funcion encargada de Eliminar los items seleccionados 
	 */
	handleEliminBtn: function(oEvent){
		if(this.validaEstado()) {
			var oTable = this.getView().byId("idItemsTable");
			var items = oTable.getItems();
		    var data = oTable.getModel();
		    var selRowCount = oTable.getSelectedItems().length;
		    var docEntry = this.getView().byId("text1").getText();
		    var loRouter = sap.ui.core.UIComponent.getRouterFor(this);
		    var rowNum;
		    
		    if(selRowCount > 0){
		    	if(selRowCount < items.length){
		    		
				    var rendicion = {SEI_RENDETCollection:[]};
					var SEI_RENDETCollection = {};
					
					if(!delLineRendiciones(docEntry, rendicion)){
						loRouter.navTo("RendicionMaster");
					}
		    	} else {
		    		new sap.m.MessageBox.show("Se Debe Dejar al menos una Linea en la Solicitud", {
						icon: sap.m.MessageBox.Icon.WARNING,
						title: "Informacion Solicitud Rendicion",
						onClose: function(oAction) {					
				    		sap.ui.core.BusyIndicator.hide();
						}
					});
		    	}
			} else {
				new sap.m.MessageBox.show("No se ha seleccionado lineas a eliminar", {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: "Informacion Solicitud Rendicion",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		}
	},
	
	// A continuación se muestra la definición de la función para 
	// llamar a la capa de servicio para "Aprobar" un pedido de cliente "Pendiente"
	handleAceptBtn: function(oEvent) {
		sap.ui.core.BusyIndicator.show(0);
		
		var docEntry = this.getView().byId("text1");
		var docStatusTxt = this.getView().byId("text2");
		
		if(docStatusTxt.getText() === "Pendiente"){
			docStatusTxt.setText("Aprobado");		
			var body = { "U_SEI_ESTADO": "Aprobado" };
			
			updateSolRendiciones(docEntry.getText(), body);
			
			getSolRendiciones();
			oSolRendicionJSONData = JSON.parse(sessionStorage.getItem("solrendiciones"));
			
			new sap.m.MessageBox.show("El documento Cambio de estado a Aprobado", {
				icon: sap.m.MessageBox.Icon.SUCCESS,
				title: "Informacion Solicitud Rendicion",
				onClose: function(oAction) {
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		} else if (docStatusTxt.getText() === "Aprobado"){
			new sap.m.MessageBox.show("El documento ya se encuentra Aprobado", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Informacion Solicitud Rendicion",
				onClose: function(oAction){
					sap.ui.core.BusyIndicator.hide();
				}
			});
		} else {
			new sap.m.MessageBox.show("El documento no se encuentra Pendiente", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Informacion Solicitud Rendicion",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		}
	},
	
	// A continuación se muestra la definición de la función para 
	// llamar a la capa de servicio para "Rechazar" un pedido de cliente "Pendiente"
	handleCancelBtn: function(oEvent) {		
		var docEntry = this.getView().byId("text1");
		var docStatusTxt = this.getView().byId("text2");
		
		if(docStatusTxt.getText() === "Pendiente"){
						
			docStatusTxt.setText("Rechazada");		
			var body = { "U_SEI_ESTADO": "Rechazada" };
			
			updateSolRendiciones(docEntry.getText(), body);
			
			getSolRendiciones();
			oSolRendicionJSONData = JSON.parse(sessionStorage.getItem("solrendiciones"));
			
			new sap.m.MessageBox.show("El documento Cambio de estado a Rechazado", {
				icon: sap.m.MessageBox.Icon.SUCCESS,
				title: "Informacion Solicitud Rendicion",
				onClose: function(oAction) {
					btnApro.setEnabled(false);
					btnRech.setEnabled(false);
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
			
		} else {
			new sap.m.MessageBox.show("El documento no se encuentra Pendiente!", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Informacion Solicitud Rendicion",
				onClose: function(oAction) {
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		}		
	},
	
	/*
	 * Funcion encargada de Volver a la pagina anterior "RendicionMaster"
	 */
	handleNavButton: function(oEvent) {
		var sPreviousHash = sap.ui.core.routing.History.getInstance().getPreviousHash();
		//The history contains a previous entry
		if (sPreviousHash !== undefined) {
			window.history.go(-1);
		} else {
			// There is no history!
			// replace the current hash with page 1 (will not add an history entry)
			this.getOwnerComponent().getRouter().navTo("SolRendicionMaster", oItems, true);
		}
	},
	
	validaEstado: function() {
		var docStatusTxt = this.getView().byId("text2").getText();
		if(docStatusTxt === "Pendiente" || docStatusTxt === "Rechazada"){
			return true;
		} else {
			new sap.m.MessageBox.show("El documento ya se encuentra Aprobado!", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Informacion",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
		    		return false;
				}
			});
		}
	},
	
	validaComboBox: function(oEvent){
		var oValidatedComboBox = oEvent.getSource(),
	    sSelectedKey = oValidatedComboBox.getSelectedKey(),
	    sValue = oValidatedComboBox.getValue();
	
		if(!sSelectedKey && sValue) {
			oValidatedComboBox.setValueState("Error");
			oValidatedComboBox.setValueStateText("Porfavor Ingresa un valor Valido!");
		} else {
			oValidatedComboBox.setValueState("None");
		}
		return oValidatedComboBox;
	},
	
	validaCampos: function(){
		this.mensaje = null;
		var items = this.getView().byId("idItemsTable").getItems();		
		for (var item = 0; item < items.length; item++) {		
			var linea = item + 1;
			for(var cell = 0; cell < items[item].getCells().length-1; cell++){
				if(cell !== 0 && cell !== 9){
					if(!items[item].getCells()[cell].getValue()){
						switch(cell){
						case 1:
							this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Ingresar Numero de Documento. En la linea: " + linea : "- Debe Ingresar Numero de Documento. En la linea: " + linea);
							break;
						case 2:
							this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Seleccionar una Fecha. En la linea: " + linea : "- Debe Seleccionar una Fecha. En la liena: " + linea);
							break;
						case 3:
							this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Seleccionar el Tipo de Documento. En la linea: " + linea : "- Debe Seleccionar el Tipo de Documento. en la linea:" + linea);
							break;
						case 4:
							if(items[item].getCells()[cell].getEnabled())
								this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Seleccionar un Rut del Proveedor y/o Nombre del Proveedor. En la linea: " + linea : "- Debe Seleccionar un Rut del Proveedor y/o Nombre del Proveedor. En la Linea: " + linea);
							break;
						case 5:
							if(items[item].getCells()[cell].getEnabled())
								this.mensaje = (this.mensaje !== null ? this.mensaje + "\n  - Debe Seleccionar un Nombre del Proveedor y/o Rut del Proveedor. En la linea: " + linea : "- Debe Seleccionar un Nombre del Proveedor y/o Rut del Proveedor. En la Linea: " + linea);
							break;
						case 6:
							if(items[item].getCells()[cell].getEnabled())
								this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Ingresar la Factura Asociada. En la linea: " + linea : "- Debe Ingresar la Factura Asociada. En la linea: " + linea);
							break;
						case 7:
							this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Ingresar el Monto. En la linea: " + linea : "- Debe Ingresar el Monto. En la linea: " + linea);
							break;
						case 8:
							this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Seleccionar la Descripcion. En la linea: " + linea : "- Debe Seleccionar la Descripcion. En la linea: " + linea);
							break;
						}
						
					}
				}
			}
		}
	},
	
	cargaCombobox: function(oControlEvent, idCbx) {
		if(idCbx === null)
			idCbx = "cbxFuncionario";
		
		switch(idCbx){
		case "tipoFinanciamientos":
			getAsync('tipoFinanciamientos');
			oRendicionJSONData.tipoFinanciamientos = JSON.parse(sessionStorage.getItem('tipoFinanciamientos')).value;
			break;
		case "rutFuncionarios":
			getAsync('rutFuncionarios');
			oRendicionJSONData.rutFuncionarios = JSON.parse(sessionStorage.getItem('rutFuncionarios')).value;
			break;
		case"departamentos":
			getAsync('departamentos');
			oRendicionJSONData.departamentos = JSON.parse(sessionStorage.getItem('departamentos')).value;
			break;
		case"tipoDocumentos":
			getAsync('tipoDocumentos');
			oRendicionJSONData.tipoDocumentos = JSON.parse(sessionStorage.getItem('tipoDocumentos')).value;
			break;
		case"rutProveedores":
			getAsync('rutProveedores');
			oRendicionJSONData.rutProveedores = JSON.parse(sessionStorage.getItem('rutProveedores')).value;
			break;
		case"tipoRendicion":
			getAsync('tipoRendicion');
			oRendicionJSONData.tipoRendicion = JSON.parse(sessionStorage.getItem('tipoRendicion')).value;
			
			for(var i = 0; i < JSON.parse(sessionStorage.getItem('tipoRendicion')).value.length; i++){
				if(JSON.parse(sessionStorage.getItem('tipoRendicion')).value[i].U_SEI_TRENDI === "1"){
					this.oRendicion1.value.push(JSON.parse(sessionStorage.getItem('tipoRendicion')).value[i]);
				} else if(JSON.parse(sessionStorage.getItem('tipoRendicion')).value[i].U_SEI_TRENDI === "2"){
					this.oRendicion2.value.push(JSON.parse(sessionStorage.getItem('tipoRendicion')).value[i]);
				}
			}
			break;
		case"saldoAsignado":
			getAsync('saldoAsignado');
			oRendicionJSONData.saldoAsignado = JSON.parse(sessionStorage.getItem('saldoAsignado')).value;
			break;
		}
	},
	
	delay : function(componente) {
		return new Promise(resolve => {
		    setTimeout(() => {
		    	componente.setBusy(false);
		    }, 5000);
		 });
	},
	
	onExit: function() {
		if (this._oActionSheet) {
			this._oActionSheet.destroy();
			this._oActionSheet = null;
		}
		//this.oProductsModel.destroy();
	},
	
	getSelectedRowContext: function(sTableId, fnCallback) {
		var oTable = this.byId(sTableId);
		var iSelectedIndex = oTable.getSelectedIndex();

		if (iSelectedIndex === -1) {
			MessageToast.show("Please select a row!");
			return;
		}

		var oSelectedContext = oTable.getContextByIndex(iSelectedIndex);
		if (oSelectedContext && fnCallback) {
			fnCallback.call(this, oSelectedContext, iSelectedIndex, oTable);
		}

		return oSelectedContext;
	},
	
	Click_btnImprimir: function(){
		getReportPresupuesto()
	}
});