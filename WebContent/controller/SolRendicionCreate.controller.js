sap.ui.define([
	"jquery.sap.global",
	"sap/m/MessageToast",
	"sap/ui/core/Fragment",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",	
	"sap/ui/model/json/JSONModel"
	
], function(jQuery, MessageToast, Fragment, Controller, Filter, JSONModel) {
	"use strict";

	var SolRendicionCreateController = Controller.extend("SeidorB1UI5.controller.SolRendicionCreate", {
		
		mensaje: null, noTable: null, noItems:null, noJSONModel: null,
		codigo: null, descripcion: null, fecha1: null, fecha2: null,
		
		oModelSel: null, oSelectD2: null, oSelectD3: null,
		codSocio: null, montoEntregado: null, saldoPendiente: null,
		nroPago: null, fechaPagoEntregado: null,
		oRendicion1: {value:[]}, oRendicion2: {value:[]},
		
		onInit: function() {
			if(JSON.parse(sessionStorage.getItem("tiles")) === null){
				this.handleLogout();
			}
			
			if((oDatosUsuario === undefined || !oDatosUsuario) && sessionStorage.getItem("tiles")){
				oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
			} else if(!sessionStorage.getItem("tiles")){
				this.handleLogout();
			}
			
			var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
			this.byId("avatar").setInitials(iAvatar);
			this.byId("avatar").setTooltip("Usuario: " + avatar);
			
			if(!oRendicionJSONData || oRendicionJSONData === undefined){
				sap.ui.core.BusyIndicator.show(0);				
				//carga los valores del sessionStorage en variable
				getRendiciones();
				
				oRendicionJSONData = JSON.parse(sessionStorage.getItem("rendiciones"));
				if(!oRendicionJSONData.tipoFinanciamientos || oRendicionJSONData.tipoFinanciamientos === undefined)
					this.cargaCombobox(null,"tipoFinanciamientos");
               
				if(!oRendicionJSONData.departamentos || oRendicionJSONData.departamentos === undefined)
					this.cargaCombobox(null,"departamentos");
				
				if(!oRendicionJSONData.rutFuncionarios || oRendicionJSONData.rutFuncionarios === undefined)
					this.cargaCombobox(null,"rutFuncionarios");
				
				if(!oRendicionJSONData.tipoRendicion || oRendicionJSONData.tipoRendicion === undefined)
					this.cargaCombobox(null,"tipoRendicion");
				
				if(!oRendicionJSONData.ubicaciones || oRendicionJSONData.ubicaciones === undefined)
					this.cargaCombobox(null,"location");
				
			}
			
			var oModel = new JSONModel();
			oModel.setData(oRendicionJSONData);
			oModel.setSizeLimit(2000);
			this.oJSONModel = oModel;
			this.byId("txtColegio").setText(oDatosUsuario.des_suc);
			this.byId('cbxTipoFinanciamiento').setValue(oDatosUsuario.des_fin).setSelectedKey(oDatosUsuario.cod_fin);
			var fecha = this.getFechaActual();
			this.byId("cbxFechaSol").setValue(fecha);
			
			this.getView().setModel(this.oJSONModel);
//			this.loadUbicaciones();
		},
		
		getFechaActual: function(){
			var date = new Date();
			var fechaActual = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
			return fechaActual;
		},
		
		handleLogout: function (evt) {
			sessionStorage.clear();
			delCookies();
			window.location.replace("");
		},
		
		handleNavButton: function(oEvent) {
			var sPreviousHash = sap.ui.core.routing.History.getInstance().getPreviousHash();
			this.limpiarCabecera();
			//The history contains a previous entry
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				// There is no history!	// replace the current hash with page 1 (will not add an history entry)
				this.getOwnerComponent().getRouter().navTo("Master", oPurchaseJSONData, true);
			}
		},
		
//		loadUbicaciones: function(){
//			var aTemp = [];
//			var oUbicacion1 = [];
//			var newItem = null;
//			var cbxColegio = this.getView().byId("cbxColegio");
//			cbxColegio.setBusy(true);
//			cbxColegio.removeAllItems();
//			
//			for(var i = 0; i < oRendicionJSONData.ubicaciones.length; i++){
//				var oUbicaciones = oRendicionJSONData.ubicaciones[i];
//				
//				if(oUbicaciones.Name === oDatosUsuario.cod_suc){
//					if(oUbicaciones.U_SEI_NIVEL1 && jQuery.inArray(oUbicaciones.U_SEI_NIVEL1, aTemp) < 0){
//						aTemp.push(oUbicaciones.U_SEI_NIVEL1);
//						
//						newItem = new sap.ui.core.Item({key: oUbicaciones.Name, text: oUbicaciones.U_SEI_NIVEL1});
//						cbxColegio.addItem(newItem);
//					}
//				}
//			}
//			setTimeout(function () {
//				cbxColegio.setBusy(false);
//			}, 5000);
//			//cbxColegio.setBusy(false);
//		},

		handleChange: function(oEvent) {
			this.fecha1 = oEvent.getParameter("value");
		},
		
		handleAceptBtn: function(oEvent) {
			sap.ui.core.BusyIndicator.show(0);
			this.mensaje = null;
			this.handleCabecera();
			
			if(this.mensaje === null) {
				var rendicion = {};
				
				rendicion.U_SEI_RUTRESPO 	= this.byId("txtRutResponsable").getText();
				rendicion.U_SEI_NRESP		= this.byId("inpNomFunc").getValue();
				rendicion.U_SEI_UNIDAD		= this.byId("txtColegio").getText();
				rendicion.U_SEI_DESACTI		= this.byId("txtDescAct").getValue();
				rendicion.U_SEI_DEPAR		= this.byId("cbxDepartamento").getSelectedKey();
				
				rendicion.U_SEI_FECHAS	 	= this.byId("cbxFechaSol").getValue();
				rendicion.U_SEI_MONTSOLI 	= this.byId("txtPagFuncionario").getValue();
				rendicion.U_SEI_FINACI		= this.byId("cbxTipoFinanciamiento").getSelectedKey();
				rendicion.U_SEI_FECHACTI 	= this.byId("cbxFechaAct").getValue();
				
				//rendicion.U_SEI_UCREATOR	= oDatosUsuario.user_code;
				
				var res = setSolRendiciones(rendicion);
				sap.ui.core.BusyIndicator.hide();
				if(res > 0){
					this.limpiarCabecera();
				}
			} else {
				sap.m.MessageBox.show(this.mensaje, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Informacion",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		},
		
		// A continuación se muestra la definición de la función para llamar a la capa de servicio //
		// para cancelar un pedido de cliente abierto //
		handleCancelBtn: function(oEvent) {
			this.limpiarCabecera()
			
		},
		
		handleFuncionario: function(oControlEvent) {
			var oValidaControl = this.validaComboBox(oControlEvent);
			
			if(oValidaControl.getValueState() === "None"){
				var selKey 	= oControlEvent.getParameter("selectedItem").getKey();
				var selText = oControlEvent.getParameter("selectedItem").getText();
				
				this.byId("txtRutResponsable").setText(selKey);	
			}
		},
		
		handleColegio: function(oControlEvent){
			var oValidaControl = this.validaComboBox(oControlEvent);
			if(oValidaControl.getValueState() === "None"){
				var selKey = oControlEvent.getParameter("selectedItem").getKey();
				var selText = oControlEvent.getParameter("selectedItem").getText();
				
				//this.byId("txtNomFuncionario").setText(selKey);
			}
		},
		
		handleDepartamento: function(oControlEvent){
			var oValidaControl = this.validaComboBox(oControlEvent);
			if(oValidaControl.getValueState() === "None"){
				var selKey = oControlEvent.getParameter("selectedItem").getKey();
				var selText = oControlEvent.getParameter("selectedItem").getText();
				
				//this.byId("txtNomFuncionario").setText(selKey);
			}
		},
		
		handleFinanciamiento: function(oControlEvent){
			var oValidaControl = this.validaComboBox(oControlEvent);
			if(oValidaControl.getValueState() === "None"){
				var selKey = oControlEvent.getParameter("selectedItem").getKey();
				var selText = oControlEvent.getParameter("selectedItem").getText();
				
				//this.byId("txtNomFuncionario").setText(selKey);
			}
		},
		
		validaComboBox: function(oEvent){
			var oValidatedComboBox = oEvent.getSource(),
		    sSelectedKey = oValidatedComboBox.getSelectedKey(),
		    sValue = oValidatedComboBox.getValue();
		
			if(!sSelectedKey && sValue) {
				oValidatedComboBox.setValueState("Error");
				oValidatedComboBox.setValueStateText("Porfavor Ingresa un valor Valido!");
			} else {
				oValidatedComboBox.setValueState("None");
			}
			return oValidatedComboBox;
		},
		
		cargaCombobox: function(oControlEvent, idCbx) {
			if(idCbx === null)
				idCbx = "cbxFuncionario";
			
			switch(idCbx){
			case "tipoFinanciamientos":
				getAsync('tipoFinanciamientos');
				oRendicionJSONData.tipoFinanciamientos = JSON.parse(sessionStorage.getItem('tipoFinanciamientos')).value;
				break;
			case "rutFuncionarios":
				getAsync('rutFuncionarios');
				oRendicionJSONData.rutFuncionarios = JSON.parse(sessionStorage.getItem('rutFuncionarios')).value;
				break;
			case"departamentos":
				getAsync('departamentos');
				oRendicionJSONData.departamentos = JSON.parse(sessionStorage.getItem('departamentos')).value;
				break;
			case"location":
				getAsync('location');
				oRendicionJSONData.ubicaciones = JSON.parse(sessionStorage.getItem('location')).value;
				break;
			}
		},
		
		handleCabecera:function(){
			// Validacion Cabecera
			if(!this.getView().byId("txtRutResponsable").getText())
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No se ha Seleccionado Funcionario." : "- No se ha Seleccionado Funcionario.");
			
			if(!this.getView().byId("inpNomFunc").getValue() || this.getView().byId("inpNomFunc").getValue() === "Selec. Funcionario")
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No se ha Seleccionado Funcionario." : "- No se ha Seleccionado Funcionario.");
			
			if(!this.getView().byId("txtDescAct").getValue())
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No se ha Descrito el tipo de Actividad." : "- No se ha Descrito el tipo de Actividad.");
			
			if(!this.getView().byId("cbxDepartamento").getValue() || this.getView().byId("cbxDepartamento").getValue() === "Selec. Departamento")
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No se ha Seleccionado Departamento." : "- No se ha Seleccionado Departamento.");
			
			if(!this.getView().byId("cbxFechaSol").getValue())
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No se ha Seleccionado Fecha de Solicitud." : "- No se ha Seleccionado Fecha de Solicitud.");
			
			if(!this.getView().byId("txtPagFuncionario").getValue())
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No se ha Igresado el Monto Solicitado." : "- No se ha Ingresado el Monto Solicitado.");
			
			if(!this.getView().byId("cbxTipoFinanciamiento").getValue() || this.getView().byId("cbxTipoFinanciamiento").getValue() === "Selec. Financiamiento")
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No se ha Seleccionado el Financiamiento." : "- No se ha Seleccionado el Financiamiento.");
			
			if(!this.getView().byId("cbxFechaAct").getValue())
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No se ha Seleccionado Fecha de la Actividad." : "- No se ha Fecha de la Actividad.");
		},
		
		limpiarCabecera: function(){
			this.byId("txtRutResponsable").setText("");	
			this.byId("inpNomFunc").setValue("Selec. Responsable");
			this.byId("txtDescAct").setValue("");
			this.byId("cbxDepartamento").setValue("Selec. Departamento");
			
			this.byId("cbxFechaSol").setValue("");
			this.byId("txtPagFuncionario").setValue("");
			this.byId("cbxTipoFinanciamiento").setValue("Selec. Financiamiento");
			this.byId("cbxFechaAct").setValue("");
		},
		
		delay : function(componente) {
			return new Promise(resolve => {
			    setTimeout(() => {
			    	componente.setBusy(false);
			    }, 5000);
			 });
		},
		
		handleClose: function(oEvent) {
			oEvent.getSource().close();
			//oEvent.getSource().destroy();
		},
	
		onValueFunc: function() {
			this._oInput = this.getView().byId("inpNomFunc");
			this.oColModel = new sap.ui.model.json.JSONModel();
			this.oColModel.setData({"cols": [{ "label": "Rut Funcionario", "template": "CardCode", "width": "10rem" }, { "label": "Nombre Funcionario", "template": "CardName" }]});
			
			var aCols = this.oColModel.getData().cols;
			this._oBasicSearchField = new sap.m.SearchField({showSearchButton: false});
			
			this._oValueHelpDialog = sap.ui.xmlfragment("SeidorB1UI5.view.fragment.Funcionario", this);
			this.getView().addDependent(this._oValueHelpDialog);

			this._oValueHelpDialog.getFilterBar().setBasicSearch(this._oBasicSearchField);
			
			this._oValueHelpDialog.getTableAsync().then(function (oTable) {
				oTable.setModel(this.oJSONModel);
				oTable.setModel(this.oColModel, "columns");

				if (oTable.bindRows) {
					oTable.bindAggregation("rows", "/rutFuncionarios");
				}

				if (oTable.bindItems) {
					oTable.bindAggregation("items", "/rutFuncionarios", function () {
						return new sap.m.ColumnListItem({
							cells: aCols.map(function (column) {
								return new sap.m.Label({ text: "{" + column.template + "}" });
							})
						});
					});
				}

				this._oValueHelpDialog.update();
			}.bind(this));

			var oToken = new sap.m.Token();
			oToken.setKey(this._oInput.getSelectedKey());
			oToken.setText(this._oInput.getValue());
			this._oValueHelpDialog.setTokens([oToken]);
			this._oValueHelpDialog.open();
		},

		onFilterBarFunc: function (oEvent) {
			var sSearchQuery = this._oBasicSearchField.getValue(),
				aSelectionSet = oEvent.getParameter("selectionSet");
			var aFilters = aSelectionSet.reduce(function (aResult, oControl) {
				if (oControl.getValue()) {
					aResult.push(new sap.ui.model.Filter({
						path: oControl.getName(),
						operator: sap.ui.model.FilterOperator.Contains,
						value1: oControl.getValue()
					}));
				}

				return aResult;
			}, []);

			aFilters.push(new sap.ui.model.Filter({
				filters: [
					new sap.ui.model.Filter({ path: "CardCode", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery }),
					new sap.ui.model.Filter({ path: "CardName", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery })
				],
				and: false
			}));

			this._filterTableFunc(new sap.ui.model.Filter({
				filters: aFilters,
				and: true
			}));
		},
		
		_filterTableFunc: function (oFilter) {
			var oValueHelpDialog = this._oValueHelpDialog;

			oValueHelpDialog.getTableAsync().then(function (oTable) {
				if (oTable.bindRows) {
					oTable.getBinding("rows").filter(oFilter);
				}

				if (oTable.bindItems) {
					oTable.getBinding("items").filter(oFilter);
				}

				oValueHelpDialog.update();
			});
		},
		
		/**
		 * Funcion de Evento Seleccion de Linea de la tabla. que carga los valores en los campos
		 * Cliente - Nombre y llama a los acuerdos globales asociados al socio de negocio seleccionado
		 * para llenar los datos en combobox Acuedo globales.
		 */
		onValueFuncOk: function (oEvent) {
			sap.ui.core.BusyIndicator.show(0);
			var aTokens = oEvent.getParameter("tokens");
			this._oInput.setValue(aTokens[0].getKey());
			var nom = aTokens[0].getText();
			nom = nom.replace(" ("+ aTokens[0].getKey() +")", "");
			
			this.byId("txtRutResponsable").setText(aTokens[0].getKey());
			this._oInput.setValue(nom);
			
			//this.cargaSaldoFuncionario(aTokens[0].getKey(), nom);
			
			sap.ui.core.BusyIndicator.hide();
			
			this._oValueHelpDialog.close();
		},

		/**
		 * Funcion de Evento Boton Cancelar. el que llama a la funcion para el cierre 
		 * de la ventana oValueHelpDialog.
		 */
		onValueFuncCancel: function () {
			this._oValueHelpDialog.close();
		},

		onValueFuncClose: function () {
			this._oValueHelpDialog.destroy();
		}
		
	});
	return SolRendicionCreateController;
});