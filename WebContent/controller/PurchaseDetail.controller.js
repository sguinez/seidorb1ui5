jQuery.sap.require("sap.ui.core.format.DateFormat", "SeidorB1UI5.controller.Formatter");

sap.ui.core.mvc.Controller.extend("SeidorB1UI5.controller.PurchaseDetail", {
	
	formatter: SeidorB1UI5.controller.Formatter,
	
	// MODIFICAR //
	// A continuación se muestra el procedimiento de enlace de la vista Detalles para mostrar los datos maestros de elementos en el área del encabezado //
	// MODIFICAR //
	onInit: function() {
		oDimenJSONData = JSON.parse(sessionStorage.getItem("location"));
		oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
		var oModel = null;
		var view = this.getView();
		this.estado = null;
		
		if(oDatosUsuario.code_rol !== "1") {
			//OCULTA BOTON SI ES NECESARIO SEGUN ID DE COMPONENTE
			var btnApro = view.byId("btnAprobar");
			var btnRech = view.byId("btnRechazar");
			
			btnApro.setVisible(false);
			btnRech.setVisible(false);
        }
    	sap.ui.core.UIComponent.getRouterFor(this).attachRouteMatched(function(oEvent) {
    		oModel = new sap.ui.model.json.JSONModel();
    		oModel.setData(oPurchaseJSONData);
        	view.setModel(oModel);
			
			if (oEvent.getParameter("name") === "PurchaseDetail") {
				if(ordersTable){				
					var oListItem = ordersTable;			    
					var context = new sap.ui.model.Context(view.getModel(), oListItem.getBindingContext().getPath());
					this.path = oListItem.getBindingContext().getPath().substr(7);
					view.setBindingContext(context);
					
					if((oDatosUsuario === undefined || !oDatosUsuario) && sessionStorage.getItem("tiles")){
						oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
					} else if(!sessionStorage.getItem("tiles")){
						this.handleLogout();
					}
					
					var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
					this.byId("avatar").setInitials(iAvatar);
					this.byId("avatar").setTooltip("Usuario: " + avatar);
					
					// Asegúrate de que el maestro esté aquí
					var oItemsTbl = this.getView().byId("idItemsTable");
					var oColListItem = this.getView().byId("idColListItem");
					oItemsTbl.setModel(oModel);
					oItemsTbl.bindItems(context.sPath + "/DocumentLines", oColListItem);
					
					var items = this.getView().byId("idItemsTable").getItems();
					//ISBN cuando no se ingresa guarda 0 entonces si viene en 0 se reemplaza con vacio y asi no habilita campo
					for(var item = 0; item < items.length; item++){
						if(items[item].getCells()[5].getValue() === "0" || items[item].getCells()[5].getValue() === null)
							items[item].getCells()[5].setValue("");
					}
					if(this.byId("txtFechApro").getText() === "" || this.byId("txtFechApro").getText() === null)
						this.byId("txtFechApro").setText("N/A");
					this.handleUnidad();
					this.traduceEstadoDoc();
				} else {
					this.handleNavButton();
				}
		    }
		}, this);
	},
	
	handleLogout: function (evt) {
		sessionStorage.clear();
		delCookies();
		window.location.replace("");
	},
	
	/*
	 * carga de combobox unidad 2 segun la unidad seleccionada en cabecera
	 */
	handleUnidad: function(oEvent) {
		var items = this.getView().byId("idItemsTable").getItems();
		var oModel = this.getView().byId("idItemsTable").getModel();
								
		for(var item = 0; item < items.length; item++){
			var linea = items[item].getCells()[0].getText();
			this.unidad = (items[item].getCells()[8].getText() ? items[item].getCells()[8].getText() : items[items.length - 1].getCells()[8].getText())
			this.unidad = items[item].getCells()[8].getText();
			
			var aTemp = [];
			var aDimension2 = [];
			for(var i = 0; i < oDimenJSONData.value.length; i++){
				var oDimensiones = oDimenJSONData.value[i];				
				//if(oDimensiones.U_SEI_NIVEL1 === this.unidad && 
				if(oDimensiones.U_SEI_NIVEL1 === this.unidad && oDimensiones.U_SEI_NIVEL2 && jQuery.inArray(oDimensiones.U_SEI_NIVEL2, aTemp) < 0){
					aTemp.push(oDimensiones.U_SEI_NIVEL2);
					aDimension2.push({"Name": oDimensiones.U_SEI_NIVEL2});
					this.cardCode = oDimensiones.U_SEI_COLEGIO;
				}
			}
			if (linea){
				oPurchaseJSONData.value[this.path].DocumentLines[item].Dimension2 = aDimension2;
//				oPurchaseJSONData.value[this.path].DocumentLines[linea].Dimension2 = aDimension2;
				this.cargaCbxUnidad(items[item].getCells()[9].getValue(), items[item].getCells()[10].getValue(), item)
//				this.cargaCbxUnidad(items[item].getCells()[9].getValue(), items[item].getCells()[10].getValue(), linea)
			} else if (linea === ""){
				oPurchaseJSONData.value[this.path].DocumentLines[items.length -1].Dimension2 = aDimension2;
			}
			oModel.setData(oPurchaseJSONData);
			oModel.refresh();
		}
	},
	
	cargaCbxUnidad: function(unidad2, unidad3, linea){
		var aTemp = [], aDimension3 = [], aDimension4 = [];
		
		for(var i = 0; i < oDimenJSONData.value.length; i++){
			var oDimensiones = oDimenJSONData.value[i];				
			if(oDimensiones.U_SEI_NIVEL2 === unidad2 && jQuery.inArray(oDimensiones.U_SEI_NIVEL3, aTemp) < 0){						
				aTemp.push(oDimensiones.U_SEI_NIVEL3);
				aDimension3.push({"Name": oDimensiones.U_SEI_NIVEL3});						
			}
			
			if(oDimensiones.U_SEI_NIVEL2 === unidad2 &&  oDimensiones.U_SEI_NIVEL3 === unidad3 && jQuery.inArray(oDimensiones.U_SEI_NIVEL4, aTemp) < 0){
				aTemp.push(oDimensiones.U_SEI_NIVEL4);
				aDimension4.push({"Name": oDimensiones.U_SEI_NIVEL4});
			}
		}
		oPurchaseJSONData.value[this.path].DocumentLines[linea].Dimension3 = aDimension3;
		
		if(aDimension4)
			//provisorio
			oPurchaseJSONData.value[this.path].DocumentLines[linea].Dimension4 = aDimension4
	},
	
	handleLoadItems: function(oControlEvent, idCbx) {
		//var selKey = oControlEvent.getParameter("selectedItem").getKey();
		var selText = oControlEvent.getParameter("selectedItem").getText();
		var linea = oControlEvent.getSource().getBindingContext().getObject().LineNum;
		var cadena = oControlEvent.getSource().getBindingContext().sPath;
		var largo = cadena.length;
		cadena = cadena.substring(largo, largo-2);
		
		if(linea === undefined)
			linea = cadena.replace("/", "");
				
		var oModel = new sap.ui.model.json.JSONModel();
		var aTemp = [];
		var aDimension3 = [];
		var aDimension4 = [];			
		
			
		if(idCbx === "dimension2") {
			var items = this.getView().byId("idItemsTable").getItems();
			this._dimension2 = selText;
			
			for(var item = 0; item < items.length; item++){
				if(items[item].getCells()[0].getText() == linea){
					items[item].getCells()[10].setValue("");
					items[item].getCells()[11].setValue("");
				}
			}
			
			this._dimension2 = selText;			
			for(var i = 0; i < oDimenJSONData.value.length; i++){
				var oDimensiones = oDimenJSONData.value[i];				
				if(oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
				   oDimensiones.U_SEI_NIVEL3 && 
				   jQuery.inArray(oDimensiones.U_SEI_NIVEL3, aTemp) < 0){						
					aTemp.push(oDimensiones.U_SEI_NIVEL3);
					aDimension3.push({"Name": oDimensiones.U_SEI_NIVEL3});						
				} else if(oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
						   !oDimensiones.U_SEI_NIVEL3 && 
						   jQuery.inArray(oDimensiones.U_SEI_NIVEL4, aTemp) < 0){
					aTemp.push(oDimensiones.U_SEI_NIVEL4);
					aDimension4.push({"Name": oDimensiones.U_SEI_NIVEL4});
				}
			}
			oPurchaseJSONData.value[this.path].DocumentLines[linea].Dimension3 = aDimension3;
			
			if(aDimension4)
				//provisorio
				oPurchaseJSONData.value[this.path].DocumentLines[linea].Dimension4 = aDimension4
			
		} else if(idCbx === "dimension3") {
			
			this.getView().byId("idItemsTable").getItems()[linea].getCells()[11].setValue("");
			
			if(this._dimension2 == undefined || this._dimension2 == "" || this._dimension2 == null){
				this._dimension2 = this.getView().byId("idItemsTable").getItems()[linea].getCells()[9].getValue();
			}
			
			this._dimension3 = selText;
			for(var i = 0; i < oDimenJSONData.value.length; i++){
				var oDimensiones = oDimenJSONData.value[i];				
				if(oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
				   oDimensiones.U_SEI_NIVEL3 === this._dimension3 && 
				   oDimensiones.U_SEI_NIVEL4 && 
				   jQuery.inArray(oDimensiones.U_SEI_NIVEL4, aTemp) < 0){
					
					aTemp.push(oDimensiones.U_SEI_NIVEL4);
					aDimension4.push({"Name": oDimensiones.U_SEI_NIVEL4});
				}
			}
			oPurchaseJSONData.value[this.path].DocumentLines[linea].Dimension4 = aDimension4			
		}			
		oModel.setData(oPurchaseJSONData);
		oModel.refresh();
	},
	
	/*
	 * Habilita los botones a la izquierda del pie de la página
	 */
	handleHabilBtn:function(oEvent) {
		sap.ui.core.BusyIndicator.show(0);
		var docStatusTxt = this.getView().byId("EstadoCab").getText();
		if(docStatusTxt === "Pendiente"){
			var items = this.getView().byId("idItemsTable").getItems();
			
			for(var item = 0; item < items.length; item++){
				for(var cell = 0; cell < items[item].getCells().length; cell++){
					if(cell !== 0 && cell !== 1 && cell !== 2 && cell !== 6 && cell !== 7 && cell !== 8 && cell !== 12 && cell !== 13)
						items[item].getCells()[cell].setEnabled(true);
						if(!items[item].getCells()[5].getValue()) {
							items[item].getCells()[5].setEnabled(false);
						}
						
//						if(!items[item].getCells()[8].getText() && !items[item].getCells()[9].getValue()) {
//							items[item].getCells()[9].setEnabled(false);
//							items[item].getCells()[10].setEnabled(false);
//							items[item].getCells()[11].setEnabled(false)
//						}
				}
			}
			this.handleUnidad();
			this.getView().byId("idItemsTable").setMode("MultiSelect");
			this.habilitaBotones();
			sap.ui.core.BusyIndicator.hide();
		} else {
			new sap.m.MessageBox.show("El documento no se puede Modificar porque no se encuentra Pendiente.", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Información",
				onClose: function(oAction){
					sap.ui.core.BusyIndicator.hide();
				}
			});
		}
	},
	
	/*
	 * Funcion encargada de actualiza los campos de la tabla
	 */
	handleUpdateBtn: function(oEvent){
		var purchase = {DocumentLines:[]};	
		var items = this.getView().byId("idItemsTable").getItems();
		var docEntry = this.getView().byId("text1").getText();
		var loRouter = sap.ui.core.UIComponent.getRouterFor(this);

		for(var item = 0; item < items.length; item++){
			var documentLine = {};	
			if(items[item].getCells()[0].getText()){
				documentLine = {
						LineNum: 		items[item].getCells()[0].getText(),
						FreeText: 		items[item].getCells()[3].getValue().slice(0, 100),
						Quantity: 		items[item].getCells()[4].getValue(),
						U_SEI_ISBN: 	items[item].getCells()[5].getValue(),
						U_SEI_NIVEL2: 	items[item].getCells()[9].getValue(),
						U_SEI_NIVEL3: 	items[item].getCells()[10].getValue(),
						U_SEI_NIVEL4: 	items[item].getCells()[11].getValue()
				}
				purchase.DocumentLines.push(documentLine);
			}
			
			if (!items[item].getCells()[0].getText()) {
				documentLine = {
						ItemCode: 			items[item].getCells()[1].getText(),							
						ItemDescription:	items[item].getCells()[2].getText(),
						FreeText:			items[item].getCells()[3].getValue().slice(0, 100),							
						Quantity:			items[item].getCells()[4].getValue(),
						RequieredDate:		oPurchaseJSONData.value[this.path].DocumentLines[0].RequiredDate,
						U_SEI_ISBN:			items[item].getCells()[5].getValue(),
						U_SEI_NIVEL1:		oPurchaseJSONData.value[this.path].DocumentLines[0].U_SEI_NIVEL1,//unidad descripcion
						U_SEI_NIVEL2:		items[item].getCells()[9].getValue(),
						U_SEI_NIVEL3:		items[item].getCells()[10].getValue(),
						U_SEI_NIVEL4:		items[item].getCells()[11].getValue(),
						CostingCode:		oPurchaseJSONData.value[this.path].DocumentLines[0].CostingCode,//unidad codigo
						CostingCode2:		oPurchaseJSONData.value[this.path].DocumentLines[0].CostingCode2,//tipoFinanciamiento
						CostingCode3:		oPurchaseJSONData.value[this.path].DocumentLines[0].CostingCode3//Departamento codigo)
				}
				purchase.DocumentLines.push(documentLine);
			}
		}		
		if(!updatePurchase(docEntry, purchase)){
			var oView = this;
			
			new sap.m.MessageBox.show(
					"Modificacion exitosa. Los cambios realizados, fueron actualizados con exito en SAP.", { 
               	icon: sap.m.MessageBox.Icon.SUCCESS, 
              	title: "Modificacion de La Solicitud.",
              	onClose: function(oAction){
              		oView.desabilitaBotones();
              		oView.handleNavButton();
              	}
            });
		}
	},
	
	/*
	 * Elimina celdas seleccionadas de la tabla
	 */
	handleEliminBtn: function(oEvent) {
		if(this.estado === null){
			var oTable = this.getView().byId("idItemsTable");
		    var data = oTable.getModel();
		    var selRowCount = oTable.getSelectedItems().length;
		    var docEntry = this.getView().byId("text1").getText();
		    var rowNum;
		    
		    
		    if(selRowCount > 0){
		    	if(selRowCount < oTable.getItems().length){
		    		var contRev = selRowCount;
				    for (var i = selRowCount; i > 0; i--) {
				    	var cadena = oTable.getSelectedItems()[i-1].getCells()[0].sId;
						var largo = cadena.length;
				    	
				    	rowNum = oTable.getSelectedItems()[i-1].getCells()[0].sId.substring(largo, largo-2).replace("-", "");
						var oData = data.getData();
						oData.value[this.path].DocumentLines.splice(rowNum, 1);
						data.setData(oData);
						data.refresh();
				    }
				    
				    var purchase = {DocumentLines:[]};
					var documentLine = {};		
					var items = this.getView().byId("idItemsTable").getItems();
					var docEntry = this.getView().byId("text1").getText();
					var loRouter = sap.ui.core.UIComponent.getRouterFor(this);
	
					for(var item = 0; item < items.length; item++){
						if(items[item].getCells()[0].getText()){
							documentLine = { LineNum: items[item].getCells()[0].getText() }
							purchase.DocumentLines.push(documentLine);
						}
						if (!items[item].getCells()[0].getText()) {
							documentLine = {
									ItemCode: 			items[item].getCells()[1].getText(),							
									ItemDescription:	items[item].getCells()[2].getText(),
									FreeText:			items[item].getCells()[3].getValue().slice(0, 100),							
									Quantity:			items[item].getCells()[4].getValue(),
									RequieredDate:		oPurchaseJSONData.value[this.path].DocumentLines[0].RequiredDate,
									U_SEI_ISBN:			items[item].getCells()[5].getValue(),
									U_SEI_NIVEL1:		oPurchaseJSONData.value[this.path].DocumentLines[0].U_SEI_NIVEL1,//unidad descripcion
									U_SEI_NIVEL2:		items[item].getCells()[9].getValue(),
									U_SEI_NIVEL3:		items[item].getCells()[10].getValue(),
									U_SEI_NIVEL4:		items[item].getCells()[11].getValue(),
									CostingCode:		oPurchaseJSONData.value[this.path].DocumentLines[0].CostingCode,//unidad codigo
									CostingCode2:		oPurchaseJSONData.value[this.path].DocumentLines[0].CostingCode2,//tipoFinanciamiento
									CostingCode3:		oPurchaseJSONData.value[this.path].DocumentLines[0].CostingCode3//Departamento codigo)
							}
							purchase.DocumentLines.push(documentLine);
						}
					}
					if(!delLinePurchase(docEntry, purchase)){
						this.getView().byId("idItemsTable").setMode("None");
						this.desabilitaBotones;
						loRouter.navTo("PurchaseMaster");
					}
		    	} else {
		    		new sap.m.MessageBox.show("Se Debe Dejar al menos una Linea en la Solicitud", {
						icon: sap.m.MessageBox.Icon.WARNING,
						title: "Informacion Crear Solicitud de Orden",
						onClose: function(oAction) {					
				    		sap.ui.core.BusyIndicator.hide();
						}
					});
		    	}
			} else {
				new sap.m.MessageBox.show("No se ha seleccionado lineas a eliminar", {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: "Informacion Crear Solicitud de Orden",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		}else{
			new sap.m.MessageBox.show("Existen acciones pendientes por Procesar.\n La accion pendiente es: " + this.estado + ".", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Informacion Modificar Solicitud de Orden",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		}
	},
	
	// A continuación se muestra la definición de la función para 
	// llamar a la capa de servicio para "Aprobar" un pedido de cliente "Pendiente"
	handleAceptBtn: function(oEvent) {
		sap.ui.core.BusyIndicator.show(0);
//		var oListItem = ordersList.getSelectedItem();
//		oListItem.setMarkLocked(true);
//		ordersList.setSelectedItem(false);
//		
//		var id = oListItem.getBindingContext().getPath().substr(7);
		var oFecha = sap.ui.core.format.DateFormat.getDateTimeInstance({ pattern: "yyyy-MM-dd" });		
		var oDate = new Date();
		var sFech = oFecha.format(oDate);
		
		var docEntry = this.getView().byId("text1");
		var docStatusTxt = this.getView().byId("text2");
		
		if(docStatusTxt.getText() === "Pendiente"){
			docStatusTxt.setText("Aprobado");		
			var body = { "U_SEI_ESTADO": "Aprobado", "U_SEI_FECHAPRO": sFech};
			
			updatePurchase(docEntry.getText(), body);
			new sap.m.MessageBox.show("El documento Cambio de estado a Aprobado", {
				icon: sap.m.MessageBox.Icon.SUCCESS,
				title: "Informacion",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
			//aceptPurchase("T");
		} else if (docStatusTxt.getText() === "Aprobado"){
			new sap.m.MessageBox.show("El documento ya se encuentra Aprobado", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Información",
				onClose: function(oAction){
					sap.ui.core.BusyIndicator.hide();
				}
			});
		} else {
			new sap.m.MessageBox.show("El documento no se encuentra Pendiente", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Informacion",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		}
	},
	
	// A continuación se muestra la definición de la función para 
	// llamar a la capa de servicio para "Rechazar" un pedido de cliente "Pendiente"
	handleCancelBtn: function(oEvent) {
//		var oListItem = ordersList.getSelectedItem();
//	    oListItem.setMarkLocked(true);
//	    ordersList.setSelectedItem(false);
//	    //
		var oFecha = sap.ui.core.format.DateFormat.getDateTimeInstance({ pattern: "yyyy-MM-dd" });		
		var oDate = new Date();
		var sFech = oFecha.format(oDate);
		
		var docEntry = this.getView().byId("text1");
		var docStatusTxt = this.getView().byId("text2");
		
		if(docStatusTxt.getText() === "Pendiente"){
			docStatusTxt.setText("Rechazada");		
			var body = { "U_SEI_ESTADO": "Rechazado", "U_SEI_FECHAPRO": sFech};
			
			updatePurchase(docEntry.getText(), body);
			new sap.m.MessageBox.show("El documento Cambio de estado a Rechazado", {
				icon: sap.m.MessageBox.Icon.SUCCESS,
				title: "Informacion",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		} else {
			new sap.m.MessageBox.show("El documento no se encuentra Pendiente!", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Informacion",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		}		
	},
			
	handleNavButton: function(oEvent) {
		if (this._oDialog)
			this.cierraFragment();
		
		var sPreviousHash = sap.ui.core.routing.History.getInstance().getPreviousHash();
		this.getView().byId("idItemsTable").setMode("None");
		this.desabilitaBotones();
		//The history contains a previous entry
		if (sPreviousHash !== undefined) {
			window.history.go(-1);
		} else {
			// There is no history!
			// replace the current hash with page 1 (will not add an history entry)
			this.getOwnerComponent().getRouter().navTo("PurchaseMaster", oItems, true);
		}
	},
	
	validaUbicaciones: function(){
		var items = this.getView().byId("idItemsTable").getItems();
		var combo = oPurchaseJSONData.value[this.path].DocumentLines[items.length-1]
		if(!combo.U_SEI_NIVEL2){
			if(items[items.length-1].getCells()[9].getEnabled() === true){
				if(items[items.length-1].getCells()[9].getValue() && 
				   items[items.length-1].getCells()[10].getValue() && 
					(combo.Dimension4.length > 0 && items[items.length-1].getCells()[11].getValue())){
					this.mensaje = null;
				} else if(items[items.length-1].getCells()[9].getValue() && 
						  items[items.length-1].getCells()[10].getValue() && 
						  combo.Dimension4.length === 0 ){
					this.mensaje = null;
				} else {
					this.mensaje = "- Debe seleccionar Ubicaciones.";
				}
			}
		}
	},
	
	validacionEnvio: function() {
		this.mensaje = null;
		//Obtengo los Items de la Tabla
		var items = this.getView().byId("idItemsTable").getItems();
		//Valida Datos de la cabecera
		if(this.getView().byId("cbxPrioridad").getValue() === "Selecciona Prioridad")
			this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -Debes Selecciona una Prioridad." : "-Debes Selecciona una Prioridad");
		
		if(this.fecha1 === null)
			this.mensaje = "- Se debe seleccionar fecha de Solicitud.";
		
		if(this.getView().byId("cbxEspecialidad").getValue() === "Selecciona Especialidad")
			this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -Debes Seleccionar una Especialidad." : "-Debes Seleccionar una Especialidad");
			
		
		if(!this.getView().byId("txtAsunto").getValue() || !this.getView().byId("txtComentarios").getValue())
			this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -No hay Asunto y/o Comentarios para crear una Llamada." : "-No hay Asunto y/o Comentarios para crear una Llamada.");
			
		//Valida datos de la Tabla
		if(items.length === 0)
			this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -No hay Ubicaciones Seleccionadas para crear una Llamada." : "-No hay Ubicaciones Seleccionadas para crear una Llamada.");
		
		for(var i = 0; i < items.length; i++){
			if(!items[items.length-1].getCells()[0].getValue() && !items[items.length-1].getCells()[2].getValue() && (oServiceJSONData.Dimension4 && items[items.length-1].getCells()[2].getValue() !== "")){
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -Debe seleccionar Ubicaciones." : "- Debe seleccionar Ubicaciones.");
			} else {
				!items[items.length-1].getCells()[2].getValue();
				
			}
		}
	},
	
	_filter : function (oEvent) {
		var oFilter = null;

		if (this._oGlobalFilter && this._oPriceFilter) {
			oFilter = new sap.ui.model.Filter([this._oGlobalFilter, this._oPriceFilter], true);
		} else if (this._oGlobalFilter) {
			oFilter = this._oGlobalFilter;
		} else if (this._oPriceFilter) {
			oFilter = this._oPriceFilter;
		}

		// actualizar la lista de enlace	
		var oBinding = oEvent.getSource().getBinding("items");
		oBinding.filter(oFilter);		
	},

	filterGlobally : function(oEvent) {
		var sQuery = oEvent.getParameter("value");
		this._oGlobalFilter = null;

		if (sQuery) {
			this._oGlobalFilter = new sap.ui.model.Filter([
				new sap.ui.model.Filter("ItemName", sap.ui.model.FilterOperator.Contains, sQuery),
				new sap.ui.model. 
				Filter("ItemCode", sap.ui.model.FilterOperator.Contains, sQuery)
			], false);
		}

		this._filter(oEvent);
	},
	
	/*
	 * Reestablece los campos a de la cabecera a como estaban inicialmente la pagina
	 */
	limpiarCampos: function(){			
		this.byId("cbxUnidad").setValue(JSON.parse(sessionStorage.getItem("tiles")).des_suc);
		this.byId("cbxPrioridad").setValue("Selecciona Prioridad");
		this.byId("DP1").setDateValue(new Date());
		this.byId("cbxEspecialidad").setValue("Selecciona Especialidad");
		this.byId("txtAsunto").setValue("");
		this.byId("txtComentarios").setValue("");
	},
	
	onExit: function() {
		if (this._oActionSheet) {
			this._oActionSheet.destroy();
			this._oActionSheet = null;
		}
		//this.oProductsModel.destroy();
	},

	handleTableSelectDialog: function(oEvent) {
		this.mensaje= null;
		this.oItems = JSON.parse(sessionStorage.getItem("items"));
		this.oJSONModel = this.loadCombobox();		
		this.validaUbicaciones();
		if(!this.mensaje){
			if (!this._oDialog) {
				this._oDialog = sap.ui.xmlfragment("SeidorB1UI5.view.ItemsMaster", this);
				this._oDialog.setModel(this.oJSONModel);
			}
			this.getView().addDependent(this._oDialog);		
			this._oDialog.open();
		} else {
			new sap.m.MessageBox.show("Debes seleccionar la unidad antes de agregar una nueva linea ", {
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Informacion Solicitud De Orden",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		}
	},
	
	handleSelect: function(oEvent) {
		var aContexts = oEvent.getParameter("selectedContexts");			
		// Si se selecciono un items entra en esta validacion y la agrega a la tabla
		if(aContexts){
			var itemCode,itemName,itemsGroupCode,virtualAssetItem,inventoryItem,Properties1;
			
			if (aContexts && aContexts.length){
				aContexts.map(function(oContext) { 
					itemCode 			= oContext.getObject().ItemCode;
					itemName 			= oContext.getObject().ItemName;
					itemsGroupCode 		= oContext.getObject().ItemsGroupCode;
					virtualAssetItem 	= oContext.getObject().VirtualAssetItem;
					inventoryItem		= oContext.getObject().InventoryItem;
					Properties1			= oContext.getObject().Properties1;
				});
			}
			
			this.cierraFragment();
			var date = new Date();
			//var fechaActual = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
			
			var itemPur = oPurchaseJSONData.value[this.path];
			var itemDoc = oPurchaseJSONData.value[this.path].DocumentLines;
			var items = this.getView().byId("idItemsTable").getItems();
			var fechaActual = items[items.length-1].getCells()[6].getText();
			var costingCode = items[items.length-1].getCells()[7].getText();
			//Actualiza la cantidad en el jsondata
			for(var i = 0; i < items.length; i++){				
				for(var x = 0; x < itemDoc.length; x++ ){
					if(items[i].getCells()[1].getText() === itemDoc[x].ItemCode &&
					items[i].getCells()[2].getText()  	=== itemDoc[x].ItemDescription &&
					items[i].getCells()[3].getValue()  	=== itemDoc[x].FreeText &&
					items[i].getCells()[4].getValue() 	=== itemDoc[x].Quantity.toString() &&
					items[i].getCells()[5].getValue() 	=== itemDoc[x].U_SEI_ISBN.toString() &&
					items[i].getCells()[9].getValue() 	=== itemDoc[x].U_SEI_NIVEL2 &&
					items[i].getCells()[10].getValue() 	=== itemDoc[x].U_SEI_NIVEL3 &&
					items[i].getCells()[11].getValue() 	=== itemDoc[x].U_SEI_NIVEL4 ){
						
						itemDoc[x].FreeText 		= items[i].getCells()[3].getValue().slice(0, 100);
						itemDoc[x].Quantity 		= items[i].getCells()[4].getValue();
						itemDoc[x].U_SEI_ISBN 		= items[i].getCells()[5].getValue();
						itemDoc[x].U_SEI_NIVEL2 	= items[i].getCells()[9].getValue();
						itemDoc[x].U_SEI_NIVEL3 	= items[i].getCells()[10].getValue();
						itemDoc[x].U_SEI_NIVEL4 	= items[i].getCells()[11].getValue();
						costingCode = items[i].getCells()[7].getText();
					}
				}
			}
			
			itemPur.DocumentLines.push({
								"VirtualAssetItem": virtualAssetItem,
								"ItemCode": itemCode,
								"ItemDescription": itemName,
								"FreeText": "",
								"Quantity": 1,
								"U_SEI_ISBN": "",
								"RequiredDate":fechaActual,
								"CostingCode":costingCode,
								"U_SEI_NIVEL2": "",
								"U_SEI_NIVEL3": "",
								"U_SEI_NIVEL4": "",
								"InventoryItem": inventoryItem,
								"ItemsGroupCode":itemsGroupCode,
								"Properties1":Properties1
								});
			
			var oTable = this.getView().byId("idItemsTable");
		    var oModel = oTable.getModel();
		    var oData = oModel.getData();
			oData.DocumentLines = itemPur.DocumentLines;
			oData.DocumentLines.Dimension2 = this.oItems.Dimension2;
			oModel.setData(oData);
			oModel.refresh();
			
			items = this.getView().byId("idItemsTable").getItems();
			
			items[items.length-1].getCells()[3].setEnabled(true);//FreeText
			items[items.length-1].getCells()[4].setEnabled(true);//Quantity
			
			if(itemsGroupCode === 117)
				items[items.length-1].getCells()[5].setEnabled(true);//ISBN
			
			if(Properties1 === "tYES"){
				items[items.length-1].getCells()[9].setEnabled(true);//Ubicacion2
				items[items.length-1].getCells()[10].setEnabled(true);//Ubicacion3
				items[items.length-1].getCells()[11].setEnabled(true);//Ubicacion4
			}
			this.handleUnidad();
		}
	},
	
	loadCombobox: function(){
		var _virtual = this.getView().getModel().getData().value[this.path].U_SEI_VIRTUAL;
		var oModel = new sap.ui.model.json.JSONModel();
		var aTemp = [];
		var aDimension2 = [];
		var item = {};
		var oItemsVY = {value: []};
		var oItemsVN = {value: []};
		
		if(this.oItems === undefined || !this.oItems.Dimension2){
			for(var i = 0; i < oDimenJSONData.value.length; i++){
				var oDimensiones = oDimenJSONData.value[i];				
				if(oDimensiones.U_SEI_NIVEL2 && jQuery.inArray(oDimensiones.U_SEI_NIVEL2, aTemp) < 0){
					aTemp.push(oDimensiones.U_SEI_NIVEL2);
					aDimension2.push({"Name": oDimensiones.U_SEI_NIVEL2});
				}
			}
			this.oItems.Dimension2 = aDimension2;
			oModel.setData(this.oItems);
		}
		
		
		var _items = this.oItems;
		for(var i = 0; i < _items.value.length; i++){
			if(_items.value[i].VirtualAssetItem === 'tYES')
				oItemsVY.value.push(_items.value[i]);
			
			if(_items.value[i].VirtualAssetItem === 'tNO')
				oItemsVN.value.push(_items.value[i]);
		}
		
		if(_virtual === 'tYES')
			oModel.setData(oItemsVY);
		else if(_virtual === 'tNO')
			oModel.setData(oItemsVN);
		
		return oModel;
	},
	
	Click_btnImprimir: function(){
		getReport()
	},

	traduceEstadoDoc: function(){
		var estado = this.getView().byId("text3").getText();
		switch(estado){
			case"bost_Open":
				this.getView().byId("text3").setText("Abierta");
				break;
			case"bost_Close":
				this.getView().byId("text3").setText("Cerrada");
				break;
			case"bost_Pend":
				this.getView().byId("text3").setText("Pendiente");
				break;
			case"bost_Sale":
				this.getView().byId("text3").setText("Ejemplo");
				break;
		}
	},
	
	habilitaCampos: function(){
		var items = this.getView().byId("idItemsTable").getItems();
		for(var i = 0; i < items.length; i++){
			if(items[i].getCells()[12].getText() === "117")
				items[i].getCells()[5].setEnabled(true);
			//virtualAssetItem === "tYES" || inventoryItem == "tNO"
			if(items[i].getCells()[13].getText() === "tYES"){
				items[i].getCells()[9].setEnabled(true);
				items[i].getCells()[10].setEnabled(true);
				items[i].getCells()[11].setEnabled(true);
			} else {
				items[i].getCells()[9].setEnabled(false);
				items[i].getCells()[10].setEnabled(false);
				items[i].getCells()[11].setEnabled(false);
			}
		}
	},
	
	cierraFragment: function(oEvent) {
        this._oDialog.destroy();  //Second: destoy fragment 
        this._oDialog=null;  // Third: null name/pointer 
    },
	
	desabilitaBotones: function(){
		var btnAr = this.getView().byId("btnArticulos");
		var btnUp = this.getView().byId("btnUpdate");
		var btnEl = this.getView().byId("btnElimin");
		btnAr.setVisible(false);
		btnUp.setEnabled(false);
		btnEl.setEnabled(false);
	},
	
	habilitaBotones: function(){
		var btnAr = this.getView().byId("btnArticulos");
		var btnUp = this.getView().byId("btnUpdate");
		var btnEl = this.getView().byId("btnElimin");
		btnAr.setVisible(true);
		btnUp.setEnabled(true);
		btnEl.setEnabled(true);
		this.habilitaCampos();
	}
});