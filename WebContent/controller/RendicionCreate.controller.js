sap.ui.define([
	"jquery.sap.global",
	"sap/m/MessageToast",
	"sap/ui/core/Fragment",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	'sap/ui/core/library',
	"sap/ui/model/json/JSONModel"
	
], function(jQuery, MessageToast, Fragment, Controller, Filter, coreLibrary, JSONModel) {
	"use strict";
	
	var ValueState = coreLibrary.ValueState;

	var RendicionCreateController = Controller.extend("SeidorB1UI5.controller.RendicionCreate", {
		
		mensaje: null, noTable: null, noItems:null, noJSONModel: null,
		codigo: null, descripcion: null, fecha1: null, fecha2: null,
		
		oModelSel: null, oSelectD2: null, oSelectD3: null, id: null,
		codSocio: null, montoEntregado: null, saldoPendiente: null,
		nroPago: null, fechaPagoEntregado: null, bExiste: false, saldo: null,
		oRendicion1: {value:[]}, oRendicion2: {value:[]}, oRendicion3: {value:[]},
		
		onInit: function() {
			if(JSON.parse(sessionStorage.getItem("tiles")) === null){
				delCookies();
    			window.location.replace("");
			}
			
			if(oDatosUsuario === undefined || !oDatosUsuario)
				oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
			
			var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
			this.byId("avatar").setInitials(iAvatar);
			this.byId("avatar").setTooltip("Usuario: " + avatar);
			
			if(!oRendicionJSONData || oRendicionJSONData === undefined || oRendicionJSONData.value.length == 0){
				sap.ui.core.BusyIndicator.show(0);				
				//carga los valores del sessionStorage en variable
				getRendiciones();
				oRendicionJSONData = JSON.parse(sessionStorage.getItem("rendiciones"));
				if(!oRendicionJSONData.tipoFinanciamientos || oRendicionJSONData.tipoFinanciamientos === undefined)
					this.cargaCombobox(null,"tipoFinanciamientos");
               
				if(!oRendicionJSONData.departamentos || oRendicionJSONData.departamentos === undefined)
					this.cargaCombobox(null,"departamentos");
				
				if(!oRendicionJSONData.tipoDocumentos || oRendicionJSONData.tipoDocumentos === undefined)
					this.cargaCombobox(null,"tipoDocumentos");
				
				if(!oRendicionJSONData.rutFuncionarios || oRendicionJSONData.rutFuncionarios === undefined)
					this.cargaCombobox(null,"rutFuncionarios");
				
				if(!oRendicionJSONData.rutProveedores || oRendicionJSONData.rutProveedores === undefined)
					this.cargaCombobox(null,"rutProveedores");
				
				if(!oRendicionJSONData.tipoRendicion || oRendicionJSONData.tipoRendicion === undefined)
					this.cargaCombobox(null,"tipoRendicion");
				
				if(!oRendicionJSONData.saldoAsignado || oRendicionJSONData.saldoAsignado === undefined)
					this.cargaCombobox(null,"saldoAsignado");
				
				if(!oRendicionJSONData.ubicaciones || oRendicionJSONData.ubicaciones === undefined)
					this.cargaCombobox(null,"location");
			}
			
			this.cargarCabecera();
			
			var fecha = this.getFechaActual();
			this.byId("cbxFechaRendicion").setValue(fecha);
			
			var oModel = new JSONModel();
			oModel.setData(oRendicionJSONData);
			oModel.setSizeLimit(2000);
			this.oJSONModel = oModel;
			
			this.getView().setModel(this.oJSONModel);
			var rows = this.getView().byId("idItemsTable").getRows().length;
			this.getView().byId("idItemsTable").setVisibleRowCount(rows);
		},
		
		getFechaActual: function(){
			var date = new Date();
			var fechaActual = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
			return fechaActual;
		},
		
		handleLogout: function (evt) {
			sessionStorage.clear();
			delCookies();
			window.location.replace("");
		},

		onExit: function() {
			if (this._oDialog) {
				this._oDialog.destroy();
			}
		},
		
		cargarCabecera: function(){
			//this.handleCancelBtn();
			this.limpiarCabecera();
			this.byId("txtColegio").setText(oDatosUsuario.des_suc);
			this.byId('cbxTipoFinanciamiento').setValue(oDatosUsuario.des_fin).setSelectedKey(oDatosUsuario.cod_fin);
			this.byId("txtPagFuncionario").setValue("");
		},
		
		handleNavButton: function(oEvent) {
			var sPreviousHash = sap.ui.core.routing.History.getInstance().getPreviousHash();
			this.limpiarCabecera();
			//The history contains a previous entry
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				// There is no history!	// replace the current hash with page 1 (will not add an history entry)
				this.getOwnerComponent().getRouter().navTo("RendicionMaster", oPurchaseJSONData, true);
			}
		},

		handleChangeCb: function(oEvent){
			var oValidatedComboBox = oEvent.getSource(),
			sSelectedKey = oValidatedComboBox.getSelectedKey(),
			sValue = oValidatedComboBox.getValue();

			if (!sSelectedKey && sValue) {
				oValidatedComboBox.setValueState(ValueState.Error);
				oValidatedComboBox.setValueStateText("Por favor ingresa valor valido!");
			} else {
				oValidatedComboBox.setValueState(ValueState.None);
			}
		},
		
		handleChange: function(oEvent) {
			this.fecha1 = oEvent.getParameter("value");
		},
		
		handleAceptBtn: function(oEvent) {
			if(!this.bExiste){
				sap.ui.core.BusyIndicator.show(0);
				
				this.mensaje = null;
				this.handleCabecera();
				this.handleFecha();
				this.handleTabla();
				if(this.mensaje === null) {
					var rendicion = {SEI_RENDETCollection:[]};
					var rendicionItems = {};
					var items = this.getView().byId("idItemsTable").getRows();
									
					rendicion.U_SEI_Unidad 		= this.byId("txtColegio").getText();
					rendicion.U_SEI_NFUNCIO 	= this.byId("inpNomFunc").getValue();
					rendicion.U_SEI_FECHAREN 	= this.byId("cbxFechaRendicion").getValue();
					rendicion.U_SEI_TRENDI		= this.byId("cbxTipoRendicion").getSelectedKey();
					rendicion.U_SEI_CFUNCIO 	= this.byId("txtCodFuncionario").getText();				
					rendicion.U_SEI_NROPAGO 	= this.byId("cbxSaldoPendiente").getSelectedKey();
					rendicion.U_SEI_TIPOFI		= this.byId("cbxTipoFinanciamiento").getValue();
					rendicion.U_SEI_DPTO		= this.byId("cbxDepartamento").getSelectedKey();
					rendicion.U_SEI_UCREADOR	= oDatosUsuario.user_name;
					
					var sumaMonto = 0;
					for(var i = 0; i < items.length; i++){
						var rut = "", nomp = "", factaso = "";
						var selText = items[i].getCells()[2].getSelectedKey();
						
						if(selText === "Bolh" || selText === "Bolhe" || selText === "Fac" || selText === "Facel" || selText === "Facex" || selText === "Face" || selText === "Notacre" || selText === "Notadeb"){
							rut		= items[i].getCells()[3].getValue();
							nomp	= items[i].getCells()[4].getValue();
							if(selText === "Notacre" || selText === "Notadeb")
								factaso	= items[i].getCells()[5].getValue();
						}
						rendicionItems = {
								U_SEI_NRODOC:		items[i].getCells()[0].getValue(),
								U_SEI_FECHADOC: 	items[i].getCells()[1].getValue(),
								U_SEI_TIPODOC:		items[i].getCells()[2].getValue(),
								U_SEI_SUPERIN:		items[i].getCells()[2].getSelectedKey(),
								U_SEI_RUT:			rut,
								U_SEI_NOMP:			nomp,
								U_SEI_FACTASO:		factaso,
								U_SEI_MONTO:		items[i].getCells()[6].getValue(),
								U_SEI_DESCRIP:		items[i].getCells()[7].getValue(),
								U_SEI_CTASUPERE:	items[i].getCells()[8].getText(),
								U_SEI_CTAR:			items[i].getCells()[9].getText()
						}
						rendicion.SEI_RENDETCollection.push(rendicionItems);
						sumaMonto = sumaMonto + parseInt(items[i].getCells()[6].getValue());
					}
					
					var porcentaje = porcentajeUsado(this.montoEntregado, this.saldoPendiente, sumaMonto);
					var saldo = this.saldoPendiente - sumaMonto;
					this.updateSaldoEntregado(false);
					
					if(porcentaje <= 50){
						new sap.m.MessageBox.show("Ha usado Mas del 50%. Te queda aun el " + porcentaje + "%. Por un Monto de " + saldo, {
							icon: sap.m.MessageBox.Icon.WARNING,
							title: "Informacion Creacion Rendiciones"
						});
					}
					
					var res = setRendiciones(rendicion);
					sap.ui.core.BusyIndicator.hide();
					if(res > 0){
						this.onDelete(items);
						this.limpiarCabecera();
					}
				} else {
					sap.m.MessageBox.show(this.mensaje, {
						icon: sap.m.MessageBox.Icon.ERROR,
						title: "Informacion",
						onClose: function(oAction) {					
				    		sap.ui.core.BusyIndicator.hide();
						}
					});
				}
			} else {
				new sap.m.MessageBox.show("El numero de documento ya se encuentra en la grilla. se debe modificar y/o eliminar para continuar.", {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Informacion Creacion Rendiciones",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					} 
				});
			}
		},
		
		// A continuación se muestra la definición de la función para llamar a la capa de servicio //
		// para cancelar un pedido de cliente abierto //
		handleCancelBtn: function(oEvent) {
			var items = this.getView().byId("idItemsTable").getRows();
			this.onDelete(items);
			
			this.updateSaldoEntregado(true);
			//this.getView().byId("txtPagFuncionario").setValue("$" + $.number(this.saldo, 0, ',', '.' ));
			
			var rows = this.getView().byId("idItemsTable").getModel().getData().newItems.length;
			this.getView().byId("idItemsTable").setVisibleRowCount(rows);
			this.limpiarCabecera();
		},
		
		handleEliminBtn: function(oEvent) {
			var oTable = this.getView().byId("idItemsTable");
		    var data = oTable.getModel();
		    var rowCount = oTable.getRows().length;
		    var selRowCount = oTable.getSelectedIndex();
		    //var selRowCount = oTable._oSelection.aSelectedIndices.length;
			if(selRowCount >= 0){
		    	for (var i = 0; i < rowCount; i++) {
			    	oTable.getSelectedIndex();
			        var rowNum = oTable.getSelectedIndex();
					var oData = data.getData();
					oData.newItems.splice(rowNum, 1);
					data.setData(oData);
					data.refresh();
			    }
			} else {
				new sap.m.MessageBox.show("No se ha seleccionado lineas a eliminar", {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: "Informacion Creacion Rendiciones.",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		    var rows = data.getData().newItems.length;
		    this.updateSaldoEntregado(true);
			this.getView().byId("idItemsTable").setVisibleRowCount(rows);
		},
		
		onDelete: function(items){
			var  oModel = this.getView().byId("idItemsTable").getModel();
			var oData = oModel.getData();
			
			for(var i = 0; i < items.length; i++){			
				oData.newItems.splice(0, 1);
			}
			oModel.setData(oData);
			oModel.refresh();
		},
		
		cargaSaldoFuncionario: function(selKey, selText) {
			var cbxSald = this.byId("cbxSaldoPendiente"), inpPago = this.byId("txtPagFuncionario"),  newItem;
			cbxSald.removeAllItems();
			cbxSald.setValue("").setSelectedKey("");
			inpPago.setValue("");
			
			if(!oRendicionJSONData.saldoAsignado || oRendicionJSONData.saldoAsignado === undefined)
				this.cargaCombobox(null,"saldoAsignado");
	
			for(var i = 0; i < oRendicionJSONData.saldoAsignado.length; i++){
				var saldoAsignado = oRendicionJSONData.saldoAsignado[i];
				
				if(selKey === saldoAsignado.CodSocio){
					var difFecha = diffFecha(new Date(saldoAsignado.FechaPagoEntregado), new Date());
					
					if(difFecha >= 0 && difFecha <= 30){							
						newItem = new sap.ui.core.Item({key: saldoAsignado.NroPago, text: saldoAsignado.NroPago + " - " + "$" + $.number(saldoAsignado.MontoEntregado, 0, ',', '.' ) + " " + saldoAsignado.Comment});
						cbxSald.addItem(newItem);
					}
				}
			}
			
			if(cbxSald.getItems().length > 0){inpPago
				cbxSald.setValue("Monto Entregado");
				inpPago.setValue("Saldo Pendiente");
				this.fecha1 = null;
			} else if(!this.montoEntregado){
				new sap.m.MessageBox.show(
						"No Existen Montos Asignados Para el Usuario " + selText, {
	               	icon: sap.m.MessageBox.Icon.WARNING,
	              	title: "Informacion Creacion Rendiciones.",
	              	onClose: function(oAction){
	              		cbxSald.setValue("").setSelectedKey("");
	              		inpPago.setValue("");
	              		this.fecha1 = null;
	              	}
	            });
			}
		},
		
		handleRutProveedor: function(oControlEvent) {
			var oValidaControl = this.validaComboBox(oControlEvent);
			
			if(oValidaControl.getValueState() === "None"){
				var selKey = oControlEvent.getParameter("selectedItem").getKey();
				var selText = oControlEvent.getParameter("selectedItem").getText();
				
				var cadena = oControlEvent.getSource().getBindingContext().sPath;
				var largo = cadena.length;
				cadena = cadena.substring(largo, largo-2);
				var linea = cadena.replace("/", "");
				
				var items = this.getView().byId("idItemsTable").getRows();
				items[linea].getCells()[4].setValue(selKey);
			}
			this.validaNroDoc(true)
		},
		
		handleNomProveedor: function(oControlEvent) {
			var oValidaControl = this.validaComboBox(oControlEvent);
			
			if(oValidaControl.getValueState() === "None"){
				var selKey = oControlEvent.getParameter("selectedItem").getKey();
				var selText = oControlEvent.getParameter("selectedItem").getText();
				
				var cadena = oControlEvent.getSource().getBindingContext().sPath;
				var largo = cadena.length;
				cadena = cadena.substring(largo, largo-2);
				var linea = cadena.replace("/", "");
				
				var items = this.getView().byId("idItemsTable").getRows();				
				items[linea].getCells()[3].setValue(selKey);
			}
		},
		
		handleRendicion: function(oControlEvent){
			var items = this.getView().byId("idItemsTable").getRows();
			
			if(items.length === 0){
				var oValidaControl = this.validaComboBox(oControlEvent);
				
				if(oValidaControl.getValueState() === "None"){
					var selKey = oControlEvent.getParameter("selectedItem").getKey();
					var selText = oControlEvent.getParameter("selectedItem").getText();
				}
			} else {
				var key  = this.selKeyTRen;
				var text = this.selTextTRen;
				var tipo = this.byId("cbxTipoRendicion");
				
				new sap.m.MessageBox.show(
						"Debes Guardar la rendicion Creada Antes de Cambiar el Tipo de Rendicion", {
	               	icon: sap.m.MessageBox.Icon.WARNING,
	              	title: "Informacion Creacion Rendiciones.",
	              	onClose: function(oAction){
	              		tipo.setSelectedKey(key);
	              		tipo.setValue(text);
	              	}
	            });
			}
		},
		
		handleFinanciamiento: function(oControlEvent){
			var items = this.getView().byId("idItemsTable").getRows();
			
			if(items.length === 0){
				var oValidaControl = this.validaComboBox(oControlEvent);
				
				if(oValidaControl.getValueState() === "None"){
					var selKey = oControlEvent.getParameter("selectedItem").getKey();
					var selText = oControlEvent.getParameter("selectedItem").getText();
				}
			} else {
				var key  = this.selKeyTRen;
				var text = this.selTextTRen;
				var tipo = this.byId("cbxTipoRendicion");
				
				new sap.m.MessageBox.show(
						"Debes Guardar la rendicion Creada Antes de Cambiar el Tipo de Financiamiento", {
	               	icon: sap.m.MessageBox.Icon.WARNING,
	              	title: "Informacion Creacion Rendiciones.",
	              	onClose: function(oAction){
	              		tipo.setSelectedKey(key);
	              		tipo.setValue(text);
	              	}
	            });
			}
		},
		
		handleTipoDescripcion: function(oControlEvent) {
//			if(!this.oRendicion1.value || !this.oRendicion2.value || this.oRendicion3.value.length <= 0)
				this.cargaCombobox(null,"tipoRendicion");
			
//			var oValidaControl = this.validaComboBox(oControlEvent);
//			if(oValidaControl.getValueState() === "None"){
//				this.selKeyTRen = oControlEvent.getParameter("selectedItem").getKey();
//				this.selTextTRen = oControlEvent.getParameter("selectedItem").getText();

//				var oView = oControlEvent.getSource();
//				var oData = oView.getModel().getData();
				
//				if(this.selKeyTRen === "1"){//tipo de rendicion && tipo de financiamiento
//					oData.tipoRendicion = this.oRendicion1.value;
//				} else if (this.selKeyTRen === "2"){//tipo de rendicion && tipo de financiamiento
//					oData.tipoRendicion = this.oRendicion2.value;
//				} else if (this.selKeyTRen === "2"){//tipo de rendicion && tipo de financiamiento
//					oData.tipoRendicion = this.oRendicion3.value;
//				}
			var items = this.getView().byId("idItemsTable").getRows();
			if(items.length === 0){
				//var financia = this.byId("cbxTipoFinanciamiento").getSelectedKey();
				var financia = this.byId("cbxTipoRendicion").getSelectedKey();
				
				var oView = this.getView();
				var oData = oView.getModel().getData();
					
				oData.tipoRendicion = (financia === "1") ? this.oRendicion1.value : this.oRendicion2.value;
				
//				if(financia === "D2001"){//SEP
//					oData.tipoRendicion = this.oRendicion1.value;
//				} else if(financia === "D2002"){//PIE
//					oData.tipoRendicion = this.oRendicion2.value;
//				} else if(financia === "D2003" || financia === "D2004"){//Generales, Apoderados
//					oData.tipoRendicion = this.oRendicion3.value;
//				}
				
				oView.getModel().setData(oData);
				//oView.getmodel().refresh();
//			}
			}
		},
		
		handleDescripcion: function(oControlEvent){			
			var oValidaControl = this.validaComboBox(oControlEvent);
			
			if(oValidaControl.getValueState() === "None"){
				var selKey = oControlEvent.getParameter("selectedItem").getKey();
				var selText = oControlEvent.getParameter("selectedItem").getText();
				
				var cadena = oControlEvent.getSource().getBindingContext().sPath;
				var largo = cadena.length;
				cadena = cadena.substring(largo, largo-2);
				var linea = cadena.replace("/", "");
				
				var items = this.getView().byId("idItemsTable").getRows();
//				items[linea].getCells()[8].setText(selKey);
				
				var cuentas = JSON.parse(sessionStorage.getItem('tipoRendicion'));
				for(var i = 0; i < cuentas.value.length; i++){
					var cuenta = cuentas.value[i];
					
					if(cuenta.Name === selText){
						items[linea].getCells()[8].setText(cuenta.U_SEI_CSUPERE);
						items[linea].getCells()[9].setText(cuenta.U_SEI_CUENTAR);
						break;
					}
				}
			}
		},
		
		validaComboBox: function(oEvent){
			var oValidatedComboBox = oEvent.getSource(),
		    sSelectedKey = oValidatedComboBox.getSelectedKey(),
		    sValue = oValidatedComboBox.getValue();
		
			if(!sSelectedKey && sValue) {
				oValidatedComboBox.setValueState("Error");
				oValidatedComboBox.setValueStateText("Porfavor Ingresa un valor Valido!");
			} else {
				oValidatedComboBox.setValueState("None");
			}
			return oValidatedComboBox;
		},
		
		handleTableCrea: function(oEvent) {
			if(!this.bExiste){
				this.mensaje = null;
				// Aca validar si los campos de la tabla estan completos sino mandar mensaje indicandolo
				this.handleCabecera();
				this.handleFecha();
				
				if(this.mensaje === null && !this.mensaje){
					//Tomo los item de la tabla si es que lo tiene
					var items = this.getView().byId("idItemsTable").getRows();
			    	
					var date = new Date();
					var fechaActual = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();			
					this.fecha2 = (this.fecha1 !== null ? this.fecha1 : fechaActual);
					
					var sumaMonto = 0;
					this.handleTipoDescripcion();//FILTRA EL COMBOBOX DOCUMENTO SEGUN TIPO DE FINANCIEMIENTO SELECCIONADO
					
					//Actualiza la cantidad en el jsondata
					for(var i = 0; i < items.length; i++){				
						for(var x = 0; x < this.oItems.newItem.length; x++ ){
							var selText = items[x].getCells()[2].getSelectedKey();
							
							this.oItems.newItem[x].U_SEI_NRODOC 	= items[x].getCells()[0].getValue();
							this.oItems.newItem[x].U_SEI_FECHADOC 	= items[x].getCells()[1].getValue();
							this.oItems.newItem[x].U_SEI_TIPODOC 	= items[x].getCells()[2].getValue();
							this.oItems.newItem[x].U_SEI_CTASUPERE	= items[i].getCells()[2].getSelectedKey();
							if(selText === "Bolh" || selText === "Bolhe" || selText === "Fac" || selText === "Facel" || selText === "Facex" || selText === "Face" || selText === "Notacre" || selText === "Notadeb"){
								this.oItems.newItem[x].U_SEI_RUT 		= items[x].getCells()[3].getValue();
								this.oItems.newItem[x].U_SEI_NOMP 		= items[x].getCells()[4].getValue();
								if(selText === "Notacre" || selText === "Notadeb")
									this.oItems.newItem[x].U_SEI_FACTASO 	= items[x].getCells()[5].getValue();
							} else {
								this.oItems.newItem[x].U_SEI_RUT 		= "";
								this.oItems.newItem[x].U_SEI_NOMP 		= "";
								this.oItems.newItem[x].U_SEI_FACTASO 	= "";
							}
							this.oItems.newItem[x].U_SEI_MONTO 		= items[x].getCells()[6].getValue();
							this.oItems.newItem[x].U_SEI_DESCRIP 	= items[x].getCells()[7].getValue();
							this.oItems.newItem[x].U_SEI_CTASUPERE 	= items[x].getCells()[8].getText();
							this.oItems.newItem[x].U_SEI_RESULT		= items[x].getCells()[9].getText();
						}
						sumaMonto = sumaMonto + parseInt(items[i].getCells()[6].getValue());
					}
					
					var porcentaje = porcentajeUsado(this.montoEntregado, this.saldoPendiente, sumaMonto);				
					var saldo = this.saldoPendiente - sumaMonto;
					
					this.updateSaldoEntregado(false);
					
					if(porcentaje <= 50){
						new sap.m.MessageBox.show("Ha usado Mas del 50%. Te queda aun el " + porcentaje + "%. Por un Monto de " + saldo, {
							icon: sap.m.MessageBox.Icon.WARNING,
							title: "Informacion Creacion Rendiciones"
						});
					}
					
					if(items.length === 0){
						this.oItems = {newItem: [{ "U_SEI_NRODOC": "", "U_SEI_FECHADOC": "", "U_SEI_RUT": "", "U_SEI_NOMP": "", "U_SEI_FACTASO": "", "U_SEI_TIPODOC": "", "U_SEI_CTASUPERE": "", "U_SEI_MONTO": "", "U_SEI_DESCRIP": "", "U_SEI_CTASUPERE": "", "U_SEI_CTAR": "" }]};
						this.getView().byId("idItemsTable").setVisibleRowCount(items.length + 1);
					} else if(items.length > 0){
						var i = items.length - 1;
						var selText = items[i].getCells()[2].getSelectedKey();
						
						if(selText === "Bolh" || selText === "Bolhe" || selText === "Fac" || selText === "Facel" || selText === "Facex" || selText === "Face" || selText === "Notacre" || selText === "Notadeb"){
							if(selText === "Notacre" || selText === "Notadeb"){
								if(!items[i].getCells()[0].getValue() || !items[i].getCells()[1].getValue() || !items[i].getCells()[2].getValue() || !items[i].getCells()[3].getValue() || 
										!items[i].getCells()[4].getValue() || !items[i].getCells()[5].getValue() || !items[i].getCells()[6].getValue() || !items[i].getCells()[7].getValue()){
									new sap.m.MessageBox.show("Debes Llenar la linea anterior antes de agregar una nueva ubicacion", {
										icon: sap.m.MessageBox.Icon.ERROR,
										title: "Informacion Creacion Rendiciones",
										onClose: function(oAction) {					
								    		sap.ui.core.BusyIndicator.hide();
										} 
									});
								} else {
									this.oItems.newItem.push({ "U_SEI_NRODOC": "", "U_SEI_FECHADOC": "", "U_SEI_RUT": "", "U_SEI_NOMP": "", "U_SEI_FACTASO": "", "U_SEI_TIPODOC": "", "U_SEI_CTASUPERE": "", "U_SEI_MONTO": "", "U_SEI_DESCRIP": "", "U_SEI_CTASUPERE": "", "U_SEI_CTAR": "" });
									this.getView().byId("idItemsTable").setVisibleRowCount(items.length + 1);
								}
								
							}else{
								if(!items[i].getCells()[0].getValue() || !items[i].getCells()[1].getValue() || !items[i].getCells()[2].getValue() || !items[i].getCells()[3].getValue() || 
										!items[i].getCells()[4].getValue() || !items[i].getCells()[6].getValue() || !items[i].getCells()[7].getValue()){
									new sap.m.MessageBox.show("Debes Llenar la linea anterior antes de agregar una nueva ubicacion", {
										icon: sap.m.MessageBox.Icon.ERROR,
										title: "Informacion Creacion Rendiciones",
										onClose: function(oAction) {					
								    		sap.ui.core.BusyIndicator.hide();
										} 
									});
								} else {
									this.oItems.newItem.push({ "U_SEI_NRODOC": "", "U_SEI_FECHADOC": "", "U_SEI_RUT": "", "U_SEI_NOMP": "", "U_SEI_FACTASO": "", "U_SEI_TIPODOC": "", "U_SEI_CTASUPERE": "", "U_SEI_MONTO": "", "U_SEI_DESCRIP": "", "U_SEI_CTASUPERE": "", "U_SEI_CTAR": "" });
									this.getView().byId("idItemsTable").setVisibleRowCount(items.length + 1);
								}
								
								
							}
						}else if(!items[i].getCells()[0].getValue() || !items[i].getCells()[1].getValue() || !items[i].getCells()[2].getValue() ||
									!items[i].getCells()[6].getValue() || !items[i].getCells()[7].getValue()){
							new sap.m.MessageBox.show("Debes Llenar la linea anterior antes de agregar una nueva ubicacion", {
								icon: sap.m.MessageBox.Icon.ERROR,
								title: "Informacion Creacion Rendiciones",
								onClose: function(oAction) {					
						    		sap.ui.core.BusyIndicator.hide();
								} 
							});
						} else {
							this.oItems.newItem.push({ "U_SEI_NRODOC": "", "U_SEI_FECHADOC": "", "U_SEI_RUT": "", "U_SEI_NOMP": "", "U_SEI_FACTASO": "", "U_SEI_TIPODOC": "", "U_SEI_CTASUPERE": "", "U_SEI_MONTO": "", "U_SEI_DESCRIP": "", "U_SEI_CTASUPERE": "", "U_SEI_CTAR": "" });
							this.getView().byId("idItemsTable").setVisibleRowCount(items.length + 1);
						}
					}
					
					//Actualiza el oModel para posterior carga
					//var oView = oEvent.getSource();
					var oView = this.getView();
					var oData = oView.getModel().getData();
					oData.newItems = this.oItems.newItem;
					oView.getModel().setData(oData);
					
					//Recarga la lista con nuevos productos
					this.oTable = this.getView().byId("idItemsTable");
	//				this.oColumn = this.getView().byId("colListImp");
					this.oTable.setModel(this.oJSONModel);
					this.oTable.bindRows("/newItems");
		
			    	this.getView().setModel(this.oJSONModel);		    	
				}else {
					new sap.m.MessageBox.show(this.mensaje, {
						icon: sap.m.MessageBox.Icon.ERROR,
						title: "Informacion Creacion Rendiciones",
						onClose: function(oAction) {					
				    		sap.ui.core.BusyIndicator.hide();
						} 
					});
				}
			} else {
				new sap.m.MessageBox.show("El numero de documento ya se encuentra en la grilla. se debe modificar y/o eliminar para continuar.", {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Informacion Creacion Rendiciones",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					} 
				});
			}
		},
		
		handleLoadItems: function(oEvent){
			var selText = oEvent.getParameter("selectedItem").getKey();
			var selKey = oEvent.getParameter("selectedItem").getText();
			
			if(selText === "Bolh" || selText === "Bolhe" || selText === "Fac" || selText === "Facel" || selText === "Facex" || selText === "Face" || selText === "Notacre" || selText === "Notadeb"){
				this.habilitaLineas(true);
			} else {
				this.validaNroDoc(false);
				this.habilitaLineas(false);
			}
		},
		
		validaNroDoc: function(bRut){
			var items = this.getView().byId("idItemsTable").getRows();
			var intPos = items.length - 1; 

			if(intPos > 0){
				var strNro = items[intPos].getCells()[0].getValue();
				var strTDoc = items[intPos].getCells()[2].getValue();
				var strRut = items[intPos].getCells()[3].getValue();
				
				for(var i = 0; i < intPos; i++){
					var strINro = items[i].getCells()[0].getValue();//getText()
					var strITDoc = items[i].getCells()[2].getValue();
					var strIRut = items[i].getCells()[3].getValue();
					
					if(bRut){
						this.validaNroDocSAP();
						if(strNro === strINro && strTDoc === strITDoc && strRut === strIRut){
							new sap.m.MessageBox.show("El numero de documento ya se encuentra en la grilla", {
								icon: sap.m.MessageBox.Icon.ERROR,
								title: "Informacion Creacion Rendiciones",
								onClose: function(oAction) {					
						    		sap.ui.core.BusyIndicator.hide();
								} 
							});
							this.bExiste = true;
							break;
						} else { this.bExiste = false; }
					} else {
						this.validaNroDocSAP();
						if(strNro === strINro && strTDoc === strITDoc){
							new sap.m.MessageBox.show("El numero de documento ya se encuentra en la grilla", {
								icon: sap.m.MessageBox.Icon.ERROR,
								title: "Informacion Creacion Rendiciones",
								onClose: function(oAction) {					
						    		sap.ui.core.BusyIndicator.hide();
								} 
							});
							this.bExiste = true;
							break;
						} else { this.bExiste = false; }
					}
				}
			} else { this.validaNroDocSAP(); }
		},
		
		validaNroDocSAP: function(){
			var items = this.getView().byId("idItemsTable").getRows();
			var intPos = items.length - 1;

			//TODO: poner un for recorriendo los listados de los documentos para validar duplicidad
			
			for(var index = 0; index <= oRendicionJSONData.value.length -1; index++){
				var Document = oRendicionJSONData.value[index];
				for(var cont = 0; cont <= Document.SEI_RENDETCollection.length -1; cont++){
					var Detalle = Document.SEI_RENDETCollection[cont];
					
					var DocNum =Document.DocNum;
					var strNro = Detalle.U_SEI_NRODOC;
					var strTDoc = Detalle.U_SEI_TIPODOC;
					var strRut = Detalle.U_SEI_RUT;
					
					for(var i = 0; i <= intPos; i++){
						var strINro = items[i].getCells()[0].getValue();//getText()
						var strITDoc = items[i].getCells()[2].getValue();
						var strIRut = items[i].getCells()[3].getValue();
						strIRut = (strIRut === "Selec.Rut Proveedor") ? "" : strIRut;
						
						if(strNro === strINro && strTDoc === strITDoc && strRut === strIRut){
							new sap.m.MessageBox.show("El numero de documento ya se encuentra en SAP", {
								icon: sap.m.MessageBox.Icon.ERROR,
								title: "Informacion Creacion Rendiciones",
								onClose: function(oAction) {					
						    		sap.ui.core.BusyIndicator.hide();
								} 
							});
							this.bExiste = true;
							break;
						} else { this.bExiste = false; }
					}
					if(this.bExiste)
						break;
				}
				if(this.bExiste)
					break;
			}
		},
		
		validaCorreccionNroDoc: function(oEvent){
			if(this.bExiste){
				var items = this.getView().byId("idItemsTable").getRows();
				var intPos = items.length - 1; 

				var strIRut = items[intPos].getCells()[3].getValue();
				strIRut = (strIRut === "Selec.Rut Proveedor") ? "" : strIRut;
				if(strIRut === ""){
					this.validaNroDoc(false);
				} else { 
					this.validaNroDoc(true);
				}
				
			}
		},
		
		getSaldoEntregado: function(intSaldoPendiente){
			var items = this.getView().byId("idItemsTable").getRows();			
			if(items.length === 0){
				this.saldo = intSaldoPendiente;
			} else {
				new sap.m.MessageBox.show(
						"Debes Guardar la rendicion Creada Antes de Cambiar el Tipo de Rendicion", {
	               	icon: sap.m.MessageBox.Icon.WARNING,
	              	title: "Informacion Creacion Rendiciones."	              	
	            });
			}
		},
		
		updateSaldoEntregado: function(blElimina){
			this.saldo = this.saldoPendiente;
			var items = this.getView().byId("idItemsTable").getRows();
			var intPos = items.length - 1;
			var intSaldo = 0;
			var intAcu = 0;
			
			for(var i = 0; i <= intPos; i++){
				var intMonto = 0;
				var strTipoDOc = items[i].getCells()[2].getSelectedKey();
				intMonto = (items[i].getCells()[6].getValue() === "") ? 0 : items[i].getCells()[6].getValue();
				
				if(strTipoDOc === "Notacre" || strTipoDOc === "Notadeb"){
					intAcu = intAcu - parseInt(intMonto);
				} else {
					intAcu = intAcu + parseInt(intMonto);
				}
			}
			intSaldo = this.saldo - intAcu;
			
			var porcentaje = porcentajeUsado(this.montoEntregado, intSaldo, intAcu);
			
			if(porcentaje <= 50){
				new sap.m.MessageBox.show("Ha usado Mas del 50%. Te queda aun el " + porcentaje + "%. Por un Monto de " + intSaldo, {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: "Informacion Creacion Rendiciones"
				});
			}
			this.getView().byId("txtPagFuncionario").setValue("$" + $.number(intSaldo, 0, ',', '.' ));
			this.saldo = intSaldo;
		},
		
		handleCrearObj: function(oEvent) {
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([]);

			var aContexts = oEvent.getParameter("selectedContexts");
			if (aContexts && aContexts.length) {
				aContexts.map(function(oContext) { 
					var codigo = oContext.getObject().ItemCode;
					var nombre = oContext.getObject().ItemName;
				});
			}
		},
		
		cargaCombobox: function(oControlEvent, idCbx) {
			if(idCbx === null)
				idCbx = "cbxFuncionario";
			
			switch(idCbx){
			case "tipoFinanciamientos":
				getAsync('tipoFinanciamientos');
				oRendicionJSONData.tipoFinanciamientos = JSON.parse(sessionStorage.getItem('tipoFinanciamientos')).value;
				break;
			case "rutFuncionarios":
				getAsync('rutFuncionarios');
				oRendicionJSONData.rutFuncionarios = JSON.parse(sessionStorage.getItem('rutFuncionarios')).value;
				break;
			case"departamentos":
				getAsync('departamentos');
				oRendicionJSONData.departamentos = JSON.parse(sessionStorage.getItem('departamentos')).value;
				break;
			case"tipoDocumentos":
				getAsync('tipoDocumentos');
				oRendicionJSONData.tipoDocumentos = JSON.parse(sessionStorage.getItem('tipoDocumentos')).value;
				break;
			case"rutProveedores":
				getAsync('rutProveedores');
				oRendicionJSONData.rutProveedores = JSON.parse(sessionStorage.getItem('rutProveedores')).value;
				break;
			case"tipoRendicion":
				getAsync('tipoRendicion');
				oRendicionJSONData.tipoRendicion = JSON.parse(sessionStorage.getItem('tipoRendicion')).value;
				
				for(var i = 0; i < JSON.parse(sessionStorage.getItem('tipoRendicion')).value.length; i++){
					var oRendiciones = JSON.parse(sessionStorage.getItem("tipoRendicion")).value[i];
					
					if(oRendiciones.U_SEI_TRENDI === "1"){
						this.oRendicion1.value.push(oRendiciones);
					} else if(oRendiciones.U_SEI_TRENDI === "2"){
						this.oRendicion2.value.push(oRendiciones);
					}
					
//					if(oRendiciones.U_SEI_TSEP === "Si"){
//						this.oRendicion1.value.push(oRendiciones);
//					} else if(oRendiciones.U_SEP_TPIE === "Si") {
//						this.oRendicion2.value.push(oRendiciones);
//					}else if(oRendiciones.U_SEI_TFONDOSG === "Si"){
//						this.oRendicion3.value.push(oRendiciones);
//					}
				}
				break;
			case"saldoAsignado":
				getAsync('saldoAsignado');
				oRendicionJSONData.saldoAsignado = JSON.parse(sessionStorage.getItem('saldoAsignado')).value;
				break;
			case"location":
				getAsync('location');
				oRendicionJSONData.ubicaciones = JSON.parse(sessionStorage.getItem('location')).value;
				break;
			}
		},
		
		handlePago: function(oEvent){
			var selKey = oEvent.getParameter("selectedItem").getKey();
			var selText = oEvent.getParameter("selectedItem").getText();
			
			if(!oRendicionJSONData.saldoAsignado || oRendicionJSONData.saldoAsignado === undefined)
				this.cargaCombobox(null,"saldoAsignado");
			
			for(var i = 0; i < oRendicionJSONData.saldoAsignado.length; i++){
				var saldoAsignado = oRendicionJSONData.saldoAsignado[i];
				
				if(selKey == saldoAsignado.NroPago){
					this.id					= saldoAsignado.id__;
					this.nroPago			= saldoAsignado.NroPago;
					this.codSocio 			= saldoAsignado.CodSocio;
					this.montoEntregado 	= saldoAsignado.MontoEntregado;
					this.saldoPendiente 	= saldoAsignado.SaldoPendiente;
					this.fechaPagoEntregado = saldoAsignado.FechaPagoEntregado;
				}
			}//"$" + $.number(this.saldoPendiente, 0, ',', '.' )
			this.getSaldoEntregado(parseInt(this.saldoPendiente));//almacena resultado en this.saldo 
			this.getView().byId("txtPagFuncionario").setValue("$" + $.number(this.saldo, 0, ',', '.' ));
		},
		
		handleCabecera:function(){
			// Validacion Cabecera
			if(!this.getView().byId("inpNomFunc").getValue())
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No se a Seleccionado Funcionario." : "- No se a Seleccionado Funcionario.");
			
			if(!this.getView().byId("cbxDepartamento").getValue() || this.getView().byId("cbxDepartamento").getValue() === "")
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No se a Seleccionado Departamento." : "- No se a Seleccionado Departamento.");
			
			if(!this.getView().byId("cbxTipoRendicion").getValue() || this.getView().byId("cbxTipoRendicion").getValue() === "")
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No se a Seleccionado Tipo de Rendicion." : "- No se a Seleccionado Tipo de Rendicion.");
			
			if(!this.getView().byId("cbxSaldoPendiente").getValue() || this.getView().byId("cbxSaldoPendiente").getValue() === "")
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No se a Seleccionado Pago Funcionario." : "- No se a Seleccionado Pago Funcionario.");
			
			if(!this.getView().byId("cbxTipoFinanciamiento").getValue() || this.getView().byId("cbxTipoFinanciamiento").getValue() === "")
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No se a Seleccionado Financiamiento." : "- No se a Seleccionado Financiamiento.");
		},
		
		handleFecha: function(){
			var cbxFechaRendicion = this.getView().byId("cbxFechaRendicion").getValue();
			if(!cbxFechaRendicion){
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No se a Seleccionado Fecha de Rendicion." : "- No se a Seleccionado Fecha de Rendicion.");
			} else if(cbxFechaRendicion && this.fechaPagoEntregado){
				var difFecha = diffFecha(new Date(this.fechaPagoEntregado), cbxFechaRendicion);
				if(difFecha === "mayor"){
					this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - La Fecha Seleccionada supera los 30 dias para Rendir." : "- La Fecha Seleccionada supera los 30 dias para Rendir.");
				} else if(difFecha === "menor"){
					this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - La Fecha Seleccionada esta fuera de fecha para Rendir." : "- La Fecha Seleccionada esta fuera de fecha para Rendir.");
				}
			}
			
		},
		
		handleTabla: function() {
			// Validacion Tabla
			var items = this.getView().byId("idItemsTable").getRows();
			if(items.length === 0){
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n -No hay Articulos para crear una Solicitud." : "- No hay Articulos para crear una Solicitud.");
			} else {
				for(var item = 0; item < items.length; item++){
					var linea = item + 1;
					for(var cell = 0; cell < items[item].getCells().length-1; cell++){
						if(cell !== 8 && cell !== 9){
							if(!items[item].getCells()[cell].getValue()){
								switch(cell){
								case 1:
									this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Ingresar Numero de Documento. En la linea: " + linea : "- Debe Ingresar Numero de Documento. En la linea: " + linea);
									break;
								case 2:
									this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Seleccionar una Fecha. En la linea: " + linea : "- Debe Seleccionar una Fecha. En la liena: " + linea);
									break;
								case 3:
									this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Seleccionar el Tipo de Documento. En la linea: " + linea : "- Debe Seleccionar el Tipo de Documento. en la linea:" + linea);
									break;
								case 4:
									if(items[item].getCells()[cell].getEnabled())
										this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Seleccionar un Rut del Proveedor y/o Nombre del Proveedor. En la linea: " + linea : "- Debe Seleccionar un Rut del Proveedor y/o Nombre del Proveedor. En la Linea: " + linea);
									break;
								case 5:
									if(items[item].getCells()[cell].getEnabled())
										this.mensaje = (this.mensaje !== null ? this.mensaje + "\n  - Debe Seleccionar un Nombre del Proveedor y/o Rut del Proveedor. En la linea: " + linea : "- Debe Seleccionar un Nombre del Proveedor y/o Rut del Proveedor. En la Linea: " + linea);
									break;
								case 6:
									if(items[item].getCells()[cell].getEnabled())
										this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Ingresar la Factura Asociada. En la linea: " + linea : "- Debe Ingresar la Factura Asociada. En la linea: " + linea);
									break;
								case 7:
									this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Ingresar el Monto. En la linea: " + linea : "- Debe Ingresar el Monto. En la linea: " + linea);
									break;
								case 8:
									this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Seleccionar la Descripcion. En la linea: " + linea : "- Debe Seleccionar la Descripcion. En la linea: " + linea);
									break;
								}
								
							}
						}
					}
				}
			}
		},
		
		validaRut: function(){
			var items = this.getView().byId("idItemsTable").getRows();
			for(var i = 0; i < items.length; i++){
				var rutVal = items[i].getCells()[2].getValue();
				// Uso de la función
				alert(Fn.validaRut(rutVal) ? 'Valido' : 'inválido');
				
			}
		},
		
		habilitaLineas: function(estado){
			var items = this.getView().byId("idItemsTable").getRows();
			
			for(var item = 0; item < items.length; item++){
				var selText = items[item].getCells()[2].getSelectedKey();
				
				if(selText === "Bolh" || selText === "Bolhe" || selText === "Fac" || selText === "Facel" || selText === "Facex" || selText === "Face" || selText === "Notacre" || selText === "Notadeb"){
					items[item].getCells()[3].setEnabled(estado);
					items[item].getCells()[4].setEnabled(estado);
					if(selText === "Notacre" || selText === "Notadeb"){
						items[item].getCells()[5].setEnabled(estado);
					} else {
						items[item].getCells()[5].setEnabled(false);
					}
				} else {
					items[item].getCells()[3].setEnabled(false);
					items[item].getCells()[4].setEnabled(false);
					items[item].getCells()[5].setEnabled(false);
				}
			}
		},
		
		limpiarCabecera: function(){
			this.byId("txtCodFuncionario").setText("");	
			this.byId("inpNomFunc").setValue("");
			this.byId("cbxDepartamento").setValue("").setSelectedKey("");
			this.byId("cbxTipoRendicion").setValue("").setSelectedKey("");
			//this.byId("cbxFechaRendicion").setValue("");
			this.byId("cbxSaldoPendiente").removeAllItems();
			this.byId("cbxSaldoPendiente").setValue("").setSelectedKey("");
			this.byId("txtPagFuncionario").setValue("");
			this.byId('cbxTipoFinanciamiento').setValue(oDatosUsuario.des_fin).setSelectedKey(oDatosUsuario.cod_fin);
		},
		
		delay : function(componente) {
			return new Promise(resolve => {
			    setTimeout(() => {
			    	componente.setBusy(false);
			    }, 5000);
			 });
		},
		
		handleClose: function(oEvent) {
			oEvent.getSource().close();
			//oEvent.getSource().destroy();
		},
		
		loadCombobox: function(){
//			var oModel = new JSONModel();//			
//			oModel.setData(oItems);			
//			return oModel;
		},
		
		onValueFunc: function() {
			this._oInput = this.getView().byId("inpNomFunc");
			this.oColModel = new sap.ui.model.json.JSONModel();
			this.oColModel.setData({"cols": [{ "label": "Rut Funcionario", "template": "CardCode", "width": "10rem" }, { "label": "Nombre Funcionario", "template": "CardName" }]});
			
			var aCols = this.oColModel.getData().cols;
			this._oBasicSearchField = new sap.m.SearchField({showSearchButton: false});
			
			this._oValueHelpDialog = sap.ui.xmlfragment("SeidorB1UI5.view.fragment.Funcionario", this);
			this.getView().addDependent(this._oValueHelpDialog);

			this._oValueHelpDialog.getFilterBar().setBasicSearch(this._oBasicSearchField);
			
			this._oValueHelpDialog.getTableAsync().then(function (oTable) {
				oTable.setModel(this.oJSONModel);
				oTable.setModel(this.oColModel, "columns");

				if (oTable.bindRows) {
					oTable.bindAggregation("rows", "/rutFuncionarios");
				}

				if (oTable.bindItems) {
					oTable.bindAggregation("items", "/rutFuncionarios", function () {
						return new sap.m.ColumnListItem({
							cells: aCols.map(function (column) {
								return new sap.m.Label({ text: "{" + column.template + "}" });
							})
						});
					});
				}

				this._oValueHelpDialog.update();
			}.bind(this));

			var oToken = new sap.m.Token();
			oToken.setKey(this._oInput.getSelectedKey());
			oToken.setText(this._oInput.getValue());
			this._oValueHelpDialog.setTokens([oToken]);
			this._oValueHelpDialog.open();
		},

		onFilterBarFunc: function (oEvent) {
			var sSearchQuery = this._oBasicSearchField.getValue(),
				aSelectionSet = oEvent.getParameter("selectionSet");
			var aFilters = aSelectionSet.reduce(function (aResult, oControl) {
				if (oControl.getValue()) {
					aResult.push(new sap.ui.model.Filter({
						path: oControl.getName(),
						operator: sap.ui.model.FilterOperator.Contains,
						value1: oControl.getValue()
					}));
				}

				return aResult;
			}, []);

			aFilters.push(new sap.ui.model.Filter({
				filters: [
					new sap.ui.model.Filter({ path: "CardCode", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery }),
					new sap.ui.model.Filter({ path: "CardName", operator: sap.ui.model.FilterOperator.Contains, value1: sSearchQuery })
				],
				and: false
			}));

			this._filterTableFunc(new sap.ui.model.Filter({
				filters: aFilters,
				and: true
			}));
		},
		
		_filterTableFunc: function (oFilter) {
			var oValueHelpDialog = this._oValueHelpDialog;

			oValueHelpDialog.getTableAsync().then(function (oTable) {
				if (oTable.bindRows) {
					oTable.getBinding("rows").filter(oFilter);
				}

				if (oTable.bindItems) {
					oTable.getBinding("items").filter(oFilter);
				}

				oValueHelpDialog.update();
			});
		},
		
		/**
		 * Funcion de Evento Seleccion de Linea de la tabla. que carga los valores en los campos
		 * Cliente - Nombre y llama a los acuerdos globales asociados al socio de negocio seleccionado
		 * para llenar los datos en combobox Acuedo globales.
		 */
		onValueFuncOk: function (oEvent) {
			sap.ui.core.BusyIndicator.show(0);
			var aTokens = oEvent.getParameter("tokens");
			this._oInput.setValue(aTokens[0].getKey());
			var nom = aTokens[0].getText();
			nom = nom.replace(" ("+ aTokens[0].getKey() +")", "");
			
			this.byId("txtCodFuncionario").setText(aTokens[0].getKey());
			this._oInput.setValue(nom);
			
			this.cargaSaldoFuncionario(aTokens[0].getKey(), nom);
			
			sap.ui.core.BusyIndicator.hide();
			
			this._oValueHelpDialog.close();
		},

		/**
		 * Funcion de Evento Boton Cancelar. el que llama a la funcion para el cierre 
		 * de la ventana oValueHelpDialog.
		 */
		onValueFuncCancel: function () {
			this._oValueHelpDialog.close();
		},

		onValueFuncClose: function () {
			this._oValueHelpDialog.destroy();
		}
	});
	return;
});