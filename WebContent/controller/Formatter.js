sap.ui.define(function() {
	"use strict";

	var Formatter = {
		
		statusDes :  function (sStatus) {
			if (sStatus === "Aprobado") {
				return "Success";
			} else if (sStatus === "Pendiente") {
				return "Warning";
			} else if (sStatus === "Rechazado"){
				return "Error";
			} else {
				return "Error";
			}
		},
		
		estado :  function (sStatus) {
			if (sStatus === "0") {
				return "Success";
			} else if (sStatus === "1") {
				return "Error";
			} else if (sStatus === "2") {
				return "Warning";
			}
		},
		
		replaceEstado :  function (sStatus) {
			if (sStatus === "0") {
				return "Aprobado";
			} else if (sStatus === "1") {
				return "Rechazado";
			} else if (sStatus === "2") {
				return "Pendiente";
			}
		},
		
		replaceStatus :  function (sStatus) {
			if (sStatus === 1) {
				return "INGRESADA";
			} else if (sStatus === 2) {
				return "APROBADA";
			} else if (sStatus === 3) {
				return "EVALUACION";
			} else if (sStatus === 4) {
				return "EVALUACION CONSULTA";
			} else if (sStatus === 5) {
				return "COTIZANDO";
			} else if (sStatus === 6) {
				return "ADJUDICADA";
			} else if (sStatus === 7) {
				return "EJECUTANDO";
			} else if (sStatus === 8) {
				return "RECEPCIONADA";
			} else if (sStatus === 9) {
				return "RECHAZADA";
			} else if (sStatus === 10) {
				return "CERRADA";
			}
		},
		
		status :  function (sStatus) {
			if (sStatus === "C") {
				return "Success";
			} else if (sStatus === "O") {
				return "Warning";
			} else {
				return "Error";
			}
		},
		
		intBoolRandomizer: function(iRandom) {
			return iRandom % 2 === 0;
		},
		
		favorite: function(sStatus) {
			return sStatus.length % 2 === 0;
		},
		
		replace :  function (sStatus) {
			if (sStatus === "O") {
				return "Abierta";
			} else if (sStatus === "C") {
				return "Agendada";
			} else {
				return "Pendiente";
			}
		},
		
		statusStock: function (fValue) {
			try {
				fValue = parseInt(fValue);
				if (fValue > 100) {
					return "None";
				} else if (fValue > 10 && fValue < 99) {
					return "Success";
				} else if (fValue >= 1 && fValue <= 10) {
					return "Warning";
				} else if (fValue <= 0) {
					return "Error";
				} else {
					return "None";
				}
			} catch (err) {
				return "None";
			}
		},
		
		formatDate: function(fDate){
			if(fDate){
				return new Date(fDate).toISOString().slice(0,10);
			} else {
				return "0000-00-00";
			}
		},
		
		formatHour: function(fHour){
			var hora;
			if(fHour){
				hora  = fHour;
				hora = (hora.length === 3) ? "0" + hora : hora;
				hora = hora.substr(0,2) + ':' + hora.substr(2,2);
				return hora;
			} else {
				return hora = "--:--";
			}
		},
		
		formatNum: function(fNum){
			if(fNum)
				return parseInt(fNum);
		},
		
		replaceQuot1:  function (sStatus) {
			if (sStatus === "O") {
				return "Por Agendar";
			} else if (sStatus === "C") {
				return "Agendada";
			} else {
				return "Desconocido";
			}
		},
		
		statusOrd2: function(sStatus) {
			if (sStatus === "-1") {
				return "Success";
			} else if (sStatus === "0") {
				return "Warning";
			}  else if (sStatus === "1")  {
				return "Error";
			}
		},
		
		replaceOrd1:  function (sStatus) {
			if (sStatus === "O") {
				return "Entregas Pendientes";
			} else if (sStatus === "C") {
				return "Con Entregas";
			} else {
				return "Desconocido";
			}
		},
		
		replaceOrd2:  function (sStatus) {
			if (sStatus === "-1") {
				return "Sin Procesar";
			} else if (sStatus === "0") {
				return "En Proceso";
			}  else if (sStatus === "1")  {
				return "Procesado";
			}
		},
		
		replaceAsis:  function (sStatus) {
			if (!sStatus)
				return "Sin Asignar";
			else
				return sStatus;
		}
	};

	return Formatter;

}, /* bExport= */ true);