sap.ui.define([
	"sap/ui/core/mvc/Controller", 
	"sap/ui/core/routing/History",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/Filter",
	"sap/ui/model/Sorter",
	"./Formatter"
	
], function(Controller, History, FilterOperator, Filter, Sorter, Formatter){
	"use strict";
	var PurchaseMasterController = Controller.extend("SeidorB1UI5.controller.PurchaseMaster", {
		
	formatter: Formatter, busy: new sap.m.BusyDialog(),
	
	onInit: function() 	{
		if(document.cookie.length === 0){
			this.handleLogout();
		} else {
			this.busy.open();
			
			oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
			var oPrevious = History.getInstance().getPreviousHash();
			
			this.busy.close();
			//GUARDA ELA FUNCION ACTUALIZAR PARA FORZAR POSTERIOR CARGA DE PANTALLA
			const onAfterShow = () => this.handleActualizar();
			this._afterShowDelegate = { onAfterShow };
			if(oPrevious !== "PurchaseCreate" || oPrevious !== "Page")
				this.getView().addEventDelegate(this._afterShowDelegate);
			
			var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
			
			this.byId("avatar").setInitials(iAvatar);
			this.byId("avatar").setTooltip("Usuario: " + avatar);
			
			
			var oTable = this.getView().byId("tablaPurchase");
			var oModelTable = new sap.ui.model.json.JSONModel();
	
			oModelTable.setData(oPurchaseJSONData);
			oTable.setModel(oModelTable);
	    	this.getView().setModel(oModelTable);
	    	
	    	oTable.sort(this.getView().byId("DocDate"), sap.ui.table.SortOrder.Descending, true);
	    	oTable.sort(this.getView().byId("DocNum"), sap.ui.table.SortOrder.Descending, true);
	    	this.getView().setModel(this.oModelList);
	    	
	    	var fnPress = this.handleSelect.bind(this);
	    	this.modes=[{
	    		key: "Multi",
				text: "Multiple Actions",
				handler: function(){
					var oTemplate = new sap.ui.table.RowAction({items: [
						new sap.ui.table.RowActionItem({
							type: "Navigation",
							press: fnPress,
							visible: "{Available}"
						})
					]});
					return [2, oTemplate];
				}
	    	}];
	    	this.getView().setModel(new sap.ui.model.json.JSONModel({items: this.modes}), "modes");
			this.switchState("Multi");
		}
	},
	
	handleLogout: function (evt) {
		sessionStorage.clear();
		delCookies();
		window.location.replace("");
	},
	
	switchState : function (sKey) {
		var oTable = this.byId("tablaPurchase");
		var iCount = 0;
		var oTemplate = oTable.getRowActionTemplate();
		if (oTemplate) {
			oTemplate.destroy();
			oTemplate = null;
		}

		for (var i = 0; i < this.modes.length; i++) {
			if (sKey == this.modes[i].key) {
				var aRes = this.modes[i].handler();
				iCount = aRes[0];
				oTemplate = aRes[1];
				break;
			}
		}

		oTable.setRowActionTemplate(oTemplate);
		oTable.setRowActionCount(iCount);
	},
	
	handleActualizar: function(){
		oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
		
		var fecIni = "", fecFin = "";
		
		fecIni = this.getView().byId("dpFechaDesde").getValue();
		fecFin = this.getView().byId("dpFechaHasta").getValue();
		
		if(fecIni != "" && fecFin != ""){
			if(fecIni <= fecFin){
				getPurchase(fecIni, fecFin);
			}else{
				new sap.m.MessageBox.show(
						"Las fechas de inicio no puede ser mayor a la fin", { 
	               	icon: sap.m.MessageBox.Icon.ERROR, 
	              	title: "Error en Busqueda de Solicitudes de Compra."
	            });
			}
			
		}else{
			getPurchase();
		}
		//oPurchaseJSONData = JSON.parse(sessionStorage.getItem("ordenes"));
		
		var oTable = this.getView().byId("tablaPurchase");
	    var data = oTable.getModel();
		var oData = data.getData();
		
		data.setData(oPurchaseJSONData);
		data.refresh();
		this.getView().setModel(data);
	},
	
	handleSelect: function(oEvent) {
		var oRow = oEvent.getParameter("row");
		var oItem = oEvent.getParameter("item");
		ordersTable = oItem;
		selectedOrder = oEvent.getParameter("row").getCells()[0].getText();
		sap.ui.core.UIComponent.getRouterFor(this).navTo("PurchaseDetail",{from: "purchasemaster", contextPath: oItem.getBindingContext().getPath().substr(7)});
	},
	
	handleCrearBtnPress: function() {		
		sap.ui.core.UIComponent.getRouterFor(this).navTo("PurchaseCreate",{});
	},
	
	_filter : function () {
		var oFilter = null;

		if (this._oGlobalFilter && this._oPriceFilter) {
			oFilter = new sap.ui.model.Filter([this._oGlobalFilter, this._oPriceFilter], true);
		} else if (this._oGlobalFilter) {
			oFilter = this._oGlobalFilter;
		} else if (this._oPriceFilter) {
			oFilter = this._oPriceFilter;
		}
		this.byId("tablaPurchase").getBinding("rows").filter(oFilter, "Application");		
	},

	filterGlobally : function(oEvent) {
		var sQuery = oEvent.getParameter("query");
		this._oGlobalFilter = null;

		if (sQuery) {
			this._oGlobalFilter = new Filter([
				new Filter("RequesterName", FilterOperator.Contains, sQuery),
				new Filter("DocDate", FilterOperator.Contains, sQuery),
				new Filter("U_SEI_ESTADO", FilterOperator.Contains, sQuery)
			], false);
		}

		this._filter();
	},
	
	onBack : function () {
		var sPreviousHash = History.getInstance().getPreviousHash();

		//The history contains a previous entry
		if (sPreviousHash !== undefined) {
			window.history.go(-1);
		} else {
			// There is no history!
			// replace the current hash with page 1 (will not add an history entry)
			this.getOwnerComponent().getRouter().navTo("Page", null, true);
		}
	}
	});
	return PurchaseMasterController;
}, /* bExport= */ true);