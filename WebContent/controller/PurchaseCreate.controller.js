sap.ui.define([
	"jquery.sap.global",
	"sap/m/MessageToast",
	"./Formatter",
	"sap/ui/core/Fragment",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/Filter",	
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox"
	
], function(jQuery, MessageToast, Fragment, Formatter, Controller, FilterOperator, Filter, JSONModel, MessageBox) {
	"use strict";

	var PurchaseCreateController = Controller.extend("SeidorB1UI5.controller.PurchaseCreate", {
		/*
		 * Declaracion de variables
		 */
		mensaje: null, oTable: null, oItems:null, oJSONModel: null,
		codigo: null, descripcion: null, fecha1: null, fecha2: null,		
		oModelSel: null, oSelectD2: null, oSelectD3: null,
		
		/*
		 * Carga de Elementos de la vista
		 */
		onInit: function() {
			if(document.cookie.length === 0){
				this.handleLogout();
			} else {
				oDimenJSONData = JSON.parse(sessionStorage.getItem("location"));
				oItems = JSON.parse(sessionStorage.getItem("items"));
				oItems.departamentos = JSON.parse(sessionStorage.getItem('departamentos')).value;
				oItems.tipoFinanciamientos = JSON.parse(sessionStorage.getItem('tipoFinanciamientos')).value;
				
				if((oDatosUsuario === undefined || !oDatosUsuario) && sessionStorage.getItem("tiles")){
					oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
				} else if(!sessionStorage.getItem("tiles")){
					this.handleLogout();
				}
				
				
				
				var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
				this.byId("avatar").setInitials(iAvatar);
				this.byId("avatar").setTooltip("Usuario: " + avatar);
				
				this.oJSONModel = this.loadCombobox();
				this.byId("DP1").setDateValue(new Date());
				this.fecha1 = this.byId("DP1").getDateValue();
//				if(JSON.parse(sessionStorage.getItem("tiles")).des_suc)
//					this.byId("txtUnidad").setText(JSON.parse(sessionStorage.getItem("tiles")).des_suc);
				
				this.byId("txtSolicitante").setText(JSON.parse(sessionStorage.getItem("tiles")).user_name);
				this.byId("cbxTipoFinanciamiento").setValue(JSON.parse(sessionStorage.getItem("tiles")).des_fin);
				this.byId("cbxTipoFinanciamiento").setSelectedKey(JSON.parse(sessionStorage.getItem("tiles")).cod_fin);
				this.getView().setModel(this.oJSONModel);
			}
		},
		
		handleLogout: function (evt) {
			sessionStorage.clear();
			delCookies();
			window.location.replace("");
		},
		
		loadCombobox: function(){
			var oModel = new JSONModel();
			var aTemp = [];
			var oUbicacion1 = [];		
			for(var i = 0; i < oDimenJSONData.value.length; i++){
				var oUbicaciones = oDimenJSONData.value[i];				
				if(oUbicaciones.U_SEI_NIVEL1 && jQuery.inArray(oUbicaciones.U_SEI_NIVEL1, aTemp) < 0){
					aTemp.push(oUbicaciones.U_SEI_NIVEL1);
					oUbicacion1.push({"Value": oUbicaciones.U_SEI_COLEGIO,"Name": oUbicaciones.U_SEI_NIVEL1});
				}
			}
			
			//ESTRUCTURA FISICA
			var oEstruFisica = null;
			getAsync("estructuraFisica");
			oEstruFisica = JSON.parse(sessionStorage.getItem("estructuraFisica"));
			oItems.PhisicalStructure = oEstruFisica.value;
			
			//provisorio
			oItems.Ubicacion1 = oUbicacion1;
			oModel.setData(oItems);			
			return oModel;
		},
		
		validaCambioUbicacion: function(oEvent){
			var oModel = this.getView().getModel();
			var oData = oModel.getData();
			var oValidatedComboBox = oEvent.getSource(),
		    sSelectedKey = oValidatedComboBox.getSelectedKey(),
			sValue = oValidatedComboBox.getValue(), oBusinessPartner;
			
			oDatosUsuario.socio_cod = sSelectedKey;
			oDatosUsuario.cole_cod = sSelectedKey;
			getAsync("sociosNegocio");
			oBusinessPartner = JSON.parse(sessionStorage.getItem("sociosNegocio"));
			
			var oEstruFisica = null;
			getAsync("estructuraFisica");
			oEstruFisica = JSON.parse(sessionStorage.getItem("estructuraFisica"));
			oItems.PhisicalStructure = oEstruFisica.value;
			
			this.getView().byId("inpCustmrName").setValue(oBusinessPartner.CardCode +";"+oBusinessPartner.CardName);
			
		},
		
		/*
		 * Funcionalidades de Botones y Combobox de la vista
		 */
		handleNavButton: function(oEvent) {
			if (this._oDialog)
				this.cierraFragment();
			
			var sPreviousHash = sap.ui.core.routing.History.getInstance().getPreviousHash();
			//The history contains a previous entry
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				// There is no history!
				// replace the current hash with page 1 (will not add an history entry)
				this.getOwnerComponent().getRouter().navTo("PurchaseMaster", oPurchaseJSONData, true);
			}
		},

		handleAceptBtn: function(oEvent) {
			sap.ui.core.BusyIndicator.show(0);			
			this.validacionEnvio();
			
			if(this.mensaje === null) {
				var purchaseHeadVY = {DocumentLines:[]};
				var purchaseHeadVN = {DocumentLines:[]};
				var purchaseItems = {};
				var items = this.getView().byId("idItemsTable").getItems();
				
				purchaseHeadVY.Requester 	    = JSON.parse(sessionStorage.getItem("tiles")).user_code;
				purchaseHeadVY.RequesterName 	= JSON.parse(sessionStorage.getItem("tiles")).user_name;
				purchaseHeadVY.RequriedDate 	= this.fecha1;				
				purchaseHeadVY.RequesterEmail 	= JSON.parse(sessionStorage.getItem("tiles")).user_email;
				purchaseHeadVY.U_SEI_UNIDAD		= JSON.parse(sessionStorage.getItem("tiles")).des_suc;
				purchaseHeadVY.U_SEI_VIRTUAL  	= 'tYES';
				purchaseHeadVY.U_SEI_TIPO_FINANCIAMIENTO = this.byId("cbxTipoFinanciamiento").getValue();
				purchaseHeadVY.U_SEI_DEPARTAMENTO = this.byId("cbxDepartamento").getValue();
				
				purchaseHeadVN.RequesterName 	= JSON.parse(sessionStorage.getItem("tiles")).user_name;
				purchaseHeadVN.RequriedDate 	= this.fecha1;				
				purchaseHeadVN.RequesterEmail 	= JSON.parse(sessionStorage.getItem("tiles")).user_email;
				purchaseHeadVN.U_SEI_UNIDAD 	= JSON.parse(sessionStorage.getItem("tiles")).des_suc;
				purchaseHeadVN.U_SEI_VIRTUAL  	= 'tNO';
				purchaseHeadVN.U_SEI_TIPO_FINANCIAMIENTO = this.byId("cbxTipoFinanciamiento").getValue();
				purchaseHeadVN.U_SEI_DEPARTAMENTO = this.byId("cbxDepartamento").getValue();
										
				for(var i = 0; i < items.length; i++){
					if(items[i].getCells()[0].getText() === "tYES"){					
						purchaseItems = {
								ItemCode: 			items[i].getCells()[1].getText(),							
								ItemDescription:	items[i].getCells()[2].getText(),
								FreeText:			items[i].getCells()[3].getValue(),
								Quantity:			items[i].getCells()[4].getValue(),
								RequieredDate:		this.fecha1,
								U_SEI_ISBN:			items[i].getCells()[5].getValue(),
								U_SEI_NIVEL1:		items[i].getCells()[6].getText(),//unidad descripcion
								U_SEI_NIVEL2:		items[i].getCells()[7].getValue(),
								U_SEI_NIVEL3:		items[i].getCells()[8].getValue(),
								U_SEI_NIVEL4:		items[i].getCells()[9].getValue(),
								CostingCode:		JSON.parse(sessionStorage.getItem("tiles")).cod_suc,//unidad codigo
								CostingCode2:		this.byId("cbxTipoFinanciamiento").getSelectedKey(),//this.byId("cbxTipoFinanciamiento").getValue(),//tipoFinanciamiento
								CostingCode3:		this.byId("cbxDepartamento").getSelectedKey()//Departamento codigo
						}
						purchaseHeadVY.DocumentLines.push(purchaseItems);
					} else if (items[i].getCells()[0].getText() === "tNO"){
						purchaseItems = {
								ItemCode: 			items[i].getCells()[1].getText(),							
								ItemDescription:	items[i].getCells()[2].getText(),
								FreeText:			items[i].getCells()[3].getValue(),
								Quantity:			items[i].getCells()[4].getValue(),
								RequieredDate:		this.fecha1,
								U_SEI_ISBN:			items[i].getCells()[5].getValue(),
								U_SEI_NIVEL1:		items[i].getCells()[6].getText(),//unidad descripcion
								U_SEI_NIVEL2:		items[i].getCells()[7].getValue(),
								U_SEI_NIVEL3:		items[i].getCells()[8].getValue(),
								U_SEI_NIVEL4:		items[i].getCells()[9].getValue(),
								CostingCode:		JSON.parse(sessionStorage.getItem("tiles")).cod_suc,//unidad codigo
								CostingCode2:		this.byId("cbxTipoFinanciamiento").getSelectedKey(),//this.byId("cbxTipoFinanciamiento").getValue(),//tipoFinanciamiento
								CostingCode3:		this.byId("cbxDepartamento").getSelectedKey()//Departamento codigo
						}
						purchaseHeadVN.DocumentLines.push(purchaseItems);
					}
				}
				
				var res = 0;
				if(purchaseHeadVY.DocumentLines.length > 0){
					res = setPurchase(purchaseHeadVY);
					if(res > 0)
						this.onDelete(items);
						this.limpiarCampos();
				}
				
				if(purchaseHeadVN.DocumentLines.length > 0){
					res = setPurchase(purchaseHeadVN);
					if(res > 0)
						this.onDelete(items);
						this.limpiarCampos();
				}
			} else {
				sap.m.MessageBox.show(this.mensaje, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Informacion",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		},
		
		/*
		 * Elimina celdas seleccionadas de la tabla
		 */
		handleEliminBtn: function(oEvent) {
			var oTable = this.getView().byId("idItemsTable");
		    var model = oTable.getModel();
		    var selRowCount = oTable.getSelectedItems().length;
		    if(selRowCount > 0){
			    for (var i = 0; i < selRowCount; i++) {
			        var rowNum = oTable.getSelectedItems()[i].getBindingContext().getPath().replace("/newItems/", "");
					var oData = model.getData();
					oData.newItems.splice(rowNum, 1);
					model.setData(oData);
					model.refresh();
			    }
			} else {
				new MessageBox.show("No se ha seleccionado lineas a eliminar", {
					icon: MessageBox.Icon.WARNING,
					title: "Informacion Crear Llamada se Servicio",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		},
		
		// A continuación se muestra la definición de la función para llamar a la capa de servicio //
		// para cancelar un pedido de cliente abierto //
		handleCancelBtn: function(oEvent) {
			var oTable = this.getView().byId("idItemsTable");
			if(oTable.getModel().getData().newItems !== undefined){
				var items = oTable.getItems();
				this.onDelete(items);
				this.limpiarCampos();
			} else {
				new MessageBox.show("No hay lineas creadas a eliminar", {
					icon: MessageBox.Icon.WARNING,
					title: "Informacion Crear Solicitud de Orden",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		},
		
		handleTableSelectDialog: function(oEvent) {
			this.mensaje= null;
			this.validaUbicaciones();
			if(!this.mensaje){
				if (!this._oDialog) {
					this._oDialog = sap.ui.xmlfragment("SeidorB1UI5.view.ItemsMaster", this);
					this._oDialog.setModel(this.oJSONModel);
				}			
				this.getView().addDependent(this._oDialog);		
				this._oDialog.open();
			} else {
				new sap.m.MessageBox.show(this.mensaje, {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Informacion Solicitud De Orden",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		},
		
		handleUnidad: function(oEvent) {
			var items = this.getView().byId("idItemsTable").getItems();
			var oModel = this.getView().byId("idItemsTable").getModel();
			oPurchaseJSONData = oModel.getData();
			
			for(var item = 0; item < items.length; item++){
				var linea = items[item].getCells()[0].getText();
				this.unidad = (items[item].getCells()[6].getText() ? items[item].getCells()[6].getText() : items[items.length - 1].getCells()[6].getText())
				this.unidad = items[item].getCells()[6].getText();
				
				var aTemp = [];
				var aDimension2 = [];
				for(var i = 0; i < oDimenJSONData.value.length; i++){
					var oDimensiones = oDimenJSONData.value[i];				
					//if(oDimensiones.U_SEI_NIVEL1 === this.unidad && 
					if(oDimensiones.U_SEI_NIVEL1 === this.unidad && oDimensiones.U_SEI_NIVEL2 && jQuery.inArray(oDimensiones.U_SEI_NIVEL2, aTemp) < 0){
						aTemp.push(oDimensiones.U_SEI_NIVEL2);
						aDimension2.push({"Name": oDimensiones.U_SEI_NIVEL2});
						this.cardCode = oDimensiones.U_SEI_COLEGIO;
					}
				}
//				if (linea){
//					oPurchaseJSONData.value[item].DocumentLines[item].Dimension2 = aDimension2;
//					this.cargaCbxUnidad(items[item].getCells()[9].getValue(), items[item].getCells()[10].getValue(), item)
//				} else if (linea === ""){
				//oItems.newItems[items.length -1].DocumentLines[item].Dimension2 = aDimension2;
				oItems.newItems[items.length -1].Dimension2 = aDimension2;
//				}
				oModel.setData(oItems);
				oModel.refresh();
			}
		},
		
		handleLoadItems: function(oControlEvent, idCbx) {
			//var selKey = oControlEvent.getParameter("selectedItem").getKey();
			var selText = oControlEvent.getParameter("selectedItem").getText();
			var path = oControlEvent.getSource().getBindingContext().sPath;
			path = path.substr(path.length-1);
			var nivel1 = oItems.newItems[path].Dimension1;
			
			var oModel = new JSONModel();
			var aTemp = [];
			var aDimension3 = [];
			var aDimension4 = [];			
			
			if(idCbx === "dimension2") {
				this._dimension2 = selText;
				for(var i = 0; i < oDimenJSONData.value.length; i++){
					var oDimensiones = oDimenJSONData.value[i];				
					if(oDimensiones.U_SEI_NIVEL1 == nivel1 && 
					   oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
					   oDimensiones.U_SEI_NIVEL3 && 
					   jQuery.inArray(oDimensiones.U_SEI_NIVEL3, aTemp) < 0){						
						aTemp.push(oDimensiones.U_SEI_NIVEL3);
						aDimension3.push({"Name": oDimensiones.U_SEI_NIVEL3});						
					} else if(oDimensiones.U_SEI_NIVEL1 == nivel1 && oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
							   !oDimensiones.U_SEI_NIVEL3 && 
							   jQuery.inArray(oDimensiones.U_SEI_NIVEL4, aTemp) < 0){
						aTemp.push(oDimensiones.U_SEI_NIVEL4);
						aDimension4.push({"Name": oDimensiones.U_SEI_NIVEL4});
					}
				}				
				oItems.newItems[path].Dimension3 = aDimension3;
				
				if(aDimension4)
					oItems.newItems[path].Dimension4 = aDimension4;
				
			} else if(idCbx === "dimension3") {
				this._dimension3 = selText;
				for(var i = 0; i < oDimenJSONData.value.length; i++){
					var oDimensiones = oDimenJSONData.value[i];				
					if(oDimensiones.U_SEI_NIVEL1 == nivel1 && 
					   oDimensiones.U_SEI_NIVEL2 === this._dimension2 && 
					   oDimensiones.U_SEI_NIVEL3 === this._dimension3 && 
					   oDimensiones.U_SEI_NIVEL4 && 
					   jQuery.inArray(oDimensiones.U_SEI_NIVEL4, aTemp) < 0){
						
						aTemp.push(oDimensiones.U_SEI_NIVEL4);
						aDimension4.push({"Name": oDimensiones.U_SEI_NIVEL4});
					}
				}
				oItems.newItems[path].Dimension4 = aDimension4;				
			}
			oModel.setData(oItems);
			oModel.refresh();
//			this.oSelectD3.setModel(oModel);
//			this.getView.getModel().setData(oModel);
		},
		
		/*
		 * 
		 * COMPORTAMIENTO DEL POPUP CON ITEMS
		 * 
		 */
		handleSelect: function(oEvent) {			
			var aContexts = oEvent.getParameter("selectedContexts");			
			// Si se selecciono un items entra en esta validacion y la agrega a la tabla
			if(aContexts){
				var itemCode,itemName,itemsGroupCode,virtualAssetItem,inventoryItem,Properties1;
				
				if (aContexts && aContexts.length){
					aContexts.map(function(oContext) { 
						itemCode 			= oContext.getObject().ItemCode;
						itemName 			= oContext.getObject().ItemName;
						itemsGroupCode 		= oContext.getObject().ItemsGroupCode;
						virtualAssetItem 	= oContext.getObject().VirtualAssetItem;
						inventoryItem		= oContext.getObject().InventoryItem;
						Properties1			= oContext.getObject().Properties1;
					});
				}
				this.cierraFragment();
				var date = new Date();
				var fechaActual = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();			
				this.fecha2 = (this.fecha1 !== null ? this.fecha1 : fechaActual);
				
				var items = this.getView().byId("idItemsTable").getItems();
				//Actualiza la cantidad en el jsondata
				for(var i = 0; i < items.length; i++){
					for(var x = 0; x < this.oItems.newItem.length; x++ ){
						if(items[i].getCells()[1].getText === this.oItems.newItem[x].itemCode &&
							items[i].getCells()[2].getText() === this.oItems.newItem[x].itemName &&
							items[i].getCells()[3].getText() === this.oItems.newItem[x].FreeText &&
							items[i].getCells()[4].getValue() === this.oItems.newItem[x].Quantity &&
							items[i].getCells()[5].getValue() === this.oItems.newItem[x].Isbn &&
							items[i].getCells()[6].getValue() === this.oItems.newItem[x].Dimension2 &&
							items[i].getCells()[7].getValue() === this.oItems.newItem[x].Dimension3 &&
							items[i].getCells()[8].getValue() === this.oItems.newItem[x].Dimension4 ){

								this.oItems.newItem[x].FreeText 	= items[i].getCells()[3].getValue();
								this.oItems.newItem[x].Quantity 	= items[i].getCells()[4].getValue();
								this.oItems.newItem[x].Isbn 		= items[i].getCells()[5].getValue();
								this.oItems.newItem[x].Dimension2 	= items[i].getCells()[6].getValue();
								this.oItems.newItem[x].Dimension3 	= items[i].getCells()[7].getValue();
								this.oItems.newItem[x].Dimension4 	= items[i].getCells()[8].getValue();
						}
					}
				}
				
				if(this.oItems === null){
					this.oItems = {newItem: [{ "VirtualAssetItem": virtualAssetItem,"itemCode": itemCode, "itemName": itemName, "FreeText": "", "Quantity": 1, "Isbn": "", "Dimension1": this.byId("cbxUbicacion").getSelectedItem().getText(),"Dimension2": "", "Dimension3": "", "Dimension4": "", "InventoryItem": inventoryItem, "ItemsGroupCode":itemsGroupCode, "Properties1": Properties1, "RequieredDate" : this.fecha2, "CostingCode" : "D1001"}]};
				} else {
					this.oItems.newItem.push({ "VirtualAssetItem": virtualAssetItem,"itemCode": itemCode, "itemName": itemName, "FreeText": "","Quantity": 1, "Isbn": "", "Dimension1": this.byId("cbxUbicacion").getSelectedItem().getText(), "Dimension2": "", "Dimension3": "", "Dimension4": "", "InventoryItem": inventoryItem, "ItemsGroupCode":itemsGroupCode, "Properties1": Properties1, "RequieredDate" : this.fecha2, "CostingCode" : "D1001"});
				}
				
				//Actualiza el oModel para posterior carga
				var oView = oEvent.getSource();
				var oData = oView.getModel().getData();
				oData.newItems = this.oItems.newItem;
				oView.getModel().setData(oData);
				
				//Recarga la lista con nuevos productos
				this.oTable = this.getView().byId("idItemsTable");
				this.oColumn = this.getView().byId("idColList");
				this.oTable.setModel(this.oJSONModel);
				this.oTable.bindItems("/newItems", this.oColumn);
				
				items = this.getView().byId("idItemsTable").getItems();
				if(itemsGroupCode === 117)
					items[items.length-1].getCells()[5].setEnabled(true);
				
				if(Properties1 === "tYES"){
					items[items.length-1].getCells()[7].setEnabled(true);
					items[items.length-1].getCells()[8].setEnabled(true);
					items[items.length-1].getCells()[9].setEnabled(true);
				}
				this.habilitaCampos();
				this.handleUnidad();
			}
		},
		
		cierraFragment: function(oEvent) {
	        this._oDialog.destroy();  //Second: destoy fragment 
	        this._oDialog=null;  // Third: null name/pointer 
	    },
		
		_filter : function (oEvent) {
			var oFilter = null;

			if (this._oGlobalFilter && this._oPriceFilter) {
				oFilter = new sap.ui.model.Filter([this._oGlobalFilter, this._oPriceFilter], true);
			} else if (this._oGlobalFilter) {
				oFilter = this._oGlobalFilter;
			} else if (this._oPriceFilter) {
				oFilter = this._oPriceFilter;
			}

			// actualizar la lista de enlace	
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter(oFilter);		
		},

		filterGlobally : function(oEvent) {
			var sQuery = oEvent.getParameter("value");
			this._oGlobalFilter = null;

			if (sQuery) {
				this._oGlobalFilter = new Filter([
					new Filter("ItemName", FilterOperator.Contains, sQuery),
					new Filter("ItemCode", FilterOperator.Contains, sQuery)
				], false);
			}

			this._filter(oEvent);
		},
		
		handleChange: function(oEvent) {
			this.fecha1 = oEvent.getParameter("value");
		},
		
		onDelete: function(items){
			for(var i = 0; i < items.length; i++){			
				var  oModel = this.getView().byId("idItemsTable").getModel();
				var oData = oModel.getData();
				var removed = oData.newItems.splice(0, 1);
				oModel.setData(oData);
				oModel.refresh();
			}
		},
		
		/*
		 * Reestablece los campos a de la cabecera a como estaban inicialmente la pagina
		 */
		limpiarCampos: function(){			
			this.byId("cbxDepartamento").setValue("Selec. Departamento");
			this.byId("cbxTipoFinanciamiento").setValue(JSON.parse(sessionStorage.getItem("tiles")).des_fin);
			this.byId("DP1").setDateValue(new Date());
			
		},
		
		validacionEnvio: function() {
			this.mensaje = null;
			var items = this.getView().byId("idItemsTable").getItems();
			
			if(items.length > 0)
				this.validaUbicaciones();
			else if(items.length === 0)
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No hay Articulos para crear una Solicitud." : "- No hay Articulos para crear una Solicitud.");

			if(this.getView().byId("cbxDepartamento").getValue() === "Selec. Departamento")
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Se debe seleccionar Departamento." : "- Se debe seleccionar Departamento.");
			
			if(this.getView().byId("cbxTipoFinanciamiento").getValue() === "Selec. Financiamiento")
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Se debe seleccionar Financiamiento." : "- Se debe seleccionar Financiamiento.");
			
			if(this.fecha1 === null)
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Se debe seleccionar fecha de Solicitud." : "- Se debe seleccionar fecha de Solicitud.");
			
			for(var i = 0; i < items.length; i++){
				var linea = i + 1;
				if(!items[i].getCells()[5].getValue() && items[i].getCells()[5].getEnabled() === true)
					this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Se debe ingresar ISBN en la linea " + linea + "." : "- Se debe ingesar ISBN en la linea " + linea + ".");
			}
		},
		
		validaUbicaciones: function(){
			var items = this.getView().byId("idItemsTable").getItems();
			if(items.length > 0){
				if(items[items.length-1].getCells()[7].getEnabled() === true){
					if(items[items.length-1].getCells()[7].getValue() && 
					   items[items.length-1].getCells()[8].getValue() && 
						(oItems.newItems[items.length-1].Dimension4.length > 0 && items[items.length-1].getCells()[8].getValue())){
						this.mensaje = null;
					} else if(items[items.length-1].getCells()[7].getValue() && 
							  items[items.length-1].getCells()[8].getValue() && 
							  oItems.Dimension4.length === 0 ){
						this.mensaje = null;
					} else {
						this.mensaje = "- Debe seleccionar Ubicaciones.";
					}
				}
				
				if(!items[items.length-1].getCells()[5].getValue() && items[items.length-1].getCells()[5].getEnabled() === true){
					this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Se debe ingresar ISBN." : "- Se debe ingesar ISBN.");
				}
			}
		},
		
		handleSearch: function(oEvent) {
			//Se obtiene el texto agregado para filtrar
			var sValue = oEvent.getParameter("value");
			//Si contiene datos o el largo es mayor a 0 entra en validacion
			if(sValue && sValue.length > 0)
				//se crea un filtro con 3 componentes "nombreCamppo", tipoFiltro y cadenaBuscar"
				var oFilter = new Filter("ItemName", sap.ui.model.FilterOperator.Contains, sValue);

			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([oFilter]);
		},

		handleClose: function(oEvent) {
			oEvent.getSource().close();
			//oEvent.getSource().destroy();
		},
		
		habilitaCampos: function(){
			var items = this.getView().byId("idItemsTable").getItems();
			for(var i = 0; i < items.length; i++){
				if(items[i].getCells()[11].getText() === "117")
					items[i].getCells()[5].setEnabled(true);

				if(items[i].getCells()[12].getText() === "tYES"){
					items[i].getCells()[7].setEnabled(true);
					items[i].getCells()[8].setEnabled(true);
					items[i].getCells()[9].setEnabled(true);
				} else {
					items[i].getCells()[7].setEnabled(false);
					items[i].getCells()[8].setEnabled(false);
					items[i].getCells()[9].setEnabled(false);
				}
			}
		},
				
		onExit: function() {
			if (this._oDialog) {
//				this._oDialog.destroy();
			}
		}
	});
	return PurchaseCreateController;
});