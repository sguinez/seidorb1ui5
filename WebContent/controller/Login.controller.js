//window.onbeforeunload = function() {
//	return "¿Estás seguro que deseas salir de la actual página?"
//}
sap.ui.define([
		'jquery.sap.global',
		'sap/ui/core/mvc/Controller',
		'sap/ui/model/json/JSONModel',
		'sap/m/MessageBox'
	], function(jQuery, Controller, JSONModel, MessageBox) {
	"use strict";

	var LoginController = Controller.extend("SeidorB1UI5.controller.Login", {

		hash: "",
		
		onInit : function (evt) {			
			this.getView().byId("vPortal").setText("Version: " + vPortal);
		},
		
		onBeforeRendering : function (evt) {
			var originPath = window.location.origin + window.location.pathname
			this.hash = localStorage.getItem("hash");
			
			if(document.cookie.length > 0)
				window.location.replace(originPath + "#/Page");
			
		},

		handleLogeoPress : function (evt) {
			var loginInfo = {};
			loginInfo.CompanyDB = this.getView().byId("company").getValue();
			loginInfo.Password  = this.getView().byId("pass").getValue();
			loginInfo.UserName  = this.getView().byId("user").getValue();
			sessionStorage.setItem("SESSIONID", JSON.stringify(loginInfo));//JSON.stringify(new Date())
			
			if(loginInfo.CompanyDB === "" || loginInfo.Password === "" || loginInfo.UserName === ""){
				new sap.m.MessageBox.show(
					"Los campos de acceso no puede estar vacios.",{
					icon : sap.m.MessageBox.Icon.ERROR,
					title : "Validación de campos"
				});
				return;
			}
			
			if(document.cookie.length === 0){
				sessionStorage.clear();
				var loRouter = sap.ui.core.UIComponent.getRouterFor(this);
				
				loginInfo.Language  = 25;
				
				var basicAuth = `es-ES/${loginInfo.UserName}/${loginInfo.CompanyDB}:${loginInfo.Password}`
					basicAuth = jQuery.base64.encode(basicAuth);
	
				sap.ui.core.BusyIndicator.show(0);			
				$.ajax({
	                type: "POST",
	                dataType: "JSON",
	                xhrFields: { withCredentials: true },
					crossDomain: true,
	                url: "B1SLLogic?cmd=login&vPortal="+ vPortal,
	                data: JSON.stringify(loginInfo),
	                beforeSend: function () { },
	                success: function (res) {
	                	var result = JSON.parse(res);
	                    if (result.error) {
	                    	if(result.error.code === 301){	            				
	        					new sap.m.MessageBox.show(
	        							"Error en la autenticacion: Codigo " + result.error.code + "\n" +
	        							"El usuario necesita Permisos en SAP para realizar esta Operacion o \n Los Datos Ingresados del Usuario son Incorrectos", { 
	        		               	icon: sap.m.MessageBox.Icon.ERROR, 
	        		              	title: "Fallo En Llamada.",
	        		              	onClose: function(oAction){
	        		              		delCookies();
	        		    				window.location.replace("");
        								sap.ui.core.BusyIndicator.hide();
	        		              	}
	        		            });
	        				} else {
	        					sap.m.MessageBox.show(	        				
	                        		"Service Layer El Login ha Fallado: Codigo " + result.error.code + ". " + result.error.message.value,
	        		  		      	{
	        							icon: sap.m.MessageBox.Icon.ERROR,
	        							title: "Fallo En Ingreso",
	        							onClose: function() {
	        								delCookies();
	        								window.location.replace("");	        								
	        								sap.ui.core.BusyIndicator.hide();
	        							}
	        		  		      	});
	        				}
	                        return;
	                    } else {
	                    	sessionStorage.clear();
	                    	sessionStorage.setItem("timeSession", JSON.stringify(new Date()));
	                    	
	                    	sessionStorage.setItem("cookieSESSIONID", document.cookie);
	                    	sessionStorage.setItem("basicAuth", basicAuth);
	                    		                    	
	                    	sessionStorage.setItem("tiles", res);
							oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
							
                    		B1HSL_RouteID = document.cookie.match('(^|;) ?' + "ROUTEID" + '=([^;]*)(;|$)')[2];
                    		B1HSL_SessionID = document.cookie.match('(^|;) ?' + "B1SESSION" + '=([^;]*)(;|$)')[2];
                    		
//                    		getAsync('departamentos'); getAsync('location'); getAsync('items');
//                    		getAsync('tipoDocumentos'); getAsync('serviceEspecialidades');
//                    		getAsync('rutProveedores'); getAsync('rutFuncionarios');
//                    		getAsync('tipoFinanciamientos'); getAsync('tipoRendicion');
                    		
                    		asyncCall();
                    		
                    		new sap.m.MessageToast.show("Ingreso Satisfactorio!!.");
                    		var originPath = window.location.origin + window.location.pathname
                    		
                    		if(localStorage.getItem("hash") === "" || localStorage.getItem("hash") === null)
                    			window.location.replace(originPath + "#/Page");
                    		else
                    			window.location.replace(localStorage.getItem("hash"));
	                    }
	                },
					error: function (request, textStatus, errorThrown) {
	                    sap.m.MessageBox.show(
	                    		"Service Layer El Login ha Fallado: " + textStatus + " / " + errorThrown,
	    		  		      	{
	    							icon: sap.m.MessageBox.Icon.ERROR,
	    							title: "Fallo En Ingreso",
	    							onClose: function() {
	    								delCookies();
	    								window.location.replace("");
	    								sap.ui.core.BusyIndicator.hide();
	    							}
	    		  		      	});
	                }
	            });
			} else {
				delCookies();
				window.location.replace("");
				sap.ui.core.BusyIndicator.hide();
			}
		},

		handleBusyPress : function (evt) {
			var oTileContainer = this.byId("container");
			var newValue = !oTileContainer.getBusy();
			oTileContainer.setBusy(newValue);
			evt.getSource().setText(newValue ? "Done" : "Busy state");
		},

		handleTileDelete : function (evt) {
			var tile = evt.getParameter("tile");
			evt.getSource().removeTile(tile);
		}
	});

	return LoginController;

});