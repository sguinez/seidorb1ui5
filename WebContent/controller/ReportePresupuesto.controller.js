sap.ui.define([
	"sap/ui/core/mvc/Controller", 
	"sap/ui/core/routing/History",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/Filter",
	"sap/ui/model/Sorter",
	"sap/ui/core/Core",
	"sap/ui/core/library",
	"./Formatter"
], function(Controller, History, FilterOperator, Filter, Sorter, Core, CoreLibrary, Formatter){
	"use strict";
	var ReportePresupuestoController = Controller.extend("SeidorB1UI5.controller.ReportePresupuesto", {
		
		formatter: Formatter,
		ValueState: sap.ui.core.ValueState,
		
		onInit: function() {
			if(document.cookie.length === 0 || sessionStorage.length <= 0){
				this.Click_btnCierraSesion();
			} else {
				oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
			
				var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
				this.byId("avatar").setInitials(iAvatar);
				this.byId("avatar").setTooltip("Usuario: " + avatar);
				
				//this.byId("dpFechaReporte").setDateValue(new Date(2016, 1, 16));
				this.byId("dpFechaReporte").setMaxDate(new Date());
	    	}
		},
		
		Click_btnCierraSesion: function (evt) {
			sessionStorage.clear();
			delCookies();
			window.location.replace("");
		},
	
		CLick_onBack : function () {
			var sPreviousHash = History.getInstance().getPreviousHash();
	
			//The history contains a previous entry
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				// There is no history!
				// replace the current hash with page 1 (will not add an history entry)
				this.getOwnerComponent().getRouter().navTo("Page", null, true);
			}
		},
		
		CLick_dpFechaReporte: function(oEvent){
			var oText = this.byId("textResult"),
			oDP = oEvent.getSource(),
			//sValue = oEvent.getParameter("value"),
			bValid = oEvent.getParameter("valid");

			if (bValid) {
				oDP.setValueState(this.ValueState.None);
			} else {
				oDP.setValueState(this.ValueState.Error);
			}
		},
		
		Click_btnBuscar: function(){
			var fechaBusqueda = this.getView().byId("dpFechaReporte");
			if(fechaBusqueda.getDateValue() !== "" && fechaBusqueda.getValueState() === "None" && fechaBusqueda.getDateValue() !== null){
				fechaBusqueda = (new Date(fechaBusqueda.getDateValue())).getFullYear();
				getReportPresupuesto(oDatosUsuario.cod_suc, fechaBusqueda)
			} else {
				new sap.m.MessageBox.show("El año seleccionado esta vacio o el formato es incorrecto.", {
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Informacion Reporte Presupuesto",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		}
	});
	return ReportePresupuestoController;
}, 
/* bExport= */ 
true);