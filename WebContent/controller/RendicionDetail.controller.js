jQuery.sap.require("sap.ui.core.format.DateFormat", "SeidorB1UI5.controller.Formatter");

sap.ui.core.mvc.Controller.extend("SeidorB1UI5.controller.RendicionDetail", {
	
	formatter: SeidorB1UI5.controller.Formatter,
	
	codSocio: null, montoEntregado: null, saldoPendiente: null,
	nroPago: null, fechaPagoEntregado: null,
	// MODIFICAR //
	// A continuación se muestra el procedimiento de enlace de la vista Detalles para mostrar los datos maestros de elementos en el área del encabezado //
	// MODIFICAR //
	onInit: function() {
		oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
		
		sap.ui.core.UIComponent.getRouterFor(this).attachRouteMatched(function(oEvent) {
			// cuando se produce una navegación detallada, actualice el contexto de enlace
			// MODIFICAR //
			// A continuación se muestra el procedimiento de encuadernación de la vista Tabla de artículos para mostrar las líneas de documento de pedido de ventas en el área Detalles //
			// Es posible que deba cambiar el Identificador de servicio de /DocumentLines para que coincida con el nombre de su colección de Entity Lines
			// MODIFICAR //			
			if (oEvent.getParameter("name") === "RendicionDetail") {
		
			if(!oRendicionJSONData || oRendicionJSONData === undefined)
				oRendicionJSONData = JSON.parse(sessionStorage.getItem("rendiciones"));
			
			
	    		var oModel = null;
	    		var view = this.getView();
	    		
	    		var btnHabi = view.byId("btnHabil");
	    		var btnUpda = view.byId("btnUpdate");
	    		var btnElim = view.byId("btnElimin");
	    		var btnEnvi = view.byId("btnEnviar");
	    		var btnApro = view.byId("btnAprobar");
	    		var btnRech = view.byId("btnRechazar");
	    		
	    		if(oDatosUsuario.code_rol === "2") {
	    			//OCULTA BOTON SI ES NECESARIO SEGUN ID DE COMPONENTE
	    			btnApro.setVisible(false);
	    			btnRech.setVisible(false);
	            } else if (oDatosUsuario.code_rol === "1"){
	            	btnApro.setEnabled(true);
	    			btnRech.setEnabled(true);
	            	
	            	btnHabi.setVisible(false);
	        		btnUpda.setVisible(false);
	        		btnElim.setVisible(false);
	        		btnEnvi.setVisible(false);
	            }
	    		
	    		oModel = new sap.ui.model.json.JSONModel();
	    		oModel.setData(oRendicionJSONData);
	        	view.setModel(oModel);
	        	
	        	oRendicionJSONData.tipoDocumentos 		= JSON.parse(sessionStorage.getItem('tipoDocumentos')).value;
	        	oRendicionJSONData.rutProveedores 		= JSON.parse(sessionStorage.getItem('rutProveedores')).value;
	        	oRendicionJSONData.tipoRendicion 		= JSON.parse(sessionStorage.getItem('tipoRendicion')).value;
				this.oRendicion1 = {value:[]}; this.oRendicion2 = {value:[]};
				for(var i = 0; i < JSON.parse(sessionStorage.getItem('tipoRendicion')).value.length; i++){
					if(JSON.parse(sessionStorage.getItem('tipoRendicion')).value[i].U_SEI_TRENDI === "1"){
						this.oRendicion1.value.push(JSON.parse(sessionStorage.getItem('tipoRendicion')).value[i]);
					} else if(JSON.parse(sessionStorage.getItem('tipoRendicion')).value[i].U_SEI_TRENDI === "2"){
						this.oRendicion2.value.push(JSON.parse(sessionStorage.getItem('tipoRendicion')).value[i]);
					}
				}
			
    		
				if(ordersTable){
					var oListItem = ordersTable;			    
					var context = new sap.ui.model.Context(view.getModel(), oListItem.getBindingContext().getPath());
					this.path = oListItem.getBindingContext().getPath().substr(7);
					view.setBindingContext(context);
					
					oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
					var avatar = oDatosUsuario.user_name, iAvatar = oDatosUsuario.user_name.obtenerIniciales();
					this.byId("avatar").setInitials(iAvatar);
					this.byId("avatar").setTooltip("Usuario: " + avatar);
					
					this.nroPago = view.getModel().getData().value[this.path].U_SEI_NROPAGO;
					
					var TRen = this.byId("txtCabTipoRendicion").getText();
					var oData = view.getModel().getData();
					if(TRen === "1"){
						oData.tipoRendicion = this.oRendicion1.value;
					} else if (TRen === "2"){
						oData.tipoRendicion = this.oRendicion2.value;
					}
					view.getModel().setData(oData);
					
					// Asegúrate de que el maestro esté aquí
					var oItemsTbl = this.getView().byId("idItemsTable");
					var oColListItem = this.getView().byId("idColListItem");
					oItemsTbl.setModel(oModel);
					oItemsTbl.bindItems(context.sPath + "/SEI_RENDETCollection", oColListItem);
					
					var estado = view.byId("text2").getText();//U_SEI_ESTADO
					var envio  = oData.value[this.path].U_SEI_ENVIO;//U_SEI_ENVIO
					
					this.validaEstadoEnvio(envio, estado);
					this.handlePago();
					this.getView().byId("txtCabSaldoPendiente").setText(this.saldoPendiente);
				} else {
					this.handleNavButton();
				}
			}
		}, this);
	},
	
	handleLogout: function (evt) {
		sessionStorage.clear();
		delCookies();
		window.location.replace("");
	},
	
	//Agregar una nueva linea
	handleRendicion: function(oEvent) {
		if(this.validaEstado()) {
			
			this.validaCampos();
			if(this.mensaje){
				new sap.m.MessageBox.show(this.mensaje, {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: "Informacion",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			} else if(this.mensaje === null){
				var date = new Date();
				//var fechaActual = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
				var sumaMonto = 0;
				
				var itemRen = oRendicionJSONData.value[this.path];
				var itemCol = oRendicionJSONData.value[this.path].SEI_RENDETCollection;
				var items = this.getView().byId("idItemsTable").getItems();
				var U_SEI_CTASUPERE = items[items.length-1].getCells()[9].getText();
				var U_SEI_RESULT 	= items[items.length-1].getCells()[10].getText();
				
				//Actualiza la cantidad en el jsondata
				for(var i = 0; i < items.length; i++){				
					for(var x = 0; x < itemCol.length; x++ ){
						if(items[i].getCells()[1].getValue() === itemCol[x].U_SEI_NRODOC &&
						items[i].getCells()[2].getValue()  	=== itemCol[x].U_SEI_FECHADOC &&
						items[i].getCells()[3].getValue() 	=== itemCol[x].U_SEI_TIPODOC &&
						items[i].getCells()[4].getValue()  	=== itemCol[x].U_SEI_RUT &&
						items[i].getCells()[5].getValue() 	=== itemCol[x].U_SEI_NOMP &&
						items[i].getCells()[6].getValue() 	=== itemCol[x].U_SEI_FACTASO &&
						items[i].getCells()[7].getValue() 	=== itemCol[x].U_SEI_MONTO &&
						items[i].getCells()[8].getValue() 	=== itemCol[x].U_SEI_DESCRIP ){
							
							itemCol[x].U_SEI_NRODOC 	= items[x].getCells()[1].getValue();
							itemCol[x].U_SEI_FECHADOC 	= items[x].getCells()[2].getValue();
							itemCol[x].U_SEI_TIPODOC 	= items[x].getCells()[3].getValue();
							itemCol[x].U_SEI_RUT 		= items[x].getCells()[4].getValue();
							itemCol[x].U_SEI_NOMP 		= items[x].getCells()[5].getValue();
							itemCol[x].U_SEI_FACTASO 	= items[x].getCells()[6].getValue();							
							itemCol[x].U_SEI_MONTO 		= items[x].getCells()[7].getValue();
							itemCol[x].U_SEI_DESCRIP 	= items[x].getCells()[8].getValue();
							itemCol[x].U_SEI_CTASUPERE 	= items[x].getCells()[9].getText();
							itemCol[x].U_SEI_CTAR	 	= items[x].getCells()[10].getText();
						}
					}
					sumaMonto = sumaMonto + parseInt(items[i].getCells()[7].getValue());
				}
				
				itemRen.SEI_RENDETCollection.push({
									"U_SEI_NRODOC": "", 
									"U_SEI_FECHADOC": "", 
									"U_SEI_RUT": "", 
									"U_SEI_NOMP": "", 
									"U_SEI_FACTASO": "", 
									"U_SEI_TIPODOC": "", 
									"U_SEI_MONTO": "", 
									"U_SEI_DESCRIP": "",
									"U_SEI_CTASUPERE": U_SEI_CTASUPERE,
									"U_SEI_CTAR": U_SEI_RESULT
									});
				
				var oTable = this.getView().byId("idItemsTable");
			    var oModel = oTable.getModel();
			    var oData = oModel.getData();
				oData.SEI_RENDETCollection = itemRen.SEI_RENDETCollection;
				oModel.setData(oData);
				oModel.refresh();
				//habilita celdas para modificacion
				var porcentaje = porcentajeUsado(this.montoEntregado, this.saldoPendiente, sumaMonto);				
				var saldo = this.saldoPendiente - sumaMonto;
				
				if(porcentaje <= 50){
					new sap.m.MessageBox.show("Ha usado Mas del 50%. Te queda aun el " + porcentaje + "%. por un Monto" + saldo, {
						icon: sap.m.MessageBox.Icon.WARNING,
						title: "Informacion Creacion Rendiciones"
					});
				}
				this.handleHabilBtn();
			}
		}
	},
	
	/*
	 * Funcion encargada de actualiza los campos de la tabla
	 */
	handleUpdateBtn: function(oEvent){
		if(this.validaEstado()) {
			this.validaCampos();
			if(this.mensaje){
				new sap.m.MessageBox.show(this.mensaje, {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: "Informacion",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			} else if(this.mensaje === null){
				
				var sumaMonto = 0;
				var rendicion = {SEI_RENDETCollection:[]};
				var docEntry = this.byId("txtCabDocEntry").getText();
				var items = this.getView().byId("idItemsTable").getItems();
				var loRouter = sap.ui.core.UIComponent.getRouterFor(this);
										
				for(var i = 0; i < items.length; i++){
					var rendicionItems = {};
					if(items[i].getCells()[0].getText()){
						rendicionItems = {
								"LineId": 			items[i].getCells()[0].getText(),
								"U_SEI_NRODOC": 	items[i].getCells()[1].getValue(),
								"U_SEI_FECHADOC": 	items[i].getCells()[2].getValue(),
								"U_SEI_TIPODOC":	items[i].getCells()[3].getValue(),
								"U_SEI_SUPERIN":	items[i].getCells()[3].getSelectedKey(),
								"U_SEI_RUT":		items[i].getCells()[4].getValue(),
								"U_SEI_NOMP":		items[i].getCells()[5].getValue(),
								"U_SEI_FACTASO":	items[i].getCells()[6].getValue(),								
								"U_SEI_MONTO":		items[i].getCells()[7].getValue(),
								"U_SEI_DESCRIP":	items[i].getCells()[8].getValue(),
								"U_SEI_CTASUPERE": 	items[i].getCells()[9].getText(),
								"U_SEI_CTAR": 		items[i].getCells()[10].getText()
						}
						rendicion.SEI_RENDETCollection.push(rendicionItems);
					}
					
					if(!items[i].getCells()[0].getText()){
						rendicionItems = {
								"U_SEI_NRODOC": 	items[i].getCells()[1].getValue(),
								"U_SEI_FECHADOC": 	items[i].getCells()[2].getValue(),
								"U_SEI_TIPODOC":	items[i].getCells()[3].getValue(),
								"U_SEI_SUPERIN":	items[i].getCells()[3].getSelectedKey(),
								"U_SEI_RUT":		items[i].getCells()[4].getValue(),
								"U_SEI_NOMP":		items[i].getCells()[5].getValue(),
								"U_SEI_FACTASO":	items[i].getCells()[6].getValue(),
								"U_SEI_MONTO":		items[i].getCells()[7].getValue(),
								"U_SEI_DESCRIP":	items[i].getCells()[8].getValue(),
								"U_SEI_CTASUPERE": 	items[i].getCells()[9].getText(),
								"U_SEI_CTAR": 		items[i].getCells()[10].getText()
						}
						rendicion.SEI_RENDETCollection.push(rendicionItems);
					}
					sumaMonto = sumaMonto + parseInt(items[i].getCells()[7].getValue());
				}
				
				var porcentaje = porcentajeUsado(this.montoEntregado, this.saldoPendiente, sumaMonto);				
				var saldo = this.saldoPendiente - sumaMonto;
				
				if(porcentaje <= 50){
					new sap.m.MessageBox.show("Ha usado Mas del 50%. Te queda aun el " + porcentaje + "%. por un Monto" + saldo, {
						icon: sap.m.MessageBox.Icon.WARNING,
						title: "Informacion Creacion Rendiciones"
					});
				}
				
				if(!updateRendiciones(docEntry, rendicion)){
					this.getView().byId("idItemsTable").setMode("None");
					this.desabilitaBotones();
					loRouter.navTo("RendicionMaster");
				}
			}
		}
	},
	
	/*
	 * Funcion encargada de Eliminar los items seleccionados 
	 */
	handleEliminBtn: function(oEvent){
		if(this.validaEstado()) {
			var oTable = this.getView().byId("idItemsTable");
			var items = oTable.getItems();
		    var data = oTable.getModel();
		    var selRowCount = oTable.getSelectedItems().length;
		    var docEntry = this.getView().byId("text1").getText();
		    var loRouter = sap.ui.core.UIComponent.getRouterFor(this);
		    var rowNum;
		    
		    if(selRowCount > 0){
		    	if(selRowCount < items.length){
		    		var contRev = selRowCount;
				    for (var i = selRowCount; i > 0; i--) {
				    	var cadena = oTable.getSelectedItems()[i-1].getCells()[0].sId;
						var largo = cadena.length;
				    	
				    	rowNum = oTable.getSelectedItems()[i-1].getCells()[0].sId.substring(largo, largo-2).replace("-", "");
						var oData = data.getData();
						oData.value[this.path].SEI_RENDETCollection.splice(rowNum, 1);
						data.setData(oData);
						data.refresh();
				    }
				    
				    var rendicion = {SEI_RENDETCollection:[]};
					var SEI_RENDETCollection = {};
					var oTable = this.getView().byId("idItemsTable");
					var items = oTable.getItems();
	
					for(var item = 0; item < items.length; item++){
						if(items[item].getCells()[0].getText()){
							SEI_RENDETCollection = { "LineId": 			items[item].getCells()[0].getText() }
							rendicion.SEI_RENDETCollection.push(SEI_RENDETCollection);
						}
						if (!items[item].getCells()[0].getText()) {
							SEI_RENDETCollection = {
								"U_SEI_NRODOC": 	items[item].getCells()[1].getValue(),
								"U_SEI_FECHADOC": 	items[item].getCells()[2].getValue(),
								"U_SEI_TIPODOC":	items[item].getCells()[3].getValue(),
								"U_SEI_SUPERIN":	items[item].getCells()[3].getSelectedKey(),
								"U_SEI_RUT":		items[item].getCells()[4].getValue(),
								"U_SEI_NOMP":		items[item].getCells()[5].getValue(),
								"U_SEI_FACTASO":	items[item].getCells()[6].getValue(),
								"U_SEI_MONTO":		items[item].getCells()[7].getValue(),
								"U_SEI_DESCRIP":	items[item].getCells()[8].getValue(),
								"U_SEI_CTASUPERE": 	items[item].getCells()[9].getText(),
								"U_SEI_CTAR": 		items[i].getCells()[10].getText()
							}
							rendicion.SEI_RENDETCollection.push(SEI_RENDETCollection);
						}
					}
					if(!delLineRendiciones(docEntry, rendicion)){
						this.getView().byId("idItemsTable").setMode("None");
						this.desabilitaBotones;
						loRouter.navTo("RendicionMaster");
					}
		    	} else {
		    		new sap.m.MessageBox.show("Se Debe Dejar al menos una Linea en la Solicitud", {
						icon: sap.m.MessageBox.Icon.WARNING,
						title: "Informacion Crear Rendicion",
						onClose: function(oAction) {					
				    		sap.ui.core.BusyIndicator.hide();
						}
					});
		    	}
			} else {
				new sap.m.MessageBox.show("No se ha seleccionado lineas a eliminar", {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: "Informacion Crear Rendicion",
					onClose: function(oAction) {					
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
			}
		}
	},
	
	// A continuación se muestra la definición de la función para 
	// llamar a la capa de servicio para "Aprobar" un pedido de cliente "Pendiente"
	handleAceptBtn: function(oEvent) {
		sap.ui.core.BusyIndicator.show(0);
		
		var docEntry = this.getView().byId("text1");
		var docStatusTxt = this.getView().byId("text2");
		
		if(docStatusTxt.getText() === "Pendiente"){
			docStatusTxt.setText("Aprobado");		
			var body = { "U_SEI_ESTADO": "Aprobado", "U_SEI_ENVIO": "False"};
			
			updateRendiciones(docEntry.getText(), body);
			new sap.m.MessageBox.show("El documento Cambio de estado a Aprobado", {
				icon: sap.m.MessageBox.Icon.SUCCESS,
				title: "Informacion Crear Rendicion",
				onClose: function(oAction) {
					this.getView().byId("btnEnviar").setEnabled(false);
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		} else if (docStatusTxt.getText() === "Aprobado"){
			new sap.m.MessageBox.show("El documento ya se encuentra Aprobado", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Informacion Crear Rendicion",
				onClose: function(oAction){
					sap.ui.core.BusyIndicator.hide();
				}
			});
		} else {
			new sap.m.MessageBox.show("El documento no se encuentra Pendiente", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Informacion Crear Rendicion",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		}
	},
	
	// A continuación se muestra la definición de la función para 
	// llamar a la capa de servicio para "Rechazar" un pedido de cliente "Pendiente"
	handleCancelBtn: function(oEvent) {		
		var docEntry = this.getView().byId("text1");
		var docStatusTxt = this.getView().byId("text2");
		var btnHabi = this.getView().byId("btnHabil");
		var btnEnviar = this.getView().byId("btnEnviar");
		var btnApro = this.getView().byId("btnAprobar");
		var btnRech = this.getView().byId("btnRechazar");
		var txtMotRec = this.getView().byId("txtMotivoRechazo");
		if(!txtMotRec.getEnabled())
			txtMotRec.setEnabled(true);
		
//		<BusyIndicator text="... something happens"	class="sapUiTinyMarginBottom" />
//		"sap.m.BusyIndicator"
		
		if(docStatusTxt.getText() === "Pendiente"){
			
			if(txtMotRec.getValue()){				
				docStatusTxt.setText("Rechazada");		
				var body = { "U_SEI_ESTADO": "Rechazada", "U_SEI_ENVIO": "True", "U_SEI_MOT_RECH": txtMotRec.getValue() };
				
				updateRendiciones(docEntry.getText(), body);
				new sap.m.MessageBox.show("El documento Cambio de estado a Rechazado", {
					icon: sap.m.MessageBox.Icon.SUCCESS,
					title: "Informacion Crear Rendicion",
					onClose: function(oAction) {
						btnHabi.setEnabled(true);
						btnEnviar.setEnabled(true);
						btnApro.setEnabled(false);
						btnRech.setEnabled(false);
						txtMotRec.setEnabled(false);
			    		sap.ui.core.BusyIndicator.hide();
					}
				});
				
			} else {
				new sap.m.MessageBox.show("Debe Ingresar Motivo del Rechazo antes de Rechazarlo!", {
					icon: sap.m.MessageBox.Icon.WARNING,
					title: "Informacion Crear Rendicion"
				});
			}
			
		} else {
			new sap.m.MessageBox.show("El documento no se encuentra Pendiente!", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Informacion Crear Rendicion",
				onClose: function(oAction) {
		    		sap.ui.core.BusyIndicator.hide();
				}
			});
		}		
	},
	
	handleEnviarBtn: function(){
		var docEntry = this.getView().byId("text1");
		var docStatusTxt = this.getView().byId("text2");
		var btnHabi = this.getView().byId("btnHabil");
		var btnEnviar = this.getView().byId("btnEnviar");
		var btnApro = this.getView().byId("btnAprobar");
		var btnRech = this.getView().byId("btnRechazar");
		
		docStatusTxt.setText("Pendiente");		
		var body = { "U_SEI_ESTADO": "Pendiente", "U_SEI_ENVIO": "False", "U_SEI_MOT_RECH": "" };
		
		updateRendiciones(docEntry.getText(), body);
		this.deshabilitaLineas();
		this.desabilitaBotones();
		new sap.m.MessageBox.show("El documento Cambio de estado a Pendiente", {
			icon: sap.m.MessageBox.Icon.SUCCESS,
			title: "Informacion",
			onClose: function(oAction) {
				btnHabi.setEnabled(false);
				btnEnviar.setEnabled(false);
				btnApro.setEnabled(true);
				btnRech.setEnabled(true);
	    		sap.ui.core.BusyIndicator.hide();
			}
		});
	},
	/*
	 * Funcion encargada de Volver a la pagina anterior "RendicionMaster"
	 */
	handleNavButton: function(oEvent) {
		var sPreviousHash = sap.ui.core.routing.History.getInstance().getPreviousHash();
		this.getView().byId("idItemsTable").setMode("None");
		this.desabilitaBotones();
		
		//The history contains a previous entry
		if (sPreviousHash !== undefined) {
			window.history.go(-1);
		} else {
			// There is no history!
			// replace the current hash with page 1 (will not add an history entry)
			this.getOwnerComponent().getRouter().navTo("RendicionMaster", oItems, true);
		}
	},
	
	/*
	 * Funcion Encargada de habilitar los botones de modificar y eliminar
	 * ademas de poner un checkbox a cada linea
	 */
	handleHabilBtn:function(oEvent) {
		if(this.validaEstado()){
			fechaDoc = this.getView().byId("header").getNumber();
			fecahPag = this.fechaPagoEntregado;
			
			var difFecha = diffFecha(new Date(fecahPag), new Date(fechaDoc));
			
			if(difFecha >= 0 && difFecha <= 30){
				var items = this.getView().byId("idItemsTable").getItems();
										
				for(var item = 0; item < items.length; item++){
					for(var cell = 0; cell < items[item].getCells().length-1; cell++){
						if(cell !== 0 && cell !== 9)
							items[item].getCells()[cell].setEnabled(true);
					}
				}
				this.habilitaLineas(true);
				this.getView().byId("idItemsTable").setMode("MultiSelect");
				this.habilitaBotones();
				sap.ui.core.BusyIndicator.hide()
			} else {
				new sap.m.MessageBox.show(
						"La Rendicion contiene mas de 30 dias y no puede ser Modificada.", {
	               	icon: sap.m.MessageBox.Icon.WARNING,
	              	title: "Informacion Creacion Rendiciones."
	            });
			}
		}
	},
	
	handleLoadItems: function(oEvent){
		var selKey = oEvent.getParameter("selectedItem").getText();
		var selText = oEvent.getParameter("selectedItem").getKey();
		
		if(selText === "Bolh" || selText === "Bolhe" || selText === "Fac" || selText === "Facel" || selText === "Facex" || selText === "Face" || selText === "Notacre" || selText === "Notadeb"){
			this.habilitaLineas(true);
		} else {
			this.habilitaLineas(false);
		}
	},

	handleRutProveedor: function(oControlEvent) {
		var oValidaControl = this.validaComboBox(oControlEvent);
		
		if(oValidaControl.getValueState() === "None"){
			var selKey = oControlEvent.getParameter("selectedItem").getKey();
			var selText = oControlEvent.getParameter("selectedItem").getText();
			
			var cadena = oControlEvent.getSource().getBindingContext().sPath;
			var largo = cadena.length;
			cadena = cadena.substring(largo, largo-2);
			var linea = cadena.replace("/", "");
			
			var items = this.getView().byId("idItemsTable").getItems();
			items[linea].getCells()[5].setValue(selKey);
		}
	},
	
	handleNomProveedor: function(oControlEvent) {
		var oValidaControl = this.validaComboBox(oControlEvent);
		
		if(oValidaControl.getValueState() === "None"){
			var selKey = oControlEvent.getParameter("selectedItem").getKey();
			var selText = oControlEvent.getParameter("selectedItem").getText();
			
			var cadena = oControlEvent.getSource().getBindingContext().sPath;
			var largo = cadena.length;
			cadena = cadena.substring(largo, largo-2);
			var linea = cadena.replace("/", "");
			
			var items = this.getView().byId("idItemsTable").getItems();				
			items[linea].getCells()[4].setValue(selKey);
		}
	},
	
	validaEstado: function() {
		var docStatusTxt = this.getView().byId("text2").getText();
		if(docStatusTxt === "Pendiente" || docStatusTxt === "Rechazada"){
			return true;
		} else {
			new sap.m.MessageBox.show("El documento ya se encuentra Aprobado!", {
				icon: sap.m.MessageBox.Icon.WARNING,
				title: "Informacion",
				onClose: function(oAction) {					
		    		sap.ui.core.BusyIndicator.hide();
		    		return false;
				}
			});
		}
	},
	
	validaComboBox: function(oEvent){
		var oValidatedComboBox = oEvent.getSource(),
	    sSelectedKey = oValidatedComboBox.getSelectedKey(),
	    sValue = oValidatedComboBox.getValue();
	
		if(!sSelectedKey && sValue) {
			oValidatedComboBox.setValueState("Error");
			oValidatedComboBox.setValueStateText("Porfavor Ingresa un valor Valido!");
		} else {
			oValidatedComboBox.setValueState("None");
		}
		return oValidatedComboBox;
	},
	
	validaCampos: function(){
		this.mensaje = null;
		var items = this.getView().byId("idItemsTable").getItems();		
		for (var item = 0; item < items.length; item++) {		
			var linea = item + 1;
			for(var cell = 0; cell < items[item].getCells().length-1; cell++){
				if(cell !== 0 && cell !== 9 && cell !== 10){
					if(!items[item].getCells()[cell].getValue()){
						switch(cell){
						case 1:
							this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Ingresar Numero de Documento. En la linea: " + linea : "- Debe Ingresar Numero de Documento. En la linea: " + linea);
							break;
						case 2:
							this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Seleccionar una Fecha. En la linea: " + linea : "- Debe Seleccionar una Fecha. En la liena: " + linea);
							break;
						case 3:
							this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Seleccionar el Tipo de Documento. En la linea: " + linea : "- Debe Seleccionar el Tipo de Documento. en la linea:" + linea);
							break;
						case 4:
							if(items[item].getCells()[cell].getEnabled())
								this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Seleccionar un Rut del Proveedor y/o Nombre del Proveedor. En la linea: " + linea : "- Debe Seleccionar un Rut del Proveedor y/o Nombre del Proveedor. En la Linea: " + linea);
							break;
						case 5:
							if(items[item].getCells()[cell].getEnabled())
								this.mensaje = (this.mensaje !== null ? this.mensaje + "\n  - Debe Seleccionar un Nombre del Proveedor y/o Rut del Proveedor. En la linea: " + linea : "- Debe Seleccionar un Nombre del Proveedor y/o Rut del Proveedor. En la Linea: " + linea);
							break;
						case 6:
							if(items[item].getCells()[cell].getEnabled())
								this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Ingresar la Factura Asociada. En la linea: " + linea : "- Debe Ingresar la Factura Asociada. En la linea: " + linea);
							break;
						case 7:
							this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Ingresar el Monto. En la linea: " + linea : "- Debe Ingresar el Monto. En la linea: " + linea);
							break;
						case 8:
							this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - Debe Seleccionar la Descripcion. En la linea: " + linea : "- Debe Seleccionar la Descripcion. En la linea: " + linea);
							break;
						}
						
					}
				}
			}
		}
	},
	
	validaEstadoEnvio: function(envio, estado){
		var view = this.getView();
		var btnHabi = view.byId("btnHabil");
		var btnApro = view.byId("btnAprobar");
		var btnRech = view.byId("btnRechazar");
		var btnEnvi = view.byId("btnEnviar");
		
		if(estado === "Aprobado"){
			btnApro.setEnabled(false);
			btnRech.setEnabled(false);
			btnEnvi.setEnabled(false);
		} else if(envio === "False" && estado === "Pendiente"){
			btnApro.setEnabled(true);
			btnRech.setEnabled(true);
			btnHabi.setEnabled(false);
			btnEnvi.setEnabled(false);
		} else if(envio === "True" && estado === "Rechazada"){
			btnApro.setEnabled(false);
			btnRech.setEnabled(false);
			btnEnvi.setEnabled(true);
		} else if(envio === "True" && estado === "Pendiente"){
			btnApro.setEnabled(false);
			btnRech.setEnabled(false);
			btnEnvi.setEnabled(true);
		}
	},
	
	desabilitaBotones: function(){
		var btnAr = this.getView().byId("btnRendicion");
		var btnUp = this.getView().byId("btnUpdate");
		var btnEl = this.getView().byId("btnElimin");
		btnAr.setVisible(false);
		btnUp.setEnabled(false);
		btnEl.setEnabled(false);
	},
	
	habilitaBotones: function(){
		var btnAr = this.getView().byId("btnRendicion");
		var btnUp = this.getView().byId("btnUpdate");
		var btnEl = this.getView().byId("btnElimin");
		btnAr.setVisible(true);
		btnUp.setEnabled(true);
		btnEl.setEnabled(true);
	},
	
	habilitaLineas: function(estado){
		var items = this.getView().byId("idItemsTable").getItems();
		
		for(var item = 0; item < items.length; item++){
			var selText = items[item].getCells()[3].getSelectedKey();
			
			if(selText === "Bolh" || selText === "Bolhe" || selText === "Fac" || selText === "Facel" || selText === "Facex" || selText === "Face" || selText === "Notacre" || selText === "Notadeb"){
				items[item].getCells()[4].setEnabled(estado);
				items[item].getCells()[5].setEnabled(estado);
				if(selText === "Notacre" || selText === "Notadeb"){
					items[item].getCells()[6].setEnabled(estado);
				} else {
					items[item].getCells()[6].setEnabled(false);
				};
			} else {
				items[item].getCells()[4].setEnabled(false);
				items[item].getCells()[5].setEnabled(false);
				items[item].getCells()[6].setEnabled(false);
			}
		}
	},
	
	deshabilitaLineas: function(){
		var items = this.getView().byId("idItemsTable").getItems();
		
		for(var item = 0; item < items.length; item++){
			items[item].getCells()[1].setEnabled(false);
			items[item].getCells()[2].setEnabled(false);
			items[item].getCells()[3].setEnabled(false);
			items[item].getCells()[4].setEnabled(false);
			items[item].getCells()[5].setEnabled(false);
			items[item].getCells()[6].setEnabled(false);
			items[item].getCells()[7].setEnabled(false);
			items[item].getCells()[8].setEnabled(false);
		}
	},
	
	cargaCombobox: function(oControlEvent, idCbx) {
		if(idCbx === null)
			idCbx = "cbxFuncionario";
		
		switch(idCbx){
		case "tipoFinanciamientos":
			getAsync('tipoFinanciamientos');
			oRendicionJSONData.tipoFinanciamientos = JSON.parse(sessionStorage.getItem('tipoFinanciamientos')).value;
			break;
		case "rutFuncionarios":
			getAsync('rutFuncionarios');
			oRendicionJSONData.rutFuncionarios = JSON.parse(sessionStorage.getItem('rutFuncionarios')).value;
			break;
		case"departamentos":
			getAsync('departamentos');
			oRendicionJSONData.departamentos = JSON.parse(sessionStorage.getItem('departamentos')).value;
			break;
		case"tipoDocumentos":
			getAsync('tipoDocumentos');
			oRendicionJSONData.tipoDocumentos = JSON.parse(sessionStorage.getItem('tipoDocumentos')).value;
			break;
		case"rutProveedores":
			getAsync('rutProveedores');
			oRendicionJSONData.rutProveedores = JSON.parse(sessionStorage.getItem('rutProveedores')).value;
			break;
		case"tipoRendicion":
			getAsync('tipoRendicion');
			oRendicionJSONData.tipoRendicion = JSON.parse(sessionStorage.getItem('tipoRendicion')).value;
			
			for(var i = 0; i < JSON.parse(sessionStorage.getItem('tipoRendicion')).value.length; i++){
				if(JSON.parse(sessionStorage.getItem('tipoRendicion')).value[i].U_SEI_TRENDI === "1"){
					this.oRendicion1.value.push(JSON.parse(sessionStorage.getItem('tipoRendicion')).value[i]);
				} else if(JSON.parse(sessionStorage.getItem('tipoRendicion')).value[i].U_SEI_TRENDI === "2"){
					this.oRendicion2.value.push(JSON.parse(sessionStorage.getItem('tipoRendicion')).value[i]);
				}
			}
			break;
		case"saldoAsignado":
			getAsync('saldoAsignado');
			oRendicionJSONData.saldoAsignado = JSON.parse(sessionStorage.getItem('saldoAsignado')).value;
			break;
		}
	},
	
	handlePago: function(oEvent){
		var selKey = this.nroPago;
		
		if(!oRendicionJSONData.saldoAsignado || oRendicionJSONData.saldoAsignado === undefined)
			this.cargaCombobox(null,"saldoAsignado");
		
		for(var i = 0; i < oRendicionJSONData.saldoAsignado.length; i++){
			var saldoAsignado = oRendicionJSONData.saldoAsignado[i];
			
			if(selKey == saldoAsignado.NroPago){
				this.nroPago			= saldoAsignado.NroPago;
				this.codSocio 			= saldoAsignado.CodSocio;
				this.montoEntregado 	= saldoAsignado.MontoEntregado;
				this.saldoPendiente 	= saldoAsignado.SaldoPendiente;
				this.fechaPagoEntregado = saldoAsignado.FechaPagoEntregado;
			}
		}
	},
	
	handleFecha: function(){
		var cbxFechaRendicion = this.getView().byId("cbxFechaRendicion").getValue();
		if(!cbxFechaRendicion){
			this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - No se a Seleccionado Fecha de Rendicion." : "- No se a Seleccionado Fecha de Rendicion.");
		} else if(cbxFechaRendicion && this.fechaPagoEntregado){
			var difFecha = diffFecha(new Date(this.fechaPagoEntregado), cbxFechaRendicion);
			if(difFecha === "mayor"){
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - La Fecha Seleccionada supera los 30 dias para Rendir." : "- La Fecha Seleccionada supera los 30 dias para Rendir.");
			} else if(difFecha === "menor"){
				this.mensaje = (this.mensaje !== null ? this.mensaje + "\n - La Fecha Seleccionada esta fuera de fecha para Rendir." : "- La Fecha Seleccionada esta fuera de fecha para Rendir.");
			}
		}
		
	},
	
	handleDescripcion: function(oControlEvent){			
		var oValidaControl = this.validaComboBox(oControlEvent);
		
		if(oValidaControl.getValueState() === "None"){
			var selKey = oControlEvent.getParameter("selectedItem").getKey();
			var selText = oControlEvent.getParameter("selectedItem").getText();
			
			var cadena = oControlEvent.getSource().getBindingContext().sPath;
			var largo = cadena.length;
			cadena = cadena.substring(largo, largo-2);
			var linea = cadena.replace("/", "");
			
			var items = this.getView().byId("idItemsTable").getItems();
			
			var cuentas = JSON.parse(sessionStorage.getItem('tipoRendicion'));
			for(var i = 0; i < cuentas.value.length; i++){
				var cuenta = cuentas.value[i];
				
				if(cuenta.Name === selText){
					items[linea].getCells()[9].setText(cuenta.U_SEI_CSUPERE);
					items[linea].getCells()[10].setText(cuenta.U_SEI_CUENTAR);
					break;
				}
			}
		}
	},
	
	validaComboBox: function(oEvent){
		var oValidatedComboBox = oEvent.getSource(),
	    sSelectedKey = oValidatedComboBox.getSelectedKey(),
	    sValue = oValidatedComboBox.getValue();
	
		if(!sSelectedKey && sValue) {
			oValidatedComboBox.setValueState("Error");
			oValidatedComboBox.setValueStateText("Porfavor Ingresa un valor Valido!");
		} else {
			oValidatedComboBox.setValueState("None");
		}
		return oValidatedComboBox;
	},
	
	delay : function(componente) {
		return new Promise(resolve => {
		    setTimeout(() => {
		    	componente.setBusy(false);
		    }, 5000);
		 });
	},
	
	onExit: function() {
		if (this._oActionSheet) {
			this._oActionSheet.destroy();
			this._oActionSheet = null;
		}
		//this.oProductsModel.destroy();
	},
	
	getSelectedRowContext: function(sTableId, fnCallback) {
		var oTable = this.byId(sTableId);
		var iSelectedIndex = oTable.getSelectedIndex();

		if (iSelectedIndex === -1) {
			MessageToast.show("Please select a row!");
			return;
		}

		var oSelectedContext = oTable.getContextByIndex(iSelectedIndex);
		if (oSelectedContext && fnCallback) {
			fnCallback.call(this, oSelectedContext, iSelectedIndex, oTable);
		}

		return oSelectedContext;
	}
});