sap.ui.define([
	"sap/m/MessageBox",
	"sap/ui/core/mvc/Controller"
], function(MessageBox, Controller) {
	"use strict"
	
	return Controller.extend("SeidorB1UI5.controller.App", {
		
	});
});

//sap.ui.controller("SeidorB1UI5.controller.App", {
//	onInit : function() {
//		
//	},
//	
//	onCompleteCall1: function(result)
//	{   
//		document.cookie = result.cookies[0].value;
//	    B1HSL_SessionID = document.cookie;
//	    alert(B1HSL_SessionID);
//	
//	    // MODIFICAR //
//		// Es posible que deba ajustar el parámetro CMD en función del objeto comercial que elija "//
//		// En el siguiente ejemplo, recuperarás datos maestros de artículos //
//		// MODIFICAR //
//	    var xsjsUrl = "B1SLLogic?cmd=Items&sessionID=" + B1HSL_SessionID;
//	    
//		jQuery.ajax({
//			type: "GET",
//			xhrFields: { withCredentials: true },
//		    url: xsjsUrl,
//		    dataType: "JSON",
//		    crossDomain: true, 
//		    success: function (result) {
//		    	ItemsJSONData = JSON.parse(result);
//		    	alert(ItemsJSONData);
//		    },
//		    error: this.onErrorCall
//		});
//	},
//	
//	onErrorCall: function(jqXHR, textStatus, errorThrown) {
//		sap.ui.commons.MessageBox.show(jqXHR.responseText,
//				"ERROR",   
//		        "Error in calling Service"
//		);    
//		return;   
//	}
//});