sap.ui.define([
	'jquery.sap.global'
	,'sap/ui/core/UIComponent'
	,'sap/ui/model/json/JSONModel'
],
	function(Jquery, UIComponent, JSONModel) {
			"use strict";
			return UIComponent.extend("SeidorB1UI5.Component", {
				
				metadata: { manifest: "json"},
				
				init: function() {					
					UIComponent.prototype.init.apply(this, arguments);
					this.getRouter().initialize();
					
					//Se setean los Productos de la demo de este ejemplo
//					var oProductoModel;
//					oProductoModel = new JSONModel();
//					oProductoModel.setData(oProductsModel);
//					
//					oProductoModel.setSizeLimit(1000);
//					this.setModel(oProductsModel, 'value');
					
					//FUNCIONALIDAD PARA QUE COMPORTAMIENTO SEA EL DE UN BOTON
					var deviceModel = new sap.ui.model.json.JSONModel({
						isTouch 		: sap.ui.Device.support.touch,
						isNoTouch 		: !sap.ui.Device.support.touch,
						isPhone 		: jQuery.device.is.phone,
						isNoPhone 		: !jQuery.device.is.phone,
						listMode 		: (jQuery.device.is.phone) ? "None" : "SingleSelectMaster",
						listItemType 	: (jQuery.device.is.phone) ? "Active" : "Inactive"
					});
					deviceModel.setDefaultBindingMode("OneWay");
					this.setModel(deviceModel, "device");
				}
	});
});