var mensajeError;

/**
 * Funcion encargada de realizar la llamada asyncrona segun la 
 * informacion indicada en el parametro de entrada "tipo" 
 * para obtener los datos contenidos en SAP
 * @param parametro de tipo string que contiene a donde se dirigira la llamada
 * @returns retorna el valor de los obtenido en sap como un objeto JSON
 */
function getAsync(tipo) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		var filter, url;
		switch(tipo) {
			case 'departamentos':
				filter = "InWhichDimension eq 3";
				url = "B1SLLogic?cmd=Get&actionUri=DistributionRules&select=FactorCode,FactorDescription&filter=" + filter + "&sessionID=" + cookieJSESSIONID;
				callAsync(url, tipo, false);
				break;
			case 'tipoFinanciamientos':
				filter = "U_SEI_DIME1 eq '" + oDatosUsuario.cod_suc + "'";
				url = "B1SLLogic?cmd=Get&actionUri=U_SEI_TIPOF&filter=" + filter + "&sessionID=" + cookieJSESSIONID;
				callAsync(url, tipo, false);
				break;
			case 'location':
				filter = "Name eq '" + oDatosUsuario.cod_suc + "'";
				url = "B1SLLogic?cmd=Get&actionUri=U_SEI_ESTRUFISICA&filter=" + filter + "&sessionID=" + cookieJSESSIONID;
				callAsync(url, tipo, false);
				break;
			case 'location2':
				url = "B1SLLogic?cmd=Get&actionUri=U_SEI_ESTRUFISICA&sessionID=" + cookieJSESSIONID;
				callAsync(url, tipo, false);
				break;
			case 'items':
				filter = "PurchaseItem eq 'Y' and Valid eq 'Y' and Frozen eq 'N' and ((VirtualAssetItem eq 'tYES' and ItemType eq 'F') or (VirtualAssetItem eq 'tNO' and ItemType ne 'F'))";
				url = "B1SLLogic?cmd=Get&actionUri=Items&select=ItemCode,ItemName,ItemsGroupCode,VirtualAssetItem,InventoryItem,Properties1&filter=" + filter + "&sessionID=" + cookieJSESSIONID;
				callAsync(url , tipo, false);
				break;
			case 'tipoDocumentos':
				url = "B1SLLogic?cmd=Get&actionUri=U_SEI_SII&sessionID=" + cookieJSESSIONID;
				callAsync(url, tipo, false);
				break;
			case 'serviceEspecialidades':
				url = "B1SLLogic?cmd=Get&actionUri=U_SEI_ESPECIALIDADES&select=Code,Name&filter=U_SEI_VISIBLE eq 'SI'&sessionID=" + cookieJSESSIONID;
				callAsync(url, tipo, false);
				break;
			case 'rutProveedores':
				filter = "GroupCode eq 101";
				url = "B1SLLogic?cmd=Get&actionUri=BusinessPartners&select=CardCode,CardName&filter=" + filter + "&sessionID=" + cookieJSESSIONID;
				callAsync(url , tipo, false);
				break;
			case 'sociosTarjeta':
				url = "B1SLLogic?cmd=Get&actionUri=BusinessPartners('"+ oDatosUsuario.cole_cod +"')&select=CardCode,CardName&sessionID=" + cookieJSESSIONID;
				callAsync(url , tipo, false);
				break;
			case 'rutFuncionarios':
				//filter = "GroupCode eq 103";
				filter = "GroupCode eq 103 and Valid eq 'Y' and U_SEI_DIM1 eq '" + oDatosUsuario.cod_suc + "'" ;
				url = "B1SLLogic?cmd=Get&actionUri=BusinessPartners&select=CardCode,CardName&filter=" + filter + "&sessionID=" + cookieJSESSIONID;
				callAsync(url , tipo, false);
				break;
			case 'activities':
				filter = "ParentObjectId eq " + serviceCallId;
				url = "B1SLLogic?cmd=Get&actionUri=Activities&filter=" + filter + "&orderby=ActivityCode desc&sessionID=" + cookieJSESSIONID;
				callAsync(url , tipo, false);
				break;
			case 'tipoRendicion':
				url = "B1SLLogic?cmd=Get&actionUri=U_SEI_CUENTAPOR&select=Name,U_SEI_CUENTAR,U_SEI_CSUPERE,U_SEI_TFONDOSG,U_SEI_TSEP,U_SEI_TPIE,U_SEI_TRENDI&sessionID=" + cookieJSESSIONID;
				callAsync(url , tipo, false);
				break;
			case 'saldoAsignado':
				url = "B1SLLogic?cmd=Get&actionUri=SALDOASIGNADO&select=id__,CodSocio,NroPago,MontoEntregado,SaldoPendiente,FechaPagoEntregado,Comentarios,Comment&sessionID=" + cookieJSESSIONID;
				callAsync(url , tipo, false);
				break;
			case 'presupuestos':
				select = "ValidValuesMD";
				url = "B1SLLogic?cmd=Get&actionUri=UserFieldsMD(TableName='OINS',FieldID=13)&select=" + select + "&sessionID=" + cookieJSESSIONID;
				callAsync(url , tipo, false);
				break;
			case 'numerosDeSerie':
				select = "EquipmentCardNum,CustomerCode,CustomerName,ManufacturerSerialNum,InstallLocation,InternalSerialNum,ItemCode,ItemDescription,Street,City,County,CountryCode,StateCode,StatusOfSerialNumber,U_SEI_Path_number,U_SEI_Modelo,U_SEI_Marca,U_SEI_Cod_Barra_Interno,U_SEI_Ram,U_SEI_CapHHD,U_SEI_Procesador,U_SEI_Version_SO,U_SEI_IDProducto_SO,U_SEI_key_SO,U_SEI_Version_Office,U_SEI_ID_Office,U_SEI_Garantia,U_SEI_Presupuesto,U_SEI_Fecha_Compra,U_SEI_IP,U_SEI_Nro_Licencia_Office,U_SEI_Factura,U_SEI_FAC,U_SEI_RUTPROV,U_SEI_T_EQUIPO,U_SEI_UBICACION_PRINCIPAL,U_SEI_ANTIVIRUS,U_SEI_MOTIVO_BAJA";
//				select = "EquipmentCardNum,CustomerCode,CustomerName,ManufacturerSerialNum,InstallLocation,InternalSerialNum,ItemCode,ItemDescription,Street,City,County,CountryCode,StateCode,StatusOfSerialNumber,U_SEI_Path_number,U_SEI_Modelo,U_SEI_Marca,U_SEI_Cod_Barra_Interno,U_SEI_Ram,U_SEI_CapHHD,U_SEI_Procesador,U_SEI_Version_SO,U_SEI_IDProducto_SO,U_SEI_key_SO,U_SEI_Version_Office,U_SEI_ID_Office,U_SEI_Garantia,U_SEI_Presupuesto,U_SEI_Fecha_Compra,U_SEI_IP,U_SEI_Nro_Licencia_Office,U_SEI_Factura,U_SEI_RUTPROV,U_SEI_T_EQUIPO,U_SEI_UBICACION1,U_SEI_UBIC12,U_SEI_UBIC13,U_SEI_UBIC14";
				filtro = "CustomerCode eq '"+ oDatosUsuario.cole_cod +"'";
				url = "B1SLLogic?cmd=Get&actionUri=CustomerEquipmentCards&filter=" + filtro + "&select=" + select + "&sessionID=" + cookieJSESSIONID;
				callAsync(url , tipo, false);
				break;
			case 'activosFijos':
				select = "ItemCode,ItemName";
				filtro = "ItemType eq 'F' and AssetClass eq 'AFINFO'";
				url = "B1SLLogic?cmd=Get&actionUri=Items&filter=" + filtro + "&select=" + select + "&sessionID=" + cookieJSESSIONID;
				callAsync(url , tipo, false);
				break;
			case 'paises':
				url = "B1SLLogic?cmd=Get&actionUri=Countries&select=Code,Name&sessionID=" + cookieJSESSIONID;
				callAsync(url, tipo, false);
				break;
			case 'regiones':
				url = "B1SLLogic?cmd=Get&actionUri=States&select=Code,Name&filter=startswith(Country ,'CL')&sessionID=" + cookieJSESSIONID;
				callAsync(url, tipo, false);
				break;
			case 'ciudades':
				url = "B1SLLogic?cmd=Get&actionUri=U_A1A_TIPR&" + limit + select + filtro + "&sessionID=" + cookieJSESSIONID;
				callAsync(url, tipo, false);
				break;
			case 'comunas':
				url = "B1SLLogic?cmd=Get&actionUri=U_A1A_TICM&" + limit + select + filtro + "&sessionID=" + cookieJSESSIONID;
				callAsync(url, tipo, false);
				break;
			case 'tarjetasEquipo':
                filtro = "customer eq '"+ oDatosUsuario.cole_cod +"'";
                url = "B1SLLogic?cmd=Get&actionUri=TARJETAEQUIPO&filter=" + filtro + "&sessionID=" + cookieJSESSIONID;
                callAsync(url, tipo, false);
                break;
			case 'estructuraFisica':
				filtro = "U_SEI_COLEGIO eq '"+ oDatosUsuario.cole_cod +"'";
				url = "B1SLLogic?cmd=Get&actionUri=U_SEI_ESTRUFISICA&filter=" + filtro + "&sessionID=" + cookieJSESSIONID;
				callAsync(url , tipo, false);
				break;
			case 'motivosBaja':
				select = "ValidValuesMD";
				url = "B1SLLogic?cmd=Get&actionUri=UserFieldsMD&select=" + select + "&filter=Name eq 'SEI_MOTIVO_BAJA' and TableName eq 'OINS'&sessionID=" + cookieJSESSIONID;
				callAsync(url , tipo, false);
				break;
			case 'logout':
				url = "B1SLLogic?cmd=logout&actionUri=Logout&sessionID=" + cookieJSESSIONID;
				callAsync(url , tipo, false);
				break;
			case 'sociosNegocio':
				url = "B1SLLogic?cmd=Get&actionUri=BusinessPartners('"+ oDatosUsuario.socio_cod +"')&select=CardCode,CardName&sessionID=" + cookieJSESSIONID;
				callAsync(url , tipo, false);
				break;
			case 'CodigoEmpleado':
				url = "B1SLLogic?cmd=Get&actionUri=U_A1A_MAFU&select=" + select + "&filter=" + filtro + "&sessionID=" + cookieJSESSIONID;
				callAsync(url , tipo, false);
				break;
				
			case 'Reemplazo':
				//select="", filtro="";//filtro por colegio, fecha y estado
                //filtro = "customer eq '"+ oDatosUsuario.cole_cod +"'";
                url = "B1SLLogic?cmd=Get&actionUri=SEI_SOLR0&select=" + select + "&filter=" + filtro + "&sessionID=" + cookieJSESSIONID;
                callAsync(url, tipo, false);
                break;
		}
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}
}

function callAsync(url, tipo, async){
	mesajeError = "";
	
	url = url + '&vPortal='+ vPortal;	
	
	jQuery.ajax({
		type: "POST",
		xhrFields: { withCredentials: true },
		url: url,
		dataType: "JSON",
		async: async,
		crossDomain: true,
		success: function (res) {
			if(res){
				var result = JSON.parse(res);
	            if (result.error) {
	            	if(result.error.code === 301){	            				
						new sap.m.MessageBox.show(
								"Error en la autenticacion: Codigo " + result.error.code + "\n" +
								"El usuario necesita Permisos en SAP para realizar esta Operacion o \n Los Datos Ingresados del Usuario son Incorrectos", { 
			               	icon: sap.m.MessageBox.Icon.ERROR, 
			              	title: "Fallo En Llamada.",
			              	onClose: function(oAction){
			              		document.cookie = "B1SESSION" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
								document.cookie = "ROUTEID" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
								sap.ui.core.BusyIndicator.hide();
								window.location.replace("");
			              	}
			            });
					} else {
						new sap.m.MessageBox.show(
								"Service Layer La llamada a fallado en " + tipo + ": Codigo " + result.error.code + "\n" +
										result.error.message.value, { 
			               	icon: sap.m.MessageBox.Icon.ERROR, 
			              	title: "Fallo En Llamada.",
			              	onClose: function(oAction){
			              		getAsync(tipo);//si falla la llamada desde el service layer la vuelve a llamar
			              	}
			            });
					}
		            return;
		        } else {
		        	sessionStorage.removeItem("timeSession");
					sessionStorage.setItem("timeSession", JSON.stringify(new Date()));	
		        	
		        	sessionStorage.setItem(tipo, JSON.stringify(result));
		        	//sessionStorage.setItem(tipo, JSON.stringify(result));
		        }
			} else {
				//"Service Layer La llamada fue realizada con exito!!"
			}
		},
		error: function (request, textStatus, errorThrown) {
			new sap.m.MessageBox.show(
				"Service Layer La llamada a fallado: " + textStatus + " / " + errorThrown,{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction){
					callAsync(url, tipo, async);//si falla la comunicacion al service layer la vuelve a llamar
				}
	  	    });
		}
	});
}

function setEquipmentCard(body, nuevo) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
		var url;
	    if(nuevo == true){
	    	url = "B1SLLogic?cmd=Add&actionUri=CustomerEquipmentCards&sessionID=" + document.cookie;
	    }else{
	    	url = "B1SLLogic?cmd=Update&actionUri=CustomerEquipmentCards(" + equipmentCardNum + ")&sessionID=" + document.cookie;
	    }
	    
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.value) {
	    	oItemsRefresh = oResp.json;
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	cajaMensaje("Fallo en la accion la Tarjeta de Equipo.", "Service Layer Tarjeta de Equipo a fallado: " + mensajeError, "fail");
	    	return 0;
	    } else {
	    	if(oResp.json == "" || oResp.json.InternalSerialNum !== ""){
	    		cajaMensaje("Exito en la accion de Tarjeta de Equipo.", "Accion realizada con exito.", "success");
	    		return 1;
	    	} else {
	    		cajaMensaje("Fallo En Llamada.", mensajeError, "fail");
	    		return 0;
	    	}
	    }
	} else {
		//window.location.href = "login.html";
	}	
}

/**
 * Funcion Encargada de Obtener las Solicitudes de Ordenes Creadas en SAP
 * @returns
 */
function getPurchase(fecIni = null,fecFin = null) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		var url, mesajeError = "", cookieSESSIONID = sessionStorage.getItem("cookieSESSIONID")
		
		if((oDatosUsuario === undefined || !oDatosUsuario) && sessionStorage.getItem("tiles")){
			oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
		} else if (!sessionStorage.getItem("tiles")) {
			delCookies();
			window.location.replace("");
		}
		
		sap.ui.core.BusyIndicator.show(0);
		var fecha
		if(fecIni == null){
			fecha = " and DocDate gt '" + sumarDias(new Date(), -365).toISOString().substr(0,10) + "'";
		}else{
			fecha = "and DocDate gt '" + fecIni + "' and DocDate lt '" + fecFin + "'";
		}
		
		if(oDatosUsuario.code_rol === "1"){
			url = "B1SLLogic?cmd=Get&actionUri=PurchaseRequests&select=DocEntry,DocNum,DocDate,Comments,RequesterName,RequriedDate,U_SEI_ESTADO,U_SEI_UNIDAD,U_SEI_TIPO_FINANCIAMIENTO,U_SEI_DEPARTAMENTO,DocumentStatus,DocumentLines,U_SEI_VIRTUAL,U_SEI_FECHAPRO&filter=U_SEI_UNIDAD eq '" + oDatosUsuario.des_suc +"'" + fecha + "&sessionID=" + cookieSESSIONID;
		} else{
			url = "B1SLLogic?cmd=Get&actionUri=PurchaseRequests&select=DocEntry,DocNum,DocDate,Comments,RequesterName,RequriedDate,U_SEI_ESTADO,U_SEI_UNIDAD,U_SEI_TIPO_FINANCIAMIENTO,U_SEI_DEPARTAMENTO,DocumentStatus,DocumentLines,U_SEI_VIRTUAL,U_SEI_FECHAPRO&filter=U_SEI_UNIDAD eq '" + oDatosUsuario.des_suc + "' and Requester eq '" + oDatosUsuario.user_code +"' " + fecha  +"&sessionID=" + cookieSESSIONID;
		}
	    var oResp = llamadaAjax(url, "POST");
	    
	    if(oResp.json.value) {
	    	oPurchaseJSONData = oResp.json;

	    	oPurchaseJSONData.value.forEach(function(purchase){
	    		var dLines = [];
	    		purchase.DocumentLines.forEach(function(linea){
	    			var objPaso = {
		    				LineNum : linea.LineNum,
		    				ItemCode : linea.ItemCode,
		    				ItemDescription : linea.ItemDescription,
		    				FreeText : linea.FreeText,
		    				Quantity : linea.Quantity,
		    				U_SEI_ISBN : linea.U_SEI_ISBN,
		    				RequiredDate : linea.RequiredDate,
		    				CostingCode : linea.CostinCode,
		    				CostingCode2 : linea.CostingCode2,
		    				CostingCode3 : linea.CostingCode3,
		    				U_SEI_NIVEL1 : linea.U_SEI_NIVEL1,
		    				U_SEI_NIVEL2 : linea.U_SEI_NIVEL2,
		    				U_SEI_NIVEL3 : linea.U_SEI_NIVEL3,
		    				U_SEI_NIVEL4 : linea.U_SEI_NIVEL4,
		    				ItemsGroupCode : linea.ItemsGroupCode,
		    				Properties1 : linea.Properties1,
		    				
		    		};
	    			dLines.push(objPaso);
	    		});
	    		purchase.DocumentLines = dLines;
	    	});
	    	
	    	sessionStorage.setItem("ordenes", JSON.stringify(oPurchaseJSONData));
	    	
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	if(oResp.json.error.code === 301){
	    		new sap.m.MessageBox.show(
						"Error en la autenticacion: Codigo " + result.error.code + "\n" +
						"El usuario necesita Permisos en SAP para realizar esta Operacion o \n Los Datos Ingresados del Usuario son Incorrectos", { 
	               	icon: sap.m.MessageBox.Icon.ERROR, 
	              	title: "Fallo En Llamada.",
	              	onClose: function(oAction){
	              		document.cookie = "B1SESSION" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
						document.cookie = "ROUTEID" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
						sap.ui.core.BusyIndicator.hide();
						window.location.replace("");
	              	}
	            });
	    	}else{
	    		cajaMensaje("Fallo En Llamada de Solicitudes de Ordenes.", "Service Layer La llamada a las Solicitudes de Ordenes a fallado " + mensajeError, "fail");
	    	}
	    } else {
	    	cajaMensaje("Fallo En Llamada.", mensajeError, "fail");
	    }
	}
	else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}
}

/**
 * Funcion Encargada de Crear una Nueva Solicitud de Orden
 * @param body
 * @returns
 */
function setPurchase(body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=Add&actionUri=PurchaseRequests&sessionID=" + document.cookie;
	    
	    
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.value) {
	    	oItemsRefresh = oResp.json;
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	cajaMensaje("Fallo En La creacion de la Solicitud de Ordenes.", "Service Layer La Creacion de Solicitud de Ordenes a fallado: " + mensajeError, "fail");
	    	return 0;
	    } else {
	    	if(oResp.json){
	    		cajaMensaje("Exito En La creacion de la Solicitud de Ordenes.", "La Solicitud de Ordenes fue creada con el DocEntry: " + oResp.json.DocEntry + " y DocNum: " + oResp.json.DocNum + ".", "success");
	    		return 1;
	    	} else {
	    		cajaMensaje("Fallo En Llamada.", mensajeError, "fail");
	    		return 0;
	    	}
	    }
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

/**
 * Funcion encargada de cambiar el estado de la orden a Aprobada o Rechazada 
 * @param docEntry
 * @param body
 * @returns
 */
function updatePurchase(docEntry, body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=Update&actionUri=PurchaseRequests(" + docEntry + ")&sessionID=" + document.cookie;
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.value) {
	    	oItemsRefresh = oResp.json;
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	cajaMensaje("Fallo En La modificacion del estado de la Solicitud de Ordenes.", "Service Layer La modificacion del estado de Solicitud de Ordenes a fallado: " + mensajeError, "fail");
	    	return oResp.json.error;
	    } else if(!oResp.json) {
	    	sap.ui.core.BusyIndicator.hide();
	    	return oResp.json;
	    }
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

function delLinePurchase(docEntry, body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=DelLine&actionUri=PurchaseRequests(" + docEntry + ")&sessionID=" + document.cookie;
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.value) {
	    	oItemsRefresh = oResp.json;
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	cajaMensaje("Fallo En La modificacion de la Solicitud de Orden.", "Service Layer La Eliminacion de la o las lineas de la Solicitud de Orden a fallado: " + mensajeError, "fail");
	    	return oResp.json.error;
	    } else if(!oResp.json) {
	    	sap.ui.core.BusyIndicator.hide();
	    	return oResp.json;
	    }
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

/**
 * Funcion Encargada de Obtener las Llamada de Servicios Creadas en SAP
 * @returns
 */
function getService() {	
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		var url, CustomerCode = JSON.parse(sessionStorage.getItem("location")).value[0].U_SEI_COLEGIO
		mesajeError = "";
		
		if((oDatosUsuario === undefined || !oDatosUsuario) && sessionStorage.getItem("tiles")){
			oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
		} else if (!sessionStorage.getItem("tiles")) {
			delCookies();
			window.location.replace("");
		}
		
		sap.ui.core.BusyIndicator.show(0);
		if(oDatosUsuario.code_rol === "1"){//U_SEI_ESTADO
			url = "B1SLLogic?cmd=Get&actionUri=ServiceCalls"
									+"&select=ServiceCallID,CustomerCode,CustomerName,AssigneeCode,Status,CustomerCode,"
											+"CreationDate,Description,Subject,ServiceCallActivities,AssigneeCode,Resolution,"
											+"ServiceCallInventoryExpenses,ContactCode,ItemCode,InternalSerialNum,Priority,ProblemType,"
											+"U_SEI_ESTADO,U_SEI_ESPECIALIDADES,"
											+"U_SEI_UBICACION1,"
											+"U_SEI_UBIC12,U_SEI_UBIC13,U_SEI_UBIC14,"
											+"U_SEI_UBIC22,U_SEI_UBIC23,U_SEI_UBIC24,"
											+"U_SEI_UBIC32,U_SEI_UBIC33,U_SEI_UBIC34,"
											+"U_SEI_UBIC42,U_SEI_UBIC43,U_SEI_UBIC44,"
											+"U_SEI_UBIC52,U_SEI_UBIC53,U_SEI_UBIC54,"
											+"U_SEI_UBIC62,U_SEI_UBIC63,U_SEI_UBIC64,"
											+"U_SEI_UBIC72,U_SEI_UBIC73,U_SEI_UBIC74,"
											+"U_SEI_UBIC82,U_SEI_UBIC83,U_SEI_UBIC84,"
											+"U_SEI_UBIC92,U_SEI_UBIC93,U_SEI_UBIC94,"
											+"U_SEI_UBIC102,U_SEI_UBIC103,U_SEI_UBIC104"
									+"&filter=U_SEI_COD_SUC eq '" + oDatosUsuario.cod_suc + "'"
									+"&sessionID=" + document.cookie;
		} else{
			url = "B1SLLogic?cmd=Get&actionUri=ServiceCalls" 
									+"&select=ServiceCallID,CustomerCode,CustomerName,AssigneeCode,CustomerCode,"
											+"CreationDate,Description,Subject,ServiceCallActivities,AssigneeCode,"
											+"ServiceCallInventoryExpenses,ItemCode,InternalSerialNum,ContactCode,Priority,ProblemType,"
											+"U_SEI_ESTADO,U_SEI_ESPECIALIDADES,"
											+"U_SEI_UBICACION1,"
											+"U_SEI_UBIC12,U_SEI_UBIC13,U_SEI_UBIC14,"
											+"U_SEI_UBIC22,U_SEI_UBIC23,U_SEI_UBIC24,"
											+"U_SEI_UBIC32,U_SEI_UBIC33,U_SEI_UBIC34,"
											+"U_SEI_UBIC42,U_SEI_UBIC43,U_SEI_UBIC44,"
											+"U_SEI_UBIC52,U_SEI_UBIC53,U_SEI_UBIC54,"
											+"U_SEI_UBIC62,U_SEI_UBIC63,U_SEI_UBIC64,"
											+"U_SEI_UBIC72,U_SEI_UBIC73,U_SEI_UBIC74,"
											+"U_SEI_UBIC82,U_SEI_UBIC83,U_SEI_UBIC84,"
											+"U_SEI_UBIC92,U_SEI_UBIC93,U_SEI_UBIC94,"
											+"U_SEI_UBIC102,U_SEI_UBIC103,U_SEI_UBIC104"
									+"&filter=AssigneeCode eq " + oDatosUsuario.user_id + ""
									//and CustomerCode eq '" + oDatosUsuario.cod_col + "'
									+"&sessionID=" + document.cookie;
		}
	    var oResp = llamadaAjax(url, "POST");
	    
	    if(oResp.json.value) {
	    	oServiceJSONData = oResp.json;
	    	sessionStorage.setItem("services", JSON.stringify(oResp.json));
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	if(oResp.json.error.code === 301){
	    		new sap.m.MessageBox.show(
						"Error en la autenticacion: Codigo " + result.error.code + "\n" +
						"El usuario necesita Permisos en SAP para realizar esta Operacion o \n Los Datos Ingresados del Usuario son Incorrectos", { 
	               	icon: sap.m.MessageBox.Icon.ERROR, 
	              	title: "Fallo En Llamada.",
	              	onClose: function(oAction){
	              		document.cookie = "B1SESSION" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
						document.cookie = "ROUTEID" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
						sap.ui.core.BusyIndicator.hide();
						window.location.replace("");
	              	}
	            });
	    	} else {
	    		cajaMensaje("Fallo En Llamada de Servicio.", "Service Layer La Llamada de Servicio a fallado: " + mensajeError, "fail");
	    	}
	    } else {
	    	cajaMensaje("Fallo En Llamada.", mensajeError, "fail");
	    }
	}
	else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}
}

/**
 * Funcion Encargada de Crear una Nueva Llamada de Servicio
 * @param body
 * @returns
 */
function setService(body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=Add&actionUri=ServiceCalls&sessionID=" + document.cookie;
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.error) {
	    	cajaMensaje("Fallo En La creacion de la Lamada de Servicio.", "Service Layer La Creacion de Llamada de Servicio a fallado, codigo:" + oResp.json.error.code + "\n" + oResp.json.error.message.value, "fail");
	    	return 0;
	    } else if(oResp.json){
    		cajaMensaje("Exito En La creacion de la Llamada de Servicio.", "La Llamada de Servicio fue creada con el ServiceCallID: " + oResp.json.ServiceCallID + " y DocNum: " + oResp.json.DocNum + ".", "success");
    		return oResp.json.ServiceCallID;
    	} else {
    		cajaMensaje("Fallo En Llamada.", mensajeError, "fail");
    		return 0;
    	}
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

/**
 * Funcion encargada de cambiar el estado de la Llamada de Servicio a Aprobada o Rechazada 
 * @param docEntry
 * @param body
 * @returns
 */
function updateService(serviceCallID, body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=Update&actionUri=ServiceCalls(" + serviceCallID + ")&sessionID=" + document.cookie;
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.error) {
	    	cajaMensaje("Fallo En La modificacion del estado de la Llamada de Servicio.", 
	    			"Service Layer La modificacion del estado de la Llamada de Servicio a fallado: " + mensajeError, "fail");
	    } else if(!oResp.json) {
	    	sap.ui.core.BusyIndicator.hide();
	    	return oResp.json;
	    } else {
    		cajaMensaje("Fallo En Actividad.", mensajeError, "fail");
    		sap.ui.core.BusyIndicator.hide();
    		return 0;
    	}
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

function addActivitiesService(serviceCallID, body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=DelLine&actionUri=ServiceCalls(" + serviceCallID + ")&sessionID=" + document.cookie;
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.error) {
	    	cajaMensaje("Fallo En La modificacion del estado de la Llamada de Servicio.", 
	    			"Service Layer La modificacion del estado de la Llamada de Servicio a fallado: " + mensajeError, "fail");
	    } else if(!oResp.json) {
	    	sap.ui.core.BusyIndicator.hide();
	    	return oResp.json;
	    } else {
    		cajaMensaje("Fallo En Actividad.", mensajeError, "fail");
    		sap.ui.core.BusyIndicator.hide();
    		return 0;
    	}
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

function delLineService(serviceCallID, body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=DelLine&actionUri=ServiceCalls(" + serviceCallID + ")&sessionID=" + document.cookie;
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.error) {
	    	cajaMensaje("Fallo En La modificacion de la Llamada de Servicio.", 
	    				"Service Layer La Eliminacion de la o las lineas de la Llamada de Servicio a fallado Codigo: " + oResp.json.error.code + "\n" + oResp.json.error.message.value, "fail");
	    	sap.ui.core.BusyIndicator.hide();
	    	return oResp.json.error;
	    } else if(!oResp.json) {
	    	sap.ui.core.BusyIndicator.hide();
	    	return oResp.json;
	    } else {
    		cajaMensaje("Fallo En Actividad.", mensajeError, "fail");
    		sap.ui.core.BusyIndicator.hide();
    		return 0;
    	}
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

/**
 * Funcion encargada de retornar el listado de Rendiciones contenidas en SAP
 * @returns
 */
function getRendiciones() {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		var url;
		mesajeError = "";
		
		if((oDatosUsuario === undefined || !oDatosUsuario) && sessionStorage.getItem("tiles")){
			oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
		} else if (!sessionStorage.getItem("tiles")) {
			delCookies();
			window.location.replace("");
		}
		
		sap.ui.core.BusyIndicator.show(0);
		if(oDatosUsuario.code_rol === "1"){//U_SEI_ESTADO
			url = "B1SLLogic?cmd=Get&actionUri=SEIRENDI"
									+"&select=DocNum,DocEntry,U_SEI_Unidad,U_SEI_NFUNCIO,U_SEI_FECHAREN,U_SEI_CFUNCIO,U_SEI_NROPAGO,SEI_RENDETCollection,U_SEI_ESTADO,U_SEI_TIPOFI,U_SEI_TRENDI,U_SEI_DPTO,U_SEI_ENVIO,U_SEI_MOT_RECH"
									+"&filter=U_SEI_Unidad eq '" + oDatosUsuario.des_suc + "'"
									+"&sessionID=" + cookieJSESSIONID;
		} else{
			url = "B1SLLogic?cmd=Get&actionUri=SEIRENDI" 
									+"&select=DocNum,DocEntry,U_SEI_Unidad,U_SEI_NFUNCIO,U_SEI_FECHAREN,U_SEI_CFUNCIO,U_SEI_NROPAGO,SEI_RENDETCollection,U_SEI_ESTADO,U_SEI_TIPOFI,U_SEI_TRENDI,U_SEI_DPTO,U_SEI_ENVIO,U_SEI_MOT_RECH"
									+"&filter=U_SEI_Unidad eq '" + oDatosUsuario.des_suc + "' and U_SEI_UCREADOR eq '" + oDatosUsuario.user_name + "'"
									+"&sessionID=" + cookieJSESSIONID;
		}
	    var oResp = llamadaAjax(url, "POST");
	    
	    if(oResp.json.value) {
	    	sessionStorage.setItem("rendiciones", JSON.stringify(oResp.json));
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	if(oResp.json.error.code === 301){
	    		new sap.m.MessageBox.show(
						"Error en la autenticacion: Codigo " + result.error.code + "\n" +
						"El usuario necesita Permisos en SAP para realizar esta Operacion o \n Los Datos Ingresados del Usuario son Incorrectos", { 
	               	icon: sap.m.MessageBox.Icon.ERROR, 
	              	title: "Fallo En La  Llamada de Rendiciones.",
	              	onClose: function(oAction){
	              		delCookies();
	        			window.location.replace("");
	        			sap.ui.core.BusyIndicator.hide();
	              	}
	            });
	    	} else {
		    	cajaMensaje("Fallo En La Rendicion.", "Service Layer La Llamada a Rendiciones a fallado: " + mensajeError, "fail");
		    }
	    } else {
	    	cajaMensaje("Fallo En La Llamada de Rendiciones.", mensajeError, "fail");
	    }
	}
	else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}
}

function setRendiciones(body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=Add&actionUri=SEIRENDI&sessionID=" + document.cookie;
	    
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.value) {
	    	oItemsRefresh = oResp.json;
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	cajaMensaje("Fallo En La creacion de la Rendicion.", "Service Layer La Creacion de la Rendicion a fallado: " + mensajeError, "fail");
	    	return 0;
	    } else {
	    	if(oResp.json){
	    		cajaMensaje("Exito En La creacion de la Rendicion.", "La Rendicion fue creada con el DocEntry: " + oResp.json.DocEntry + " y DocNum: " + oResp.json.DocNum + ".", "success");
	    		return 1;
	    	} else {
	    		cajaMensaje("Fallo En Llamada.", mensajeError, "fail");
	    		return 0;
	    	}
	    }
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

/**
 * Funcion encargada de cambiar el estado de la Rendicion a Aprobada o Rechazada 
 * @param docEntry
 * @param body
 * @returns
 */
function updateRendiciones(docEntry, body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=Update&actionUri=SEIRENDI(" + docEntry + ")&sessionID=" + document.cookie;
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.value) {
	    	oItemsRefresh = oResp.json;
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	cajaMensaje("Fallo En La modificacion del estado de la Rendicion.", "Service Layer La modificacion del estado de la Rendicion a fallado: " + mensajeError, "fail");
	    } else {
	    	sap.ui.core.BusyIndicator.hide();
	    	//cajaMensaje("Fallo En Llamada.", mensajeError, "fail");
	    }
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

function delLineRendiciones(docEntry, body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=DelLine&actionUri=SEIRENDI(" + docEntry + ")&sessionID=" + document.cookie;
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.value) {
	    	oItemsRefresh = oResp.json;
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	cajaMensaje("Fallo En La modificacion de la Rendicion.", "Service Layer La Eliminacion de la o las lineas de rendicion a fallado: " + mensajeError, "fail");
	    	return oResp.json.error;
	    } else if(!oResp.json) {
	    	sap.ui.core.BusyIndicator.hide();
	    	return oResp.json;
	    }
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

function getSolRendiciones() {	
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		var url;
		mesajeError = "";
		
		if((oDatosUsuario === undefined || !oDatosUsuario) && sessionStorage.getItem("tiles")){
			oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
		} else if (!sessionStorage.getItem("tiles")) {
			delCookies();
			window.location.replace("");
		}
		
		sap.ui.core.BusyIndicator.show(0);
		if(oDatosUsuario.code_rol === "1"){//U_SEI_ESTADO
			url = "B1SLLogic?cmd=Get&actionUri=SEI_SOLFRENDIR"
				+"&select=DocNum,Creator,CreateDate,U_SEI_FECHAS,U_SEI_RUTRESPO,U_SEI_NRESP,U_SEI_DESACTI,U_SEI_MONTSOLI,U_SEI_UNIDAD,U_SEI_FINACI,U_SEI_DEPAR,U_SEI_FECHACTI,U_SEI_ESTADO"
				+"&filter=U_SEI_UNIDAD eq '" + oDatosUsuario.des_suc + "'"
				+"&sessionID=" + document.cookie;
		} else{
			url = "B1SLLogic?cmd=Get&actionUri=SEI_SOLFRENDIR" 
				+"&select=DocNum,Creator,CreateDate,U_SEI_FECHAS,U_SEI_RUTRESPO,U_SEI_NRESP,U_SEI_DESACTI,U_SEI_MONTSOLI,U_SEI_UNIDAD,U_SEI_FINACI,U_SEI_DEPAR,U_SEI_FECHACTI,U_SEI_ESTADO"
				+"&filter=U_SEI_UNIDAD eq '" + oDatosUsuario.des_suc + "' and Creator eq '" + oDatosUsuario.user_code + "'"
				+"&sessionID=" + document.cookie;
		}
	    var oResp = llamadaAjax(url, "POST");
	    
	    if(oResp.json.value) {
	    	sessionStorage.setItem("solrendiciones", JSON.stringify(oResp.json));
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	if(oResp.json.error.code === 301){
	    		new sap.m.MessageBox.show(
						"Error en la autenticacion: Codigo " + result.error.code + "\n" +
						"El usuario necesita Permisos en SAP para realizar esta Operacion o \n Los Datos Ingresados del Usuario son Incorrectos", { 
	               	icon: sap.m.MessageBox.Icon.ERROR, 
	              	title: "Fallo En La  Llamada de la Solicitud de Rendiciones.",
	              	onClose: function(oAction){
	              		document.cookie = "B1SESSION" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
						document.cookie = "ROUTEID" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
						sap.ui.core.BusyIndicator.hide();
						window.location.replace("");
	              	}
	            });
	    	} else {
		    	cajaMensaje("Fallo En La Solicitud de Rendicion.", "Service Layer La Llamada a la Solicitud de Rendiciones a fallado: " + mensajeError, "fail");
		    }
	    } else {
	    	cajaMensaje("Fallo En La Llamada de la Solicitud de Rendiciones.", mensajeError, "fail");
	    }
	}
	else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}
}

/**
 * Funcion Encargada de Crear una Nueva Rendicion
 * @param body
 * @returns
 */
function setSolRendiciones(body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=Add&actionUri=SEI_SOLFRENDIR&sessionID=" + document.cookie;
	    
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.value) {
	    	oItemsRefresh = oResp.json;
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	cajaMensaje("Fallo En La creacion de la Solicitud de Rendicion.", "Service Layer La Creacion de la Solicitud de Rendicion a fallado: " + mensajeError, "fail");
	    	return 0;
	    } else {
	    	if(oResp.json){
	    		cajaMensaje("Exito En La creacion de la Solicitud de Rendicion.", "La Solicitud de Rendicion fue creada con el DocEntry: " + oResp.json.DocEntry + " y DocNum: " + oResp.json.DocNum + ".", "success");
	    		return 1;
	    	} else {
	    		cajaMensaje("Fallo En Llamada.", mensajeError, "fail");
	    		return 0;
	    	}
	    }
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

/**
 * Funcion encargada de cambiar el estado de la Rendicion a Aprobada o Rechazada 
 * @param docEntry
 * @param body
 * @returns
 */
function updateSolRendiciones(docEntry, body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=Update&actionUri=SEI_SOLFRENDIR(" + docEntry + ")&sessionID=" + document.cookie;
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.value) {
	    	oItemsRefresh = oResp.json;
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	cajaMensaje("Fallo En La modificacion del estado de la Solicitud de Rendicion.", "Service Layer La modificacion del estado de la Solicitud de Rendicion a fallado: " + mensajeError, "fail");
	    } else {
	    	sap.ui.core.BusyIndicator.hide();
	    	//cajaMensaje("Fallo En Llamada.", mensajeError, "fail");
	    }
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

function delLineSolRendiciones(docEntry, body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=DelLine&actionUri=SEI_SOLFRENDIR(" + docEntry + ")&sessionID=" + document.cookie;
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.value) {
	    	oItemsRefresh = oResp.json;
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	cajaMensaje("Fallo En La modificacion de la Solicitud de Rendicion.", "Service Layer La Eliminacion de la Solicitud de rendicion a fallado: " + mensajeError, "fail");
	    	return oResp.json.error;
	    } else if(!oResp.json) {
	    	sap.ui.core.BusyIndicator.hide();
	    	return oResp.json;
	    }
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

/**
* Funcion Encargada de Crear una Actividad en una llamada de Servicio 18-02-2020
* @param body
* @returns
*/
function setActivities(body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=Add&actionUri=Activities&sessionID=" + document.cookie;
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.error) {
	    	if(oResp.json.error.code === 301){
	    		new sap.m.MessageBox.show(
						"Error en la autenticacion: Codigo " + result.error.code + "\n" +
						"El usuario necesita Permisos en SAP para realizar esta Operacion o \n Los Datos Ingresados del Usuario son Incorrectos", { 
	               	icon: sap.m.MessageBox.Icon.ERROR, 
	              	title: "Fallo En Llamada.",
	              	onClose: function(oAction){
	              		document.cookie = "B1SESSION" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
						document.cookie = "ROUTEID" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
						sap.ui.core.BusyIndicator.hide();
						window.location.replace("");
	              	}
	            });
	    	}else{
	    		cajaMensaje("Fallo En La creacion de la Actividad.", 
	    				"Service Layer La llamada a fallado en La Creacion de Actividad: Codigo " + oResp.json.error.code + "\n" + oResp.json.error.message.value, "fail");
	    		setActivities(body);
	    	}
	    	
	    } else if(oResp.json.ActivityCode){
//    		cajaMensaje("Exito En La creacion de la Actividad.", "La Actividad fue creada con el ActivityCode: " + oResp.json.ActivityCode + ".", "success");
    		return oResp.json.ActivityCode;
    	} else {
    		cajaMensaje("Fallo En Actividad.", mensajeError, "fail");
    		setActivities(body);
    	}
	    
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

/**
* Funcion Encargada de Actualizar una Actividad en una llamada de Servicio
* @param body
* @returns
*/
function updateActivities(activityCode, body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=Update&actionUri=Activities(" + activityCode + ")&sessionID=" + document.cookie;
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.error) {
	    	cajaMensaje("Fallo En La Actualizacion de la Actividad.", 
	    				"Service Layer La llamada a fallado en La Creacion de Actividad: Codigo " + oResp.json.error.code + "\n" + oResp.json.error.message.value, "fail");
	    	return 0;
	    } else if(!oResp.json){
//    		cajaMensaje("Exito En La Actualizacion de la Actividad.", "La Actividad fue Modificada con el Exito. " + oResp.json.ActivityCode + ".", "success");
    		return 0;
    	} else {
    		cajaMensaje("Fallo En Actividad.", mensajeError, "fail");
    		return 0;
    	}
	    
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

function updeteUser(InternalKey, body) {
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
	    var url = "B1SLLogic?cmd=Update&actionUri=Users(" + InternalKey + ")&sessionID=" + document.cookie;
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.value) {
	    	oItemsRefresh = oResp.json;
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	cajaMensaje("Gestion de Contraseña.", "Service Layer La modificacion de la Contraseña a fallado: " + mensajeError, "fail");
	    	return oResp.json.error;
	    } else if(!oResp.json) {
	    	sap.ui.core.BusyIndicator.hide();
	    	return oResp.json;
	    }
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}	
}

function getReport() {
	var cookieJSESSIONID = "", // = sessionStorage.getItem("JSESSIONID");//cookieJSESSIONID = getNomCookies("JSESSIONID");
	mesajeError = "";
	
	var body = "<?xml version='1.0' encoding='UTF-8'?>"
	+ "<b1mb:GetReportByCode xmlns:b1mb='http://tempuri.org/' xmlns='b1mb'>"
    + "<b1mb:Code>PQT20001</b1mb:Code>"
    + "<b1mb:ParameterLines>"
    + 	"<b1mb:parameter>"
    + 		"<b1mb:name>DocKey@</b1mb:name>"
    + 		"<b1mb:type>xsd:string</b1mb:type>"
    + 		"<b1mb:v1>2</b1mb:v1>"
    + 	"</b1mb:parameter>"
    + 	"<b1mb:parameter>"
    + 		"<b1mb:name>ObjectId@</b1mb:name>"
    + 		"<b1mb:type>xsd:string</b1mb:type>"
    + 		"<b1mb:v1>540000006</b1mb:v1>"//1470000113
    + 	"</b1mb:parameter>"
    + "</b1mb:ParameterLines>"
    + "</b1mb:GetReportByCode>";
	
		
	sap.ui.core.BusyIndicator.show(0);
    var url = "B1SLLogic?cmd=Get&actionUri=ReportByCode&B1IF=B1IF";//&sessionID=" + cookieJSESSIONID;
    var result = callServicesB1IF(url, body, false);
    
    if (result.error) {
    	if(result.error === 301 || result.error === 302 || result.error === 304 ||
			result.error === 400 || result.error === 403 || result.error === 404 ||
			result.error === 405 || result.error === 407 || result.error === 500 ||
			result.error === 503 || result.error === 505){
			
    		cajaMensaje("Fallo En La creacion de la Entrega.", "La Creacion de la Entrega a fallado. Error en la Comunicacion: Codigo " + result.error + ". " + result.value, "ERROR", "Box");
    		sap.ui.core.BusyIndicator.hide();
    		return 0;
		} else {
			cajaMensaje("Fallo En La creacion de la Entrega.", "La Creacion de la Entrega a fallado. Error en la Comunicacion: Codigo " + result.error.code + ". " + result.error.message.value, "ERROR", "Box");
			sap.ui.core.BusyIndicator.hide();
			document.cookie = "JSESSIONID" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		}
//    } else if (result.Status.DIresult === "failure"){
    } else if (result === ""){
    	cajaMensaje("Fallo En La creacion de la Entrega.", "Error en la Comunicacion: " + result.Status.DImsg + ".", "ERROR", "Box");
		return result.Status;
//    } else if (result.Status.DIresult === "success"){
    } else if (result.response){
    	sap.ui.core.BusyIndicator.hide();
    	window.open("data:application/pdf;base64," + result.response);
      	
    	var byteCharacters = atob(result.response);
    	var byteNumbers = new Array(byteCharacters.length);
    	for (var i = 0; i < byteCharacters.length; i++) {
    	  byteNumbers[i] = byteCharacters.charCodeAt(i);
    	}
    	var byteArray = new Uint8Array(byteNumbers);
    	var file = new Blob([byteArray], { type: 'application/pdf;base64' });
    	var fileURL = URL.createObjectURL(file);
    	window.open(fileURL);
    	
    }
}

function getReportPresupuesto(codSuc, fecha, codRep){
	var cookieJSESSIONID = "", // = sessionStorage.getItem("JSESSIONID");//cookieJSESSIONID = getNomCookies("JSESSIONID");
		mesajeError = "";
	
	if((oDatosUsuario === undefined || !oDatosUsuario) && sessionStorage.getItem("tiles")){
		oDatosUsuario = JSON.parse(sessionStorage.getItem("tiles"));
	} else if (!sessionStorage.getItem("tiles")) {
		delCookies();
		window.location.replace("");
	}
	
	var body = "<?xml version='1.0' encoding='UTF-8'?>"
	+ "<b1mb:GetReportByCode xmlns:b1mb='http://tempuri.org/' xmlns='b1mb'>"
    + "<b1mb:Code>RCRI0020</b1mb:Code>"
    + "<b1mb:ParameterLines>"
    + 	"<b1mb:parameter>"
    + 		"<b1mb:name>Unidad</b1mb:name>"
    + 		"<b1mb:type>xsd:string</b1mb:type>"
    + 		"<b1mb:v1>" + codSuc + "</b1mb:v1>"
    + 	"</b1mb:parameter>"
    + 	"<b1mb:parameter>"
    + 		"<b1mb:name>Anio</b1mb:name>"
    + 		"<b1mb:type>xsd:string</b1mb:type>"
    + 		"<b1mb:v1>" + fecha + "</b1mb:v1>"
    + 	"</b1mb:parameter>"
    + "</b1mb:ParameterLines>"
    + "</b1mb:GetReportByCode>";
	
		
	sap.ui.core.BusyIndicator.show(0);
    var url = "B1SLLogic?cmd=Get&actionUri=ReportByCode&B1IF=B1IF";//&sessionID=" + cookieJSESSIONID;
    var result = callServicesB1IF(url, body, false);
    
    if (result.error) {
    	if(result.error === 301 || result.error === 302 || result.error === 304 ||
			result.error === 400 || result.error === 403 || result.error === 404 ||
			result.error === 405 || result.error === 407 || result.error === 500 ||
			result.error === 503 || result.error === 505){
			
    		cajaMensaje("Fallo En La creacion del Reporte de Rendicion.", "La Creacion del Reporte de Rendicion a fallado. Error en la Comunicacion: Codigo " + result.error + ". " + result.value, "ERROR", "Box");
    		sap.ui.core.BusyIndicator.hide();
    		return 0;
		} else {
			cajaMensaje("Fallo En La creacion del Reporte de Rendicion.", "La Creacion del Reporte de Rendicion a fallado. Error en la Comunicacion: Codigo " + result.error.code + ". " + result.error.message.value, "ERROR", "Box");
			sap.ui.core.BusyIndicator.hide();
			document.cookie = "JSESSIONID" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		}
    } else if (result === ""){
    	cajaMensaje("Fallo En La creacion del Reporte de Rendicion.", "Error en la Comunicacion: " + result.Status.DImsg + ".", "ERROR", "Box");
		return result.Status;
    } else {
    	if (result.response !== "") {
	    	sap.ui.core.BusyIndicator.hide();
	    	//window.open("data:application/pdf;base64," + result.response);
	    	
	    	var byteCharacters = atob(result.response);
	    	var byteNumbers = new Array(byteCharacters.length);
	    	for (var i = 0; i < byteCharacters.length; i++) {
	    	  byteNumbers[i] = byteCharacters.charCodeAt(i);
	    	}
	    	var byteArray = new Uint8Array(byteNumbers);
	    	var file = new Blob([byteArray], { type: 'application/pdf;base64' });
	    	var fileURL = URL.createObjectURL(file);
	    	window.open(fileURL);
	    	
    	} else {
    		cajaMensaje("No existen Rendiciones para este Colegio.", "Error Reporte Vacio.", "fail", "Box");
    	}
    }
}

function callServicesB1IF(url, body, async){
	var resultInfo = {}, tokenJSESSIONID,// = getCookies("JSESSIONID"), //cookieJSESSIONID = sessionStorage.getItem("JSESSIONID");//cookieJSESSIONID = getNomCookies("JSESSIONID");
	sessionId = JSON.parse(sessionStorage.getItem("SESSIONID"));
	
//	var basicAuth = `es-ES/${sessionId.UserName}/${sessionId.CompanyDB}:${sessionId.Password}`
//	var basicAuth = `es-ES/manager/PROD_COEMCO:B1Admin*`
//	basicAuth = $.base64.encode(basicAuth);
	//var basicAuth = $.base64.encode('es-ES/manager/TEST_PRODCOEMCO_2021:B1Admin*');
	var basicAuth = sessionStorage.getItem("basicAuth");
	
	body = (body === null || body === undefined) ? "" : JSON.stringify(body);
		
	jQuery.ajax({
		async: async,
		data: body,
		crossDomain: true,
		headers: { "Authorization": "Basic " + basicAuth },
		type: "POST",
		url: url,
		xhrFields: { withCredentials: true },
		success: function (res) {
			resultInfo = JSON.parse(res);
		},
		error: function (request, textStatus, errorThrown) {
			cajaMensaje("Fallo En la Comunicacion.", "La Comunicacion a fallado: La comunicacion con el Servlet ha fallado" + textStatus + " / " + errorThrown, "ERROR", "Box");
		}
	});
	return resultInfo;
}

function updateEmpleado(strBody, strCode){
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	var strUrl;
	if(cookieJSESSIONID) {
		sap.ui.core.BusyIndicator.show(0);
		
		strUrl = "B1SLLogic?cmd=Update&actionUri=U_A1A_MAFU('" + strCode + "')&sessionID=" + cookieJSESSIONID;
		
		var oResp = llamadaAjax(strUrl, "POST", strBody);
		
		if(oResp.json.value) {
	    	oItemsRefresh = oResp.json;
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	cajaMensaje("Fallo en la accion Documento de aprobacion.", "Service Layer Documento de aprobacion a fallado: " + mensajeError, "fail");
	    	return 0;
	    } else {
	    	if(oResp.json == "" || oResp.json.InternalSerialNum !== ""){
	    		cajaMensaje("Exito en la accion de Documento de aprobacion.", "Accion realizada con exito.", "success");
	    		return 1;
	    	} else {
	    		cajaMensaje("Fallo En Llamada.", mensajeError, "fail");
	    		return 0;
	    	}
	    }
		
	} else {
		new sap.m.MessageBox.show(
				"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction) {
					sap.ui.core.BusyIndicator.hide();
					delCookies();
					localStorage.clear();
					sessionStorage.clear();
					window.location.replace("");
				}
	  	    });
	}
}

function setReemplazo()
{
	var cookieJSESSIONID = sessionStorage.getItem("cookieSESSIONID");
	
	if(cookieJSESSIONID) {
		mesajeError = "";
		
		sap.ui.core.BusyIndicator.show(0);
		var url;
	    if(nuevo == true){
	    	url = "B1SLLogic?cmd=Add&actionUri=CustomerEquipmentCards&sessionID=" + document.cookie;
	    }else{
	    	url = "B1SLLogic?cmd=Update&actionUri=CustomerEquipmentCards(" + equipmentCardNum + ")&sessionID=" + document.cookie;
	    }
	    
	    var oResp = llamadaAjax(url, "POST", body);
	    
	    if(oResp.json.value) {
	    	oItemsRefresh = oResp.json;
	    	sap.ui.core.BusyIndicator.hide();
	    } else if(oResp.json.error) {
	    	cajaMensaje("Fallo en la accion la Tarjeta de Equipo.", "Service Layer Tarjeta de Equipo a fallado: " + mensajeError, "fail");
	    	return 0;
	    } else {
	    	if(oResp.json == "" || oResp.json.InternalSerialNum !== ""){
	    		cajaMensaje("Exito en la accion de Tarjeta de Equipo.", "Accion realizada con exito.", "success");
	    		return 1;
	    	} else {
	    		cajaMensaje("Fallo En Llamada.", mensajeError, "fail");
	    		return 0;
	    	}
	    }
	}else {
			new sap.m.MessageBox.show(
					"Se ha perdido la Cookie de sesion o su sesion ha expirado, Vuelve a Ingresar",{
					icon: sap.m.MessageBox.Icon.ERROR,
					title: "Fallo En Llamada.",
					onClose: function(oAction) {
						sap.ui.core.BusyIndicator.hide();
						delCookies();
						localStorage.clear();
						sessionStorage.clear();
						window.location.replace("");
					}
		  	    });
	}
}









/**
 * Funcion llamadaAjax encargada de realiza llamada ajax segun parametros indicados. 
 * @param url parametro de tipo string que contiene la url a la que se llamara
 * @param tipo parametro de tipo string que indica el tipo de llamada a realizar "GET", "POST", etc
 * @returns res que retorna el objeto de respuesta del llamado.
 */
function llamadaAjax(url, tipo, body) {
	var resultInfo = {};
	
	url = url + '&vPortal='+ vPortal;
	
	if(body === null || body === undefined){	
		jQuery.ajax({
			type: tipo,
			xhrFields: { withCredentials: true },
			url: url,
			dataType: "JSON",
			async: false,
			crossDomain: true, 
			success: function (result) {
				if(result){
					resultInfo.json = JSON.parse(result);
			    		if(resultInfo.json.error){
							if(resultInfo.json.error.code === 301){	            				
			    				mensajeError = "Error en la autenticacion: Codigo " + resultInfo.json.error.code + "\n El usuario necesita Permisos en SAP para realizar esta Operacion o \n Los Datos Ingresados del Usuario son Incorrectos"
			    								+ "\n (" + resultInfo.json.error.message.value + ")";
			    				document.cookie = "B1SESSION" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
								document.cookie = "ROUTEID" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
			    				window.location.replace("");
							} else if(resultInfo.json.error.code === 506){
								mensajeError = "en: Codigo " + resultInfo.json.error.code + "\n" + resultInfo.json.error.message.value;
							} else if((resultInfo.json.error.code === -1)){
								mensajeError = "en: Codigo " + resultInfo.json.error.code + "\n" + resultInfo.json.error.message.value;
							} else {
								mensajeError = "en: Codigo " + resultInfo.json.error.code + "\n" + resultInfo.json.error.message.value;
								//llamadaAjax(url, tipo);
							}
			    		} else if (resultInfo.json.value){
							sap.ui.core.BusyIndicator.hide();
			    		} else { 
			    			mensajeError = "Service Layer La llamada a sufrido una falla: Codigo " + result;
			    			//llamadaAjax(url, tipo);
			    		}			    		
						sap.ui.core.BusyIndicator.hide();
				} else {
					sessionStorage.removeItem("timeSession");
					sessionStorage.setItem("timeSession", JSON.stringify(new Date()));
					llamadaAjax(url, tipo);
					resultInfo.json = result;
				}
			},
			error: function (request, textStatus, errorThrown) {
				mensajeError = "Service Layer La llamada a sufrido una falla: " + textStatus + " / " + errorThrown;
				sap.ui.core.BusyIndicator.hide();
			}
		});
	} else {
		jQuery.ajax({
			type: tipo,
			xhrFields: { withCredentials: true },
			url: url,
			data: JSON.stringify(body),
			dataType: "JSON",
			async: false,
			crossDomain: true, 
			success: function (result, textStatus, xhr) {
				if(result){
					resultInfo.json = JSON.parse(result);
			    		if(resultInfo.json.error){
			    			if(resultInfo.json.error.code === 301){	            				
			    				mensajeError = "Error en la autenticacion: Codigo " + resultInfo.json.error.code + "\n El usuario necesita Permisos en SAP para realizar esta Operacion o \n Los Datos Ingresados del Usuario son Incorrectos"
								+ "\n (" + resultInfo.json.error.message.value + ")";
			    				document.cookie = "B1SESSION" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
								document.cookie = "ROUTEID" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
			    				window.location.replace("");
			    			} else if(resultInfo.json.error.code === 506){
								mensajeError = "en: Codigo " + resultInfo.json.error.code + "\n" + resultInfo.json.error.message.value;
			    			} else if(resultInfo.json.error.code === -5002){
			    				mensajeError = "en: Codigo " + resultInfo.json.error.code + "\n" + resultInfo.json.error.message.value;
			    			} else if((resultInfo.json.error.code === -1)){
								mensajeError = "en: Codigo " + resultInfo.json.error.code + "\n" + resultInfo.json.error.message.value;
							} else {
								mensajeError = "en: Codigo " + resultInfo.json.error.code + "\n" + resultInfo.json.error.message.value;
								//llamadaAjax(url, tipo, body);
							}
			    		} else if (resultInfo.json.value){
							sap.ui.core.BusyIndicator.hide();
			    		} else {
			    			mensajeError = "Service Layer La llamada a sufrido una falla: " + result;	    			
			    		}
				} else {
					sessionStorage.removeItem("timeSession");
					sessionStorage.setItem("timeSession", JSON.stringify(new Date()));					
					resultInfo.json = result;
				}
			},
			error: function (request, textStatus, errorThrown) {
				mensajeError = "Service Layer La llamada a sufrido una falla: " + textStatus + " / " + errorThrown;
			}
		});
	}
	return resultInfo;
}

/**
 * Funcion cajaMensaje encargada de mostrar el mensaje al ser invocada
 * @param titulo parametro de tipo string que indica el titulo del mensaje
 * @param contenido parametro de tipo string que contiene el contendo del mensaje
 * @param accion parametro de tipo string que contiene el tipo de mensaje a mostrar
 * @returns
 */
function cajaMensaje(titulo, contenido, accion) {
	if(accion === 'success') {
		new sap.m.MessageToast.show(contenido);
		sap.ui.core.BusyIndicator.hide();
//		new sap.m.MessageBox.show(contenido, {
//			icon: sap.m.MessageBox.Icon.success,
//			title: titulo,
//			onClose: function(oAction) {					
//	    		sap.ui.core.BusyIndicator.hide();
//			}
//		});
	} else if (accion === 'fail') {
		new sap.m.MessageToast.show(contenido);
		sap.ui.core.BusyIndicator.hide();
//		new sap.m.MessageBox.show(contenido, {
//			icon: sap.m.MessageBox.Icon.ERROR,
//			title: titulo,
//			onClose: function(oAction) {					
//	    		sap.ui.core.BusyIndicator.hide();
//			}
//		});
	}
}

function aceptPurchase(aprobado) {
	var xsjsUrl = "B1SLLogic?cmd=Update&actionUri=PurchaseRequests(" + selectedOrder + ")/"+ aprobado +"&sessionID=" + document.cookie;
	  
	jQuery.ajax({
		type: "POST",
		xhrFields: { withCredentials: true },
		url: xsjsUrl,
		dataType: "json",
		crossDomain: true, 
		success: function (result) {
			//oModel.refresh(true);
			if (JSON.parse(result).error) {
                //alert(result.error);
                sap.m.MessageBox.show(
                		"Service Layer La llamada de cierre del documento " + selectedOrder + " a fallado: " + JSON.parse(result).error.message.value,
		  		      	{
							icon: sap.m.MessageBox.Icon.ERROR,
							title: "Fallo En Cancelacion"
		  		      	});
                return;
            } else {                        	
            	sap.m.MessageBox.show(
    					"El Documento " + selectedOrder + " fue creado satisfactoriamente.", 
    					{
    						icon: sap.m.MessageBox.Icon.success,
    						title: "Confirmacion",
    						onClose: function(oAction){
                        		window.location.href = "index.html";
    						}
    					});                        	
            }			
		},
		error: function (request, textStatus, errorThrown) {
			//alert("Service Layer Call for document closure has failed: " + textStatus + " / " + errorThrown );
			sap.m.MessageBox.show(
					"Service Layer La llamada de cierre del documento " + selectedOrder + " a fallado: " + textStatus + " / " + errorThrown,
	  		      	{
						icon: sap.m.MessageBox.Icon.ERROR,
						title: "Fallo En Cancelacion"
	  		      	});
		}
	});
}

function cancelPurchase() {
	var xsjsUrl = "B1SLLogic?cmd=Action&actionUri=PurchaseRequests(" + selectedOrder + ")/Cancel&sessionID=" + document.cookie;
	
	jQuery.ajax({
		type: "POST",
		xhrFields: { withCredentials: true },
		url: xsjsUrl,
		dataType: "json",
		async: false,
		crossDomain: true, 
		success: function (result) {
			//oModel.refresh(true);
			sap.m.MessageBox.show(
					"EL Documento " + selectedOrder + " fue cancelado satisfactoriamente.",
	  		      	{
						icon: sap.m.MessageBox.Icon.success,
						title: "Confirmacion"
	  		      	});
			},
			error: function (request, textStatus, errorThrown) {
				sap.m.MessageBox.show(
						"Service Layer a sufrido una falla al cerrar el documento " + selectedOrder + ": " + textStatus + " / " + errorThrown ,
		  		      	{
							icon: sap.m.MessageBox.Icon.ERROR,
							title: "Fallo En Cancelacion"
		  		      	});
			}
	});
}

function closePurchase() {
	var xsjsUrl = "B1SLLogic?cmd=Action&actionUri=PurchaseRequests(" + selectedOrder + ")/Close&sessionID=" + document.cookie;
	  
	jQuery.ajax({
		type: "POST",
		xhrFields: { withCredentials: true },
		url: xsjsUrl,
		dataType: "json",
		crossDomain: true, 
		success: function (result) {
			//oModel.refresh(true);
			if (JSON.parse(result).error) {
                //alert(result.error);
                sap.m.MessageBox.show(
                		"Service Layer La llamada de cierre del documento " + selectedOrder + " a fallado: " + JSON.parse(result).error.message.value,
		  		      	{
							icon: sap.m.MessageBox.Icon.ERROR,
							title: "Fallo En Cancelacion"
		  		      	});
                return;
            } else {                        	
            	sap.m.MessageBox.show(
    					"El Documento " + selectedOrder + " fue cerado satisfactoriamente.", 
    					{
    						icon: sap.m.MessageBox.Icon.success,
    						title: "Confirmacion",
    						onClose: function(oAction){
                        		window.location.href = "index.html";
    						}
    					});                        	
            }			
		},
		error: function (request, textStatus, errorThrown) {
			//alert("Service Layer Call for document closure has failed: " + textStatus + " / " + errorThrown );
			sap.m.MessageBox.show(
					"Service Layer La llamada de cierre del documento " + selectedOrder + " a fallado: " + textStatus + " / " + errorThrown,
	  		      	{
						icon: sap.m.MessageBox.Icon.ERROR,
						title: "Fallo En Cancelacion"
	  		      	});
		}
	});
}

function respuesta(resp){
	if(resp === "OK")
		_button = true;
	//alert(resp);
}

function diffFecha(ini, fin){
	var fechauno = new Date(ini);
	var fechados = new Date(fin);
	var diffFech;
	if(fechauno.getTime() <= fechados.getTime()){
		diffFech = (fechados - fechauno)/86400000;
		if(diffFech <= 30){
			return diffFech.toFixed(0); 
		} else {
			return "mayor";
		}
	} else {
		return "menor";
	}
}

function porcentajeUsado (valorEntregado, valorPendiente, valorUsado){
	return (((valorPendiente - valorUsado)*100)/valorEntregado).toFixed(1);
}

var mensajeError;

function getURLSearchParams (){
	const valores = window.location.search;

	//Mostramos los valores en consola:
//	console.log(valores);//producto=camiseta&color=azul&talla=s
//	
//	//Creamos la instancia
//	const urlParams = new URLSearchParams(valores);
//	//Accedemos a los valores
//	var producto = urlParams.get('producto');
//	
//	//Verificar si existe el parámetro
//	console.log(urlParams.has('producto'));
//
//	//Puedes recorrer los valores, claves y pares completos.
//	const
//	  keys = urlParams.keys(),
//	  values = urlParams.values(),
//	  entries = urlParams.entries();
//
//	for (const value of values) console.log(value);
//	//camiseta, azul, s
	
	
	//http://localhost:8081/SeidorB1UI5/?idFormulario=RRHH&DocNum=12344
	////////////////////////////////////////////////////////////////////
//	const urlParams = new URLSearchParams( window.location.search),
//	  keys = urlParams.keys(),
//	  values = urlParams.values(),
//	  entries = urlParams.entries();
//	for (const value of values) console.log(value);
	////////////////////////////////////////////////////////////////////
	const urlParams = new URLSearchParams(window.location.search); 
	var datos = urlParams.entries(); 
	for (const value of datos){ 
		console.log(value[0]); console.log(value[1]);
	}
}

/**
 * Funcion encargada de devolver la cookie segun nombre entregado
 * @param nombreCookie parametro de tipo String, que contiene el nombre de la cookie a buscar
 * @returns parametro de tipo String, que devuelve el token de la cookie encontrada segun parametro
 *  entregado en nombreCookie
 */
function getCookies(nombreCookie){
	var miCookie, igual, valor, misCookies = document.cookie,
	listaCookies = misCookies.split(";");
	for(i in listaCookies) {
		busca = listaCookies[i].search(nombreCookie);
		if (busca > -1) { miCookie = listaCookies[i] } 
	}
	igual = miCookie.indexOf("=");
    valor = miCookie.substring(igual+1);
    return valor;
}

/**
 * Funcion encargada de devolver la cadena nombreCookie=tokenCcookie segun el nombre entregado
 * @param nombreCookie parametro de tipo String, que contiene el nombre de la cookie a buscar
 * @returns parametro de tipo String. que devuelve el string nombreCookie=tokenCcookie encontrada
 *  segun el parametro entregado en nombreCookie
 */
function getNomCookies(nombreCookie){
	var miCookie, igual, valor, misCookies = document.cookie,
	listaCookies = misCookies.split(";");
	for(i in listaCookies) {
		busca = listaCookies[i].search(nombreCookie);
		if (busca > -1) { miCookie = listaCookies[i] } 
	}
    return miCookie;
}

/**
 * Funcion encargada de borrar todas la cookies registradas y 
 * encontradas en la session activa
 */
function delCookies() {
    var lista = document.cookie.split(";");
    for (i in lista) {
        var igual = lista[i].indexOf("=");
        var nombre = lista[i].substring(0,igual);
        lista[i] = nombre+"="+""+";expires=1 Dec 2000 00:00:00 GMT"
        document.cookie = lista[i]
    } 
}

/**
 * Funcion encargada de calcular el tiempo actual con una fecha entregada
 * @param descTime parametro de tipo string, que contendra el tipo de tiempo a calcular
 * @param inicio parametro de tipo new Date(), que contiene la fecha que se desea calcular (fechas anteriores)
 * @returns tiempo parametro de tipo int, que es el resultado del calculo en funcion del parametro entregado en descTime
 */
function calculaTiempo(descTime, inicio) {
	var tiempo, actual = new Date();
	if(!inicio)
		inicio = new Date(JSON.parse(sessionStorage.getItem("timeSession")));
	var tiempoPasado= actual - inicio
	var segs = 1000;
	var mins = segs * 60;
	var hours = mins * 60;
	var days = hours * 24;
	var months = days * 30.416666666666668;
	var years = months * 12;
	
	switch(descTime) {
		case 'segundos':
			tiempo = Math.floor(tiempoPasado / segs);
			break;
		case 'minutos':
			tiempo = Math.floor(tiempoPasado / mins);	
			break;
		case 'horas':
			tiempo = Math.floor(tiempoPasado / hours);
			break;
		case 'dias':
			tiempo = Math.floor(tiempoPasado / days);
			break;
		case 'meses':
			tiempo = Math.floor(tiempoPasado / months);
			break;
		case 'anios':
			tiempo = Math.floor(tiempoPasado / years);
			break;
	}
	return tiempo;
}

function sumarDias(fecha, dias){
	fecha.setDate(fecha.getDate() + dias);
	return fecha;
}

var Fn = {
	// Valida el rut con su cadena completa "XXXXXXXX-X"
	validaRut : function (rutCompleto) {
		var patron = /[-\.]/g;
		rutCompleto = rutCompleto.replace(patron, '');
		cDv = rutCompleto.charAt(rutCompleto.length - 1);
		cRut = rutCompleto.substring(0, rutCompleto.length - 1);
		
		rutCompleto = cRut + '-' + cDv
		
		if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( rutCompleto ))
			return false;
		var tmp 	= rutCompleto.split('-');
		var digv	= tmp[1]; 
		var rut 	= tmp[0];
		if ( digv == 'K' ) digv = 'k' ;
		return (Fn.dv(rut) == digv );
	},
	dv : function(T){
		var M=0,S=1;
		for(;T;T=Math.floor(T/10))
			S=(S+T%10*(9-M++%6))%11;
		return S?S-1:'k';
	}
}

//async function callServiceSlayer(tipo, body){
//	const url = '${tipo}'
//	const respuesta = await fetch(url)
//	const json = await respuesta.json()
//	
//	if(respuesta.status !== 200)
//		trow Error('El Usuario no Existe')
//	
//	retutrn json.name
//}
//
//(async function(){
//	try{
//		const nombre = await callServiceSlayer('url', 'jsonBody')
//		console.log(nombre)
//	} catch(e){
//		console.log('Error: ${e}')
//	}
//})()

//callAsync(url , tipo, false);

function delay(tipo) {
	return new Promise(resolve => {
	    //setTimeout(() => {
		resolve(getAsync(tipo));
	      //resolve('resolved');
	    //}, 5000);
	 });
}

async function asyncCall() {
	console.log('calling');
	var result = await delay('departamentos'); 
	result = await delay('location'); 
	result = await delay('items');
	result = await delay('tipoDocumentos'); 
	result = await delay('serviceEspecialidades');
	result = await delay('rutProveedores'); 
	result = await delay('rutFuncionarios');
	result = await delay('tipoFinanciamientos');
	result = await delay('tipoRendicion');
	sap.ui.core.BusyIndicator.hide();
	 // expected output: 'resolved'
}
//asyncCall();

String.prototype.obtenerIniciales = function(glue){
    if (typeof glue == "undefined") {
        var glue = true;
    }

    var iniciales = this.replace(/[^a-zA-Z- ]/g, "").match(/\b\w/g);
    
    if (glue) {
        return iniciales.join('');
    }

    return  iniciales;
};

String.prototype.capitalize = function(){
    return this.toLowerCase().replace( /\b\w/g, function (m) {
        return m.toUpperCase();
    });
};

String.prototype.isNullOrEmpty = function(value) {
	  return !(typeof value === "string" && value.length > 0);
	}
;
//var inicialesJugador= "Chicharito Herandes".obtenerIniciales();

function callItem(url, tipo, async){
	mesajeError = "";		
	return jQuery.ajax({
		type: "POST",
		xhrFields: { withCredentials: true },
		url: url,
		dataType: "JSON",
		async: async,
		crossDomain: true,
		success: function (res) {
				var result = JSON.parse(res);
//	            if (result.error) {
//	            	if(result.error.code === 301){	            				
//						new sap.m.MessageBox.show(
//								"Error en la autenticacion: Codigo " + result.error.code + "\n" +
//								"El usuario necesita Permisos en SAP para realizar esta Operacion o \n Los Datos Ingresados del Usuario son Incorrectos", { 
//			               	icon: sap.m.MessageBox.Icon.ERROR, 
//			              	title: "Fallo En Llamada.",
//			              	onClose: function(oAction){
//			              		document.cookie = "B1SESSION" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
//								document.cookie = "ROUTEID" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
//								sap.ui.core.BusyIndicator.hide();
//								window.location.replace("");
//			              	}
//			            });
//					} else {
//						new sap.m.MessageBox.show(
//								"Service Layer La llamada a fallado en " + tipo + ": Codigo " + result.error.code + "\n" +
//										result.error.message.value, { 
//			               	icon: sap.m.MessageBox.Icon.ERROR, 
//			              	title: "Fallo En Llamada.",
//			              	onClose: function(oAction){
//			              		getAsync(tipo);//si falla la llamada desde el service layer la vuelve a llamar
//			              	}
//			            });
//					}
//		            return;
//		        } else {
//		        	sessionStorage.removeItem("timeSession");
//					sessionStorage.setItem("timeSession", JSON.stringify(new Date()));	
//		        	
//		        	sessionStorage.setItem(tipo, JSON.stringify(result));
//		        }
//			} else {
//				//"Service Layer La llamada fue realizada con exito!!"
		},
		error: function (request, textStatus, errorThrown) {
			new sap.m.MessageBox.show(
				"Service Layer La llamada a fallado: " + textStatus + " / " + errorThrown,{
				icon: sap.m.MessageBox.Icon.ERROR,
				title: "Fallo En Llamada.",
				onClose: function(oAction){
					callAsync(url, tipo, async);//si falla la comunicacion al service layer la vuelve a llamar
				}
	  	    });
		}
	});
}
