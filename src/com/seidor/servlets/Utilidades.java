package com.seidor.servlets;

import java.io.BufferedReader;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

//import com.seidor.servlets.B1SLLogic.MyTrustManager;

public class Utilidades {
	public static String getBuffering(BufferedReader bfr){
		String resultado = "";
		try{
			StringBuilder sb = new StringBuilder();
		    String str;
		    while((str = bfr.readLine()) != null ){
		        sb.append(str);
		    } 
		    resultado = new String(sb);
		}catch(Exception ex){
			
		} finally {
			
		}
		return resultado;
	}

	public static void disableSSL(){
		try{
			TrustManager[] trustAllCerts = new TrustManager[] { new MyTrustManager() };
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new SecureRandom());
			
			HostnameVerifier allHostsValid = new HostnameVerifier() {				
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());			
		} catch(Exception ex){
			ex.printStackTrace();
		}
	}
}