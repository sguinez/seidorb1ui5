package com.seidor.servlets;

public class PostData //Se declara una clase con una estructura similar al JSON que se recibe
{
    private String UserName;
    private String Password;
    private String CompanyDB;
    
    public PostData() { }

	public void setUserName(String userName) { UserName = userName; }
	
	public String getUserName() { return UserName; }

	public void setPassword(String password) { Password = password; }
	
	public String getPassword() { return Password; }

	public void setCompanyDB(String companyDB) { CompanyDB = companyDB; }
	
	public String getCompanyDB() { return CompanyDB; }
}